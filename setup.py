#! /usr/bin/env python
#
from distutils.core import setup, Extension
from Cython.Distutils import build_ext 
from sys import version
if version < '2.2.3':
    from distutils.dist import DistributionMetadata
    DistributionMetadata.classifiers = None
    DistributionMetadata.download_url = None


DISTNAME = 'marl'
VERSION = '0.85'
DESCRIPTION = "socket-based multiagent reinforcement learning test framework"
AUTHOR = "Minwoo Jake Lee"
EMAIL = "lemin@cs.colostate.edu"
URL = ""
pkg = ['marl', 'marl.games', 'marl.learn', 'marl.approx', 'marl.utils']
ext_pkg = 'marl'
ext = [
    Extension('approx.neuralnet', ['marl/approx/neuralnet.pyx']),
    Extension('approx.nnetq', ['marl/approx/nnetq.pyx']),
    Extension('approx.expUtils', ['marl/approx/expUtils.pyx']),
    Extension('approx.grad', ['marl/approx/grad.pyx']),
          ]

## TODO: add rl after test and design


setup(name=DISTNAME,
      version=VERSION,
      description=DESCRIPTION,
      author=AUTHOR,
      author_email=EMAIL,
      packages=pkg,
      cmdclass = {'build_ext': build_ext},
      ext_package=ext_pkg,
      ext_modules=ext,
      classifiers=[
          'Intended Audience :: Science/Research',
          'Intended Audience :: Developers',
          'License :: OSI Approved :: Python Software Foundation License',
          'Operating System :: MacOS :: MacOS X',
          'Operating System :: Microsoft :: Windows',
          'Operating System :: POSIX',
          'Programming Language :: Python',
          'Programming Language :: C',
          'Topic :: Software Development',
          'Topic :: Scientific/Engineering'
          ]
     )
