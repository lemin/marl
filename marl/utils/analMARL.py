""" Analysis tools 
    a collection of some utility functions for experiments
"""
import numpy as np
import os
import string
import re
import matplotlib
matplotlib.use('Agg') # to backend for experiments
import matplotlib.pyplot as plt

def toArray(rtr,rAll):
    if rtr is None:
        rtr = np.array(rAll).T
    else:
        rtr = np.vstack((rtr,np.array(rAll).T))
    return rtr

def storeArray(rtr,key, nact, nagent,opt=''):
    hostn = os.uname()[1]
    hostn = ''.join(string.split(hostn, '-'))
    fname = ''.join([key,'-',hostn,str(opt),'-',str(nact),'actions-',str(nagent),'agents.npz'])
    np.savez(fname, rtr)
    return fname

def plotReward(fname, intvl=100):
    if type(fname) == type(''):
        rtr = np.load(fname)
    else:
        rtr = fname
    last = rtr.shape[0] - rtr.shape[0]%intvl
    mrtr = None
    for i in xrange(0,rtr.shape[0], intvl):
        if i==0:
            mrtr = np.mean(rtr[:i+intvl,:],0)
        elif i+intvl==last:
            mrtr = np.vstack((mrtr, np.mean(rtr[i:,:],0)))
        else:
            mrtr = np.vstack((mrtr, np.mean(rtr[i:i+intvl,:],0)))

    plt.plot(mrtr)
    plt.xlabel('# games (/'+str(intvl)+')')
    plt.ylabel('Mean Rewards')
    return mrtr


def read_nsec(outdir, **param):
    splitter = re.compile(r'[\D]')

    idx_TYPE = param.pop('idx_type', 0)
    idx_HOST = param.pop('idx_host', 2)
    idx_STEP = param.pop('idx_steps', 1)
    idx_ACT = param.pop('idx_action', 3)
    idx_AGNT = param.pop('idx_agent', 4)


    for dir in os.walk(outdir):
        if dir[0].find('skip') != -1:
            print "skip", dir[0]
            continue
        for f in dir[2]:
            fn = string.split(f,'-')
            nsec = int(splitter.split(fn[idx_STEP])[5])
            return nsec

    return -1
            
def readResultDir(outdir='test',
                  setagents=[2,4,6,8,10,15,20,25,30],
                  setactions=[4,6,8,10],
                  **param):
    splitter = re.compile(r'[\D]')
    ret = {}
    lagnt = len(setagents)
    lacts = len(setactions)

    idx_TYPE = param.pop('idx_type', 0)
    idx_HOST = param.pop('idx_host', 2)
    idx_STEP = param.pop('idx_steps', 1)
    #idx_TIMEOUT = param.pop('idx_steps', 2)
    idx_ACT = param.pop('idx_action', 3)
    idx_AGNT = param.pop('idx_agent', 4)

    for dir in os.walk(outdir):
        if dir[0].find('skip') != -1:
            print "skip", dir[0]
            continue
        for f in dir[2]:
            #print os.path.split(dir[0])[1]

            fn = string.split(f,'-')

            typeName = fn[idx_TYPE]
            hostn = fn[idx_HOST]
            nsteps = int(splitter.split(fn[idx_STEP])[0])
            nsec = int(splitter.split(fn[idx_STEP])[5])
            nactions = int(splitter.split(fn[idx_ACT])[0])
            nagents = int(splitter.split(fn[idx_AGNT])[0])

            # workaround for numpy bug Ticket #1517
            with open(''.join([dir[0],'/',f])) as fd:
                rTbl = np.load(fd)['arr_0']
                #agntIdx = setagents.index(nagents)
                #actIdx = setactions.index(nactions)

                len_surv = [len(rTbl[0]['survive']), len(rTbl[1]['survive']), len(rTbl[2]['survive'])]
                if np.any(np.asarray(len_surv)==0):
                    print f 
                    #print rTbl[0]['algo'], rTbl[1]['algo'], rTbl[2]['algo']
                    #print len(rTbl[0]['survive']), len(rTbl[1]['survive']), len(rTbl[2]['survive']) 

                if not isinstance(rTbl[0], dict) and np.any(np.isnan(rTbl)):
                    # if nan is added in the end. ignore it
                    rTbl = rTbl[:-1]

                if typeName=='SRLbulk':
                    typeName += str(nsteps)

                key_str = ''.join([typeName, '-', str(nagents), '-', str(nactions)])
                if not ret.has_key(key_str):
                    ret[key_str] = []
                ret[key_str].append(rTbl)
            
    return ret

def getAnalMetrics(Rmat, Str, anali=0):
    """ for Matrix games
    """
    numG_best = np.sum(np.max(Rmat,axis=1)==Rmat[:,anali])
    adaptivity = float(numG_best) * 100. / Rmat.shape[0]
    #adapMean = np.sum(Rmat[:,0])
    adapMean= np.mean(Rmat[:,anali])

    Radap = np.mean((np.max(Rmat,axis=1)==Rmat[:,anali]) * Rmat[:,anali])

    #print Str,"best games: ", numG_best
    #print Str,"adaptivity: ", adaptivity
    numG_best1 = np.sum(np.logical_and(np.max(Rmat,axis=1)==Rmat[:,anali], np.min(Rmat,axis=1)!=Rmat[:,anali]))
    adaptivity1 = float(numG_best1) * 100. / Rmat.shape[0]
    #print Str,"best games (w/o tie): ", numG_best1
    #print Str,"adaptivity (w/o tie): ", adaptivity1


    #productivity = np.mean(Rmat[:,0]) * float(Rmat.shape[0]) / 10.
    productivity = Radap * float(Rmat.shape[0]) / 600. # ngames * A / (sec)
    #print Str,"productivity: ", productivity

    
    #print Str, adaptivity, adaptivity1, productivity, adapMean, Radap
    #if Str.find('SRL') == 0:
    #    import pdb; pdb.set_trace()
    return adaptivity, adaptivity1, productivity, adapMean, Radap


def getAnalMetricsPvP(Rmat, maxstep=500, **params):
    """ for PvP analysis 

        Parameters
        ----------
        Rmat list[nagents] = {'type':,'algo':,'rtrace','survive','ngames', 'rlen'}
    """

    nsec = params.pop('nsec', 900.)
    ignore_last = params.pop('ignore_last', False)

    ret = {}
    i = 0
    for agent in Rmat:
        _type = agent['type']
        algo = agent['algo'] + str(i)
        n_games = agent['ngames']
        n_total_steps = len(agent['rtrace'])
        rtrace = agent['rtrace']
        survive = agent['survive']

        if ignore_last:
            n_last_steps = n_games[-1]
            if n_last_steps > 0:
                rtrace = rtrace[:-n_last_steps]
            n_games = n_games[:-1]
            survive = survive[:-1]

        mask = survive == 1
        # check survive is correctly collected
        if np.all(mask == 0):
            mask = np.asarray(n_games) == maxstep
        if _type == 1: #predator needs to finish each episode earily
            mask = np.logical_not(mask)

        mask = np.repeat(mask, n_games)
        r_plus = np.asarray(rtrace)[mask]

        adaptability = np.sum(r_plus) * len(r_plus) / float(len(n_games))
        mean = np.mean(rtrace)
        productivity = adaptability * float(n_total_steps) / nsec # ngames * A / (sec)

        if not ret.has_key(algo):
            ret[algo] = {}
        ret[algo]['adap'] = adaptability
        ret[algo]['mean'] = mean
        ret[algo]['prod'] = productivity
        ret[algo]['type'] = agent['type']
        i += 1

    return ret



def plotNgamesPlayed(readDic, ylim=None, setagents=[2,4,6,8,10,15,20,25,30], setactions=[4,6,8,10]):
    lagnt = len(setagents)
    lacts = len(setactions)
    avgMat = np.zeros((lacts,lagnt))

    l=0
    for k in readDic.keys():
        avgMat += readDic[k]
        l+=1
    avgMat =avgMat/l
    linestyles = ['-', '--', '-.',':', '.', 'o']
    for i in xrange(avgMat.shape[0]):
        plt.plot(setagents, avgMat[i,:], linestyles[i], linewidth=3)
        print i
    #plt.plot(setagents, avgMat.T)
    plt.xticks(setagents)
    plt.xlim(2,30)
    #plt.xticks(range(lagnt),map(str,setagents))
    plt.legend(setactions, loc='best',title='# actions')
    if ylim is not None:
        plt.ylim( ylim )
    plt.xlabel("# agents")
    plt.ylabel("Avg. # games")
    #### Put title and labels outsize the fuction
    #plt.title("APQ")

def plotRewardDic(readDic, intvl=100):
    shape = readDic.values()[0].shape
    avgMat = np.zeros(shape)
    l=0
    for k in readDic.keys():
        avgMat += readDic[k]
        l+=1
    rtr=avgMat/l
    last = rtr.shape[0] - rtr.shape[0]%intvl
    mrtr = None
    for i in xrange(0,rtr.shape[0], intvl):
        if i==0:
            mrtr = np.mean(rtr[:i+intvl,:],0)
        elif i+intvl==last:
            mrtr = np.vstack((mrtr, np.mean(rtr[i:,:],0)))
        else:
            mrtr = np.vstack((mrtr, np.mean(rtr[i:i+intvl,:],0)))

    plt.plot(mrtr,alpha=0.5)
    plt.xlabel('# games (/'+str(intvl)+')')
    plt.ylabel('Mean Rewards')


