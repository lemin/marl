import struct
import socket
import sys
try:
    import cPickle as pickle
except:
    import pickle
from StringIO import StringIO
import time
from select import select

debug=0
SyncExit=True #False
SyncExitTimeout = 3
ACK, SEND, RESP, SNDBLK, RSPBLK, JOIN, EXIT, READY, TERM, JOINRSP = range(10)
MAXBUF=65535

def openSock(host='localhost',port=None):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.setblocking(1) # make to blocking for easier synchronization
    if port is not None:
        while True:
            try: sock.bind((host,port))
            except socket.error as (errno, msg): 
                if debug>=1: sys.stdout.write("socket bind error: "+msg+"\n")
                sys.stdout.write("socket bind error: "+msg+"\n")
                time.sleep(1)
                continue
            break
    return sock, port

def closeSock(sock):
    sock.close()

def listenSock(sock,backlog=1):
    sock.listen(backlog)

def connectSock(sock,server,maxtry=5):
    for i in xrange(maxtry):
        try: sock.connect(server)
        except socket.error: 
            continue
        return True
        break;
    if debug: print "failed to connect to server!"
    return False

"""
    packet protocol

    j - agent index
    S0_j - initial state of agent j
    t - time step
    Rg - global reward 
    St_j - state of agent j at time t
    At_j - action of agent j at time t
    St -  joint state of all agents at time t
    At -  joint action of all agents at time t

    n - number of actions 
    A_j's -  sequence of actions
    Rg's  -  rewards in sequence
    
    ACK:     | cmd=0 (byte) | j (int) | org cmd (byte) |
    SEND:    | cmd=1 (byte) | j (int) | t (int) | St_j (var) | At_j (var) |
    RESP:    | cmd=2 (byte) | j (int) | t (int) | term(int) |Rg (var) | St (var) | At (var) |
    SNDBLK:  | cmd=3 (byte) | j (int) | n (int) | St_j (var) | A_j's (var) |
    RSPBLK:  | cmd=4 (byte) | j (int) | n (int) | Rg's (var) | St's (var) | At's (var) |
    JOIN:    | cmd=5 (byte) | j (int) | S0_j (var) |
    EXIT:    | cmd=6 (byte) | j (int) | 
    READY:   | cmd=7 (byte) | j (int) | 
    TERM:    | cmd=8 (byte) | j (int) | 
    JOINRSP: | cmd=9 (byte) | j (int) | St (var) |

"""
"""
    data    data dependant on commands (key is format character
"""

def sendMarlMsg(sock, cmd, j, **data):
    # string protocol to binary
    #msg = ''.join([str(cmd),str(j),str(data)])
    #sock.sendto(msg, addr)

    if cmd==ACK:
        orgcmd = data.pop('orgcmd', -1)
        msg = struct.pack('ib', j, orgcmd)
    elif cmd==SEND:
        outs = StringIO()
        pickle.dump(j,outs,pickle.HIGHEST_PROTOCOL)
        pickle.dump(data.pop('t', 0),outs,pickle.HIGHEST_PROTOCOL)
        pickle.dump(data.pop('sj', []),outs,pickle.HIGHEST_PROTOCOL)
        pickle.dump(data.pop('aj', []),outs,pickle.HIGHEST_PROTOCOL)
        msg=outs.getvalue()
        outs.close()
        #msg = struct.pack('ibss', j,t, sj, aj)
        #msg = ''.join([js,ts,sj,aj])
    elif cmd==RESP:
        outs = StringIO()
        pickle.dump(j,outs,pickle.HIGHEST_PROTOCOL)
        pickle.dump(data.pop('t', 0),outs,pickle.HIGHEST_PROTOCOL)
        pickle.dump(data.pop('term', 0),outs,pickle.HIGHEST_PROTOCOL)
        pickle.dump(data.pop('Rg', []),outs,pickle.HIGHEST_PROTOCOL)
        pickle.dump(data.pop('sall', []),outs,pickle.HIGHEST_PROTOCOL)
        pickle.dump(data.pop('aall', []),outs,pickle.HIGHEST_PROTOCOL)
        msg=outs.getvalue()
        outs.close()
        #msg = ''.join([js,ts,sRg,sall])
        #msg = struct.pack('ibds', j, t, Rg, sall)
        #aall = pickle.dumps(data.pop('aall', []))
        #msg = struct.pack('bdss', t, Rg, sall, aall)
    elif cmd==SNDBLK:
        outs = StringIO()
        pickle.dump(j,outs,pickle.HIGHEST_PROTOCOL)
        pickle.dump(data.pop('n', 0),outs,pickle.HIGHEST_PROTOCOL)
        pickle.dump(data.pop('sj', []),outs,pickle.HIGHEST_PROTOCOL)
        pickle.dump(data.pop('Aall', []),outs,pickle.HIGHEST_PROTOCOL)
        msg=outs.getvalue()
        outs.close()
    elif cmd==RSPBLK:
        outs = StringIO()
        pickle.dump(j,outs,pickle.HIGHEST_PROTOCOL)
        pickle.dump(data.pop('n', 0),outs,pickle.HIGHEST_PROTOCOL)
        pickle.dump(data.pop('term', 0),outs,pickle.HIGHEST_PROTOCOL)
        pickle.dump(data.pop('Rgs', []),outs,pickle.HIGHEST_PROTOCOL)
        pickle.dump(data.pop('Sall', []),outs,pickle.HIGHEST_PROTOCOL)
        #pickle.dump(data.pop('Aall', []),outs)
        msg=outs.getvalue()
        outs.close()

    elif cmd==JOIN:
        outs = StringIO()
        pickle.dump(j,outs,pickle.HIGHEST_PROTOCOL)
        pickle.dump(data.pop('s0', []),outs,pickle.HIGHEST_PROTOCOL)
        msg=outs.getvalue()
        outs.close()

    elif cmd==JOINRSP:
        outs = StringIO()
        pickle.dump(j,outs,pickle.HIGHEST_PROTOCOL)
        pickle.dump(data.pop('sall', []),outs,pickle.HIGHEST_PROTOCOL)
        msg=outs.getvalue()
        outs.close()
        #msg = ''.join([js,s0])
        #msg = struct.pack('is', j,s0)
    elif cmd==EXIT:
        msg = struct.pack('i',j)
    elif cmd==READY:
        msg = struct.pack('i',j)
    elif cmd==TERM:
        msg = struct.pack('i',j)
    packet = ''.join([str(cmd),msg])
    if debug==2: sys.stdout.write("["+repr(sock)+"]  "+str((cmd,j))+"---> "+repr(packet)+"\n")
    try:
        sock.send(packet)
    except socket.error, msg:
        if debug>=1: sys.stdout.write("send error\n")

def parseMsg(cmd, data):
    if cmd==ACK:
        return struct.unpack('ib',data)
    elif cmd==SEND:
        ins = StringIO(data)
        ret = pickle.load(ins), pickle.load(ins),pickle.load(ins),pickle.load(ins)
        ins.close()
        return ret
        #j, t, sj, aj = struct.unpack('ibss', data)
    elif cmd==RESP:
        ins = StringIO(data)
        ret = pickle.load(ins), pickle.load(ins), pickle.load(ins),\
              pickle.load(ins), pickle.load(ins), pickle.load(ins)
        ins.close()
        return ret
        #j, t, Rg, sall = struct.unpack('ibds', data)
        #return j, t, Rg, pickle.loads(sall)
        #t, Rg, sall, aall = struct.unpack('bdss', data)
        #return t, Rg, pickle.loads(sall), loads(aall)

    elif cmd==SNDBLK:
        ins = StringIO(data)
        ret = pickle.load(ins), pickle.load(ins),pickle.load(ins),pickle.load(ins)
        ins.close()
        return ret
    elif cmd==RSPBLK:
        ins = StringIO(data)
        ret = pickle.load(ins), pickle.load(ins), pickle.load(ins), pickle.load(ins), pickle.load(ins)#, pickle.load(ins)
        ins.close()
        return ret

    elif cmd==JOIN:
        ins = StringIO(data)
        ret = pickle.load(ins), pickle.load(ins)
        ins.close()
        return ret

    elif cmd==JOINRSP:
        ins = StringIO(data)
        ret = pickle.load(ins), pickle.load(ins)
        ins.close()
        return ret

        #j, s0 = struct.unpack('is', data)
        #return j, pickle.loads(s0)
    elif cmd==EXIT or cmd==READY or cmd==TERM:
        return struct.unpack('i',data)[0]
        #try:
        #except struct.error:
        #    print "struct error"
        #    return -255

def recvMarlMsg(sock):
    try:
        data = sock.recv(MAXBUF)   # FIX ME: check if this is blocked call
    except socket.error: 
        data = None
    if data:
        cmd = int(data[0])
        return cmd, data[1:]
    else:
        return -1, None

###
###     Server Utility Functions
###
def replyMarlMsg(agents, cmd, t, Rg, S1all, Aall, trmflg):
    for k in agents.keys():
        if cmd==RESP:
            sendMarlMsg(agents[k][1], RESP, k, t=t, Rg=Rg, term=trmflg, sall=S1all, aall=Aall)
        else: 
            sendMarlMsg(agents[k][1], RSPBLK, k, n=t, term=trmflg, Rgs=Rg, Sall=S1all[:,k], Aall=Aall)
        #cmd,_= recvMarlMsg(agents[k][1])   #### 111

def multicastMarlMsg(agents, cmd, **param):
    for k in agents.keys():
        sendMarlMsg(agents[k][1], cmd, k, **param)

###
###     Client Utility Functions
###
def joinMarl(sock, s0, j=-1):
    sendMarlMsg(sock, JOIN, j, s0=s0)
    cmd, data = recvMarlMsg(sock)
    if debug>=1: sys.stdout.write("recvMarlMSG:::::: "+str((cmd, j))+'\n')
    #sys.stdout.write("["+str(j)+"] JOIN:::::: DONE\n")
    if cmd==JOINRSP:
        return parseMsg(cmd, data) # return j
    else: 
        if debug>=1: sys.stdout.write("Failed to join"+str(j)+"\n")
        return None, None

def exitMarl(sock, j):
    inset = [ sock ] 
    if debug==2: sys.stdout.write("["+str(j)+"] :::::: waiting ACK\n")
    if SyncExit:
        got_rep = False
        for i in xrange(SyncExitTimeout):
            sendMarlMsg(sock, EXIT, j)
            inrdy, outrdy, errrdy = select(inset, [], [],1)
            if sock in inrdy:
                cmd, _ = recvMarlMsg(sock)   
                got_rep = True
                break
        if got_rep:
            inrdy, outrdy, errrdy = select(inset, [], [],2)
            if sock in inrdy:
                cmd, _ = recvMarlMsg(sock)   
        else:
            cmd, _ = recvMarlMsg(sock)   

    else:
        sendMarlMsg(sock, EXIT, j)
        cmd, _ = recvMarlMsg(sock)   

    #sys.stdout.write("["+str(j)+"] Exit:::::: DONE\n")
    if debug>=1: sys.stdout.write("recvMarlMSG:::::: "+str((cmd, j))+'\n')
    if cmd!=ACK:
        if debug>=1: sys.stdout.write("Failed to exit"+str(j)+"\n")

def actionMarl(sock, j, t, s, a):
    
    sendMarlMsg(sock, SEND, j, t=t, sj=s, aj=a)
    cmd, data = recvMarlMsg(sock)
    if debug==2: sys.stdout.write("["+str(j)+"] recvMarlMSG:::::: "+str((cmd,parseMsg(cmd, data)[0]))+'\n')
    if cmd==RESP:
        #sendMarlMsg(sock, ACK, j, orgcmd=cmd) # ack  111
        #sys.stdout.write("["+str(j)+"] ACTION:::::: DONE\n")
        return parseMsg(cmd,data)[1:] # return parsed data except j
    else: 
        if debug>=1: sys.stdout.write("["+repr(sock)+"] Wrong response: "+str(cmd)+"\n")
        return -1, None, None, None

def actionMarlBulk(sock, j, n, s, a):
    
    sendMarlMsg(sock, SNDBLK, j, n=n, sj=s, Aall=a)
    cmd, data = recvMarlMsg(sock)
    if debug==2: sys.stdout.write("["+str(j)+"] recvMarlMSG:::::: "+str((cmd,parseMsg(cmd, data)[0]))+'\n')
    if cmd==RSPBLK:
        #sendMarlMsg(sock, ACK, j, orgcmd=cmd) # ack 111
        return parseMsg(cmd,data)[1:] # return parsed data except j
    else: 
        if debug>=1: sys.stdout.write("["+repr(sock)+"] Wrong response: "+str(cmd)+"\n")
        return -1, None, None#, None




def waitMarl(sock):
    while True:
        cmd, _ = recvMarlMsg(sock)
        if cmd==READY:
            break;
        

