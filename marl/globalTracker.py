import sys
import marlComm as comm
from threading import Thread
from select import select
import numpy as np

debugopt = 0 #False #True #
debug2file=False

def debug(str,level):
    if debugopt >= level:
        if debug2file:
            with open('dbggt.txt','a+') as f:
                f.write(str)
        else:
            sys.stdout.write(str)

class GlobalTracker(Thread):

    def __init__(self, nagents, envmodel, port=3030):
        Thread.__init__(self)

        host = 'localhost'
        self.sock, self.port = comm.openSock(host,port)
        self.nagents=nagents
        self.agents= {}
        self.env = envmodel
        self.flgTerm = False

    def __del__(self):
        comm.closeSock(self.sock)

    def run(self):
        debug("starting GlobalTracker server process.\n",1)

        curT = -1  ## init the time step to sync. -1 means waiting for agents to join
        njoined = 0
        nactions = 0
        nagents = self.nagents

        comm.listenSock(self.sock)
        inset = [self.sock]
        while nagents>0: 
            if self.flgTerm:
                for s in inset:
                    s.close()
                break

            inrdy, outrdy, errrdy = select(inset,[],[],5)  # check term status every 5 secs

            for s in inrdy:
                if s == self.sock:
                    client, addr = self.sock.accept()
                    inset.append(client)
                else:
                    data = s.recv(comm.MAXBUF)
                    debug("<<<<<  "+repr(data)+"\n",2)
                    if not data:
                        debug("client has left!\n",1)
                        nagents -= 1
                        inset.remove(s)
                        if nagents == 0: 
                            debug("Finishing GlobalTracker...\n", 1)
                            break
                    else:
                        cmd = int(data[0])

                        if cmd==comm.SEND and curT>-1:
                            debug('recived SEND packet\n',1)
                            j, t, sj, aj = comm.parseMsg(cmd,data[1:])
                            debug(">>>"+ str((j,t,sj,aj))+"\n",1)
                            # collecting actions if (t-1)==curT:

                            if t-1==curT:
                                Sall[j] = sj 
                                Aall[j] = aj
                                nactions += 1

                                debug("CHECK: "+str((nactions,self.nagents))+"\n",1)
                                if nactions == self.nagents:
                                    # ask env to process joint action
                                    debug('------'+str((Sall,Aall))+"\n",1)
                                    S1all, Rg, tflg = self.env.next(Sall, Aall)
                                    # send response to all
                                    debug('RESP: '+str((t,Rg,S1all))+'\n',1)
                                    comm.replyMarlMsg(self.agents, comm.RESP, t, Rg, S1all,Aall.values(), tflg) 
                                    curT = t
                                    nactions = 0
                                    Sall, Aall = {}, {}
                                    debug("==========================================================="+str(curT)+"\n",1)

                        elif cmd==comm.SNDBLK and curT>-1:
                            debug('recived SNDBLK packet\n',1)
                            j, n, sj, ajs = comm.parseMsg(cmd, data[1:])
                            if n > 0: 
                                Sall[j] = sj
                                Aall[j] = ajs 
                                nactions += 1

                                if nactions == self.nagents:
                                    Salls, Rgs = self.env.next_all(Sall, Aall, n)

                                    
                                    debug('sending RSPBLK packet\n',1)
                                    comm.replyMarlMsg(self.agents, comm.RSPBLK, n, Rgs, Salls, Aall, tflg) 
                                    curT = curT + n
                                    nactions = 0
                                    Sall, Aall = {}, {}

                        elif cmd==comm.JOIN:
                            debug('recived JOIN packet\n',1)
                            j, s0 = comm.parseMsg(cmd,data[1:])
                            debug(str((j,s0))+'\n',1)
                            if j == -1: j = njoined
                            self.agents[j] = [s0, s]
                            njoined += 1
                            if njoined == self.nagents:
                                debug('multicasting ACK packet\n',1)

                                s0all = self.env.init()
                                if s0all is None:
                                    s0all = np.array([self.agents[k][0] for k in self.agents.keys()])
                                comm.multicastMarlMsg(self.agents, comm.JOINRSP, sall=s0all)
                                curT = 0
                                nactions = 0
                                Sall = {}
                                Aall = {}

                        elif cmd==comm.EXIT:
                            debug('recived EXIT packet\n',1)
                            j = comm.parseMsg(cmd, data[1:])
                            if comm.SyncExit: comm.sendMarlMsg(s, comm.ACK, j)
                            if njoined >0 and self.agents[j][0] is not None:
                                self.agents[j][0] = None # ready to clear
                                njoined -= 1
                                if njoined ==0:
                                    for k in self.agents.keys():
                                        # send ACK before clearing agent info
                                        comm.sendMarlMsg(self.agents[k][1], comm.ACK, j)
                                        debug('clearing agent '+str(k)+'\n',1)
                                        del(self.agents[k])
                                    curT = -1
                                    del(Sall)
                                    del(Aall)
                                    nactions = 0
                                
                        #elif cmd==comm.TERM:
                        #    if debug : sys.stdout.write('recived TERM packet\n')
                        #    self.nagents -= 1
                        #    if self.nagent == 0:
                        #        if debug: sys.stdout.write('all agents are terminated... exiting...\n')
                        #        break

        comm.closeSock(self.sock)
