import numpy as np
import marl.globalTracker as gt 
import marl.games.match as agent
from marl.learn.marltrain import SRL
from marl.learn.apq import APQ
from marl.learn.wpl import WPL
from marl.learn.gigawolf import GIGAWoLF
from marl.learn.exp3 import Exp3
from marl.learn.triv import MARandom
import marl.environ as env
import matplotlib.pyplot as plt
import sys


if __name__ == '__main__':

    if len(sys.argv) != 3:
        print "Usage: match_test option num_episodes"
        sys.exit(1)

    nreps =  int(sys.argv[2]) #100000
    nsteps = 1

    smooth_window = 1000
    optstr = [ "SRLvsRAND", "APQvsRAND", "SRLvsAPQ",\
                "WPLvsRAND", "SRLvsWPL", "WPL-self",\
                "GWFvsRAND", "SRLvsGWF", "GWF-self",\
                "Exp3vsRAND", "SRLvsExp3", "Exp3-self" ]

    option = optstr.index(sys.argv[1])
    # match option 
    SRLvsRAND, APQvsRAND, SRLvsAPQ,\
    WPLvsRAND, SRLvsWPL, WPLvsWPL,\
    GWFvsRAND, SRLvsGWF, GWFvsGWF,\
    Exp3vsRAND, SRLvsExp3, Exp3vsExp3 = range(len(optstr))
    #option = WPLvsWPL #GWFvsRAND
    print "testing " + optstr[option] + ".............."

    #totRtrace1 = None
    #totRtrace2 = None
    #totPtrace1 = None
    #totPtrace2 = None
    totResult = [None, None, None, None]
    for i  in xrange(1):

        # create SRL Match agent
        SRLagent = SRL(agent.MatchAgent(), agIdx=0)
        # creat NN network for SRL agent
        SRLagent.makeQNN([SRLagent.get_nnNI(), 5, 1])
        SRLagent.setParams(nReps=nreps, nSteps=nsteps)
        SRLagent.setProgressOn() # print out progress from this thread

        # create SRL Rand agent
        RANDagent = MARandom(agent.MatchAgent(), agIdx=1)
        RANDagent.setParams(nReps=nreps, nSteps=nsteps)

        # create APQ agent
        APQagent = APQ(agent.MatchAgent(), agIdx=0)
        APQagent.setParams(nReps=nreps, nSteps=nsteps)

        # create WPL agent
        WPLagent = WPL(agent.MatchAgent(), agIdx=0)
        WPLagent.setParams(nReps=nreps, nSteps=nsteps)
        WPLagent1 = WPL(agent.MatchAgent(), agIdx=1)
        WPLagent1.setParams(nReps=nreps, nSteps=nsteps)

        # create GIGA-WoLF agent
        GWFagent = GIGAWoLF(agent.MatchAgent(), agIdx=0)
        GWFagent.setParams(nReps=nreps, nSteps=nsteps)
        GWFagent1 = GIGAWoLF(agent.MatchAgent(), agIdx=1)
        GWFagent1.setParams(nReps=nreps, nSteps=nsteps)

        # create Exp3 agent
        Exp3agent = Exp3(agent.MatchAgent(), agIdx=0)
        Exp3agent.setParams(nReps=nreps, nSteps=nsteps)
        Exp3agent1 = Exp3(agent.MatchAgent(), agIdx=1)
        Exp3agent1.setParams(nReps=nreps, nSteps=nsteps)

        # create global tracker 
        matchEnv = env.Matchenv()
        gtracker = gt.GlobalTracker(2, matchEnv)

        #start global tracker thread
        gtracker.start()

        if option==SRLvsRAND:
            agentList = [SRLagent, RANDagent, gtracker]
            label1, label2 = 'SRL', 'Rand'
        elif option == APQvsRAND:
            agentList = [APQagent, RANDagent, gtracker]
            label1, label2 = 'APQ', 'Rand'
        elif option == WPLvsRAND:
            agentList = [WPLagent, RANDagent, gtracker]
            label1, label2 = 'WPL', 'Rand'
        elif option == GWFvsRAND:
            agentList = [GWFagent, RANDagent, gtracker]
            label1, label2 = 'GIGA-WoLF', 'Rand'
        elif option == Exp3vsRAND:
            agentList = [Exp3agent, RANDagent, gtracker]
            label1, label2 = 'Exp3', 'Rand'
        elif option == SRLvsAPQ :
            agentList = [SRLagent, APQagent, gtracker]
            label1, label2 = 'SRL', 'APQ'
        elif option == SRLvsWPL :
            agentList = [SRLagent, WPLagent, gtracker]
            label1, label2 = 'SRL', 'WPL'
        elif option == WPLvsWPL :
            agentList = [WPLagent, WPLagent1, gtracker]
            label1, label2 = 'WPL', 'WPL'
        elif option == GWFvsGWF :
            agentList = [GWFagent, GWFagent1, gtracker]
            label1, label2 = 'GIGA-WoLF', 'GIGA-WoLF'
        elif option == Exp3vsExp3 :
            agentList = [Exp3agent, Exp3agent1, gtracker]
            label1, label2 = 'Exp3', 'Exp3'

        agentList[0].agentIdx = 0
        agentList[1].agentIdx = 1
        agentList[0].start()
        agentList[1].start()

        for thr in agentList: 
            thr.join()
            #print "JOIN!!!"

        rtrace1 = agentList[0].rtrace
        rtrace2 = agentList[1].rtrace

        ptrace1 = agentList[0].ptrace
        ptrace2 = agentList[1].ptrace
        # plot results from each agent
        #print SRLagent.rtrace
        #print RANDagent.rtrace
        print 'Matching Pennies game test DONE!'
        ltrace = [rtrace1, rtrace2, ptrace1, ptrace2]
        for ti in xrange(len(totResult)):
            if totResult[ti] is None:
                totResult[ti] = np.array(ltrace[ti])
            else:
                totResult[ti] = np.hstack((totResult[ti], ltrace[ti]))

    if True:
        np.savez(optstr[option] + "_results.npz", tot=totResult, label1=label1, label2=label2)
    else:
        plt.clf()
        xs = range(1,len(totResult[0])+1)
        #plt.plot(xs, totRtrace1/10., 'r.-', label=label1)
        #plt.plot(xs, totRtrace2/10., 'g.-', label=label2)
        plt.plot(np.convolve(totResult[0],np.array([0.2]*smooth_window),mode='valid'), 'r', label=label1)
        plt.plot(np.convolve(totResult[1],np.array([0.2]*smooth_window),mode='valid'), 'b', label=label2)
        plt.xlabel("Number of games (/"+str(nsteps)+")")
        plt.ylabel("Rewards")
        plt.legend()
        #plt.show()
        plt.savefig("matching_pennies_rewards_" + optstr[option] +".pdf")

        plt.clf()
        #plt.plot(np.convolve(totResult[2],np.array([0.2]*smooth_window),mode='valid'), 'r', label=label1)
        #plt.plot(np.convolve(totResult[3],np.array([0.2]*smooth_window),mode='valid'), 'b', label=label2)
        plt.plot(totResult[2], 'r', label=label1)
        plt.plot(totResult[3], 'b', label=label2)
        plt.xlabel("Number of games (/"+str(nsteps)+")")
        plt.ylabel("Policy")
        plt.legend()
        plt.savefig("matching_pennies_policy_" + optstr[option] +".pdf")

    ##---------------------- Next Step
    # start agents  (S.RL vs APQ)

#def movingAvg(x,q=2):
    
