""" parameter test for prey-vs-predator game
"""
import os
import marl.globalTracker as gt 
import marl.games.PvPagent as agent
from marl.learn.marltrain import SRL, MARL
from marl.learn.marltrain import ORL
from marl.learn.apq import APQ
from marl.learn.wpl import WPL
from marl.learn.gigawolf import GIGAWoLF
from marl.learn.exp3 import Exp3
from marl.learn.triv import MARandom, MAStatic
import marl.environ as env

import matplotlib.pyplot as plt
import numpy as np
import random
import sys
from marl.utils.analMARL import storeArray, plotReward
import time
import shutil


def test_params(tgamma, talpha, teta):
    print "_____________________________"
    print "gamma: ", tgamma
    print "alpha: ", talpha
    print "eta: ", teta

    npreys = 1
    npreds = 2
    nagents = npreys+npreds
 
    Preys=[]
    Preds=[]
    agentList=[]

    if option == optSRL:
        # create SRL TD agent ###### Single RL ######
        TESTagent = SRL(agent.PreyAgent(npreys), agIdx=0)
        # creat NN network for SRL agent
        nH = 20
        TESTagent.makeQNN([TESTagent.get_nnNI(), nH, 1])
        TESTagent.setParams(nReps=nreps, nSteps=nsteps,
                            fixedEpsilon=0.3,
                            Lambda=0.01, Gamma=0.95, nIterations=10,
                            wPrecision=1e-4, fPrecision=1e-4,
                            record=nsteps)

    elif option == optAPQ:
        aPrey = agent.PreyAgent(npreys)
        nactions = len(aPrey.getActions(None))
        # A_j - create cartesian product of possible actions
        A_j=np.tile(np.array([range(1,nactions+1)]).T,(1,nagents-1))
        A_j=np.array([range(1,nactions+1)]).T
        for i in xrange(nagents-2):
            nr = A_j.shape[0]
            colm = np.repeat(range(1,nactions+1),nr)
            colm.shape=(len(colm),1)
            A_j = np.hstack((np.tile(A_j, (nactions,1)), colm))

        # create APQ agent
        TESTagent = APQ(aPrey, agIdx=0, 
                        A_j=A_j , expdec=0.9999)
        TESTagent.setParams(nReps=nreps, nSteps=nsteps,
                            #alpha=0.5, gamma=0.9,
                            alpha=talpha, gamma=tgamma,
                            nhistory=10, nsampling=10,
                            record=nsteps)

    elif option == optORL:
        # create ORL TD agent ###### Omnisient RL ######
        TESTagent = ORL(agent.PreyAgent(npreys,(nagents-1)*2),
                        agIdx=0, filter='none', abstract=absopt, nagents=nagents)
        # creat NN network for SRL agent
        nH = 20
        TESTagent.makeQNN([TESTagent.get_nnNI(), nH, 1])
        TESTagent.setParams(nReps=nreps, nSteps=nsteps,
                            fixedEpsilon=0.3,
                            Lambda=0.01, Gamma=0.95, nIterations=10,
                            wPrecision=1e-4, fPrecision=1e-4,
                            record=nsteps)

    elif option == optWPL:
        # create WPL agent
        TESTagent = WPL(agent.PreyAgent(npreys), agIdx=0)
        TESTagent.setParams(nReps=nreps, nSteps=nsteps,
                            record=nsteps)
        TESTagent.gamma = tgamma #0.9
        TESTagent.eta = teta #0.01
 
    elif option == optGWF:
        # create GIGA-WoLF agent
        TESTagent = GIGAWoLF(agent.PreyAgent(npreys), agIdx=0)
        TESTagent.setParams(nReps=nreps, nSteps=nsteps,
                            record=nsteps)
        TESTagent.gamma = tgamma #0.1
 
    elif option == optExp3:
        # create Exp3 agent
        TESTagent = Exp3(agent.PreyAgent(npreys), agIdx=0)
        TESTagent.setParams(nReps=nreps, nSteps=nsteps,
                            record=nsteps)

    TESTagent.setProgressOn()
    TESTagent.agentIdx = 0
    TESTagent.flgTerm = False
    Preys.append(TESTagent)
    agentList = [TESTagent]
    sti = 1

    # create static preys 
    for i in xrange(sti, npreys):
        Preys.append(MAStatic(agent.PreyAgent(npreys), agIdx=i))
        Preys[i].setParams(nReps=nreps, nSteps=nsteps)
        Preys[i].agentIdx = i
        Preys[i].flgTerm = False
        agentList.append(Preys[i])

    # Predators
    sti = 0
    for i in xrange(sti,npreds):
        # create Static Predators
        if i==sti:
            Preds.append(MAStatic(agent.PredAgent(npreys), agIdx=npreys+i))
            Preds[i].setParams(nReps=nreps, nSteps=nsteps)
            Preds[i].agentIdx = npreys+i
            Preds[i].flgTerm = False
            agentList.append(Preds[i])
        else:
            # create random predators
            Preds.append(MARandom(agent.PredAgent(npreys), agIdx=npreys+i))
            Preds[i].setParams(nReps=nreps, nSteps=nsteps)
            Preds[i].agentIdx = npreys+i
            Preds[i].flgTerm = False
            agentList.append(Preds[i])

    # create global tracker 
    Env = env.PvPenv([0] * npreys + [1] * npreds)
    gtracker = gt.GlobalTracker(nagents, Env)
    gtracker.flgTerm = False

    #start global tracker thread
    gtracker.start()
    for thr in agentList: 
        thr.start()

    #time.sleep(running_time) # running time in seconds
    #for thr in agentList: 
    #    thr.flgTerm = True
    #gtracker.flgTerm = True

    for thr in agentList: 
        thr.join()
    gtracker.join()

    # collect rewards 
    if TESTagent.rtrace is None:
        rAlltr = np.array([])
    else:
        rlen = len(TESTagent.rtrace)
        rAlltr = np.array([TESTagent.rtrace]).T

    print "mean: ", np.mean(TESTagent.rtrace), "last 10", np.mean(TESTagent.rtrace[-10:])
    print "complete: ", np.sum(TESTagent.completeGames), "/", len(TESTagent.completeGames)
    #print "nGames: ", TESTagent.nGames

    if False:
        fname= storeArray(rAlltr, optstr[option] + '-'+ str(nsteps)+ 'step_' + str(running_time) + "sec",
                            nactions, nagents, testn)
        shutil.move(fname, "pvp/")

    #if rlen<=1: 
    #    print "Stop testing more number of actions: reached lowest number of games"
    #    break


if __name__ == '__main__':

    running_time = 600
    #if len(sys.argv) != 3:
    #    print "Usage: prey_pred option test_number"
    #    sys.exit(1)

    nreps = 2000 #1000000
    nsteps =  500

    #testn =  int(sys.argv[2])

    optstr = [ "SRL", "APQ", "ORL", "WPL", "GWF", "Exp3" ]

    option = optstr.index(sys.argv[1])
    # match option 
    optSRL, optAPQ, optORL,\
    optWPL, optGWF, optExp3 = range(len(optstr))
    #option = WPLvsWPL #GWFvsRAND
    print "testing " + optstr[option] + ".............."

   
    #for nagents in testNagents:
    #for tgamma in test_gammas:
    #    for talpha in test_alphas:
    if option == optAPQ:
        #tgamma = float(sys.argv[3])
        #talpha = float(sys.argv[4])
        #print "gamma: ", tgamma
        #print "alpha: ", talpha
        test_alphas = [float(sys.argv[2])] #[0.2, 0.4, 0.6, 0.8, 0.9]
        test_gammas = [0.2, 0.5, 0.7, 0.9, 0.95, 1]
        for ta in test_alphas:
            for tg in test_gammas:
                test_params(tg, ta, 1)
    elif option == optWPL:
        #tgamma = float(sys.argv[3])
        #teta = float(sys.argv[4])
        #print "gamma: ", tgamma
        #print "eta: ", teta
        #test_gammas = [0.2, 0.5, 0.7, 0.9, 0.95, 1]
        t_gammas = [[0.2, 0.5], [0.7, 0.9], [0.95, 1]]
        test_gammas = t_gammas[int(sys.argv[3])]
        test_etas = [float(sys.argv[2])] #[0.001, 0.01, 0.1, 1, 10]
        for te in test_etas:
            for tg in test_gammas:
                test_params(tg, 1, te)
    elif option == optGWF:
        #tgamma = float(sys.argv[3])
        #print "gamma: ", tgamma
        test_gammas = [0.2, 0.5, 0.7, 0.9, 0.95, 1]
        for tg in test_gammas:
            test_params(tg, 1, 1)
 
