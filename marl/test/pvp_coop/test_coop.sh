#!/bin/bash
algos=(srl srld orl orld wpl gwf exp3)

for ((a=0; a<7 ; a++))
do
    for ((b=$a+1; b<7 ; b++))
    do
        echo "test coop: ${algos[$a]} ${algos[$b]}  _____ $1"
        nice -n 19 python coop.py ${algos[$a]} ${algos[$b]} $1
    done
done
