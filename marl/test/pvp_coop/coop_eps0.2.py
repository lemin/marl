import os
import marl.globalTracker as gt 
import marl.games.PvPagent as agent
from marl.learn.marltrain import SRL, MARL
from marl.learn.marltrain import ORL
from marl.learn.discrete import SRLD, ORLD
from marl.learn.apq import APQ
from marl.learn.wpl import WPL
from marl.learn.gigawolf import GIGAWoLF
from marl.learn.exp3 import Exp3
from marl.learn.triv import MARandom, MAStatic
import marl.environ as env

import matplotlib.pyplot as plt
import numpy as np
import random
import sys
from marl.utils.analMARL import storeArray, plotReward
import time
import shutil


def run_coop(algos, testn, nsteps, nreps, running_time, fsize):

    npreys = 1
    npreds = 2
    nagents = npreys+npreds
 
    Preys=[]
    Preds=[]
    agentList=[]

    SRLagent = SRL(agent.DiscPredAgent(npreys), agIdx=0)
    # creat NN network for SRL agent
    nH = 20
    SRLagent.makeQNN([SRLagent.get_nnNI(), nH, 1])
    SRLagent.setParams(nReps=nreps, nSteps=nsteps,
                        fixedEpsilon=0.2,
                        Lambda=0.01, Gamma=0.95, nIterations=10,
                        wPrecision=1e-4, fPrecision=1e-4,
                        record=nsteps)

    # create ORL TD agent ###### Omnisient RL ######
    ORLagent = ORL(agent.DiscPredAgent(npreys,(nagents-1)*2), agIdx=0)
    # creat NN network for SRL agent
    nH = 20
    ORLagent.makeQNN([ORLagent.get_nnNI(), nH, 1])
    ORLagent.setParams(nReps=nreps, nSteps=nsteps,
                        fixedEpsilon=0.2,
                        Lambda=0.01, Gamma=0.95, nIterations=10,
                        wPrecision=1e-4, fPrecision=1e-4,
                        record=nsteps)

    SRLDagent = SRLD(agent.DiscPredAgent(npreys), agIdx=0)
    SRLDagent.setParams(nReps=nreps, nSteps=nsteps,
                        fixedEpsilon=0.2,
                        Lambda=0.01, Gamma=0.95,
                        record=nsteps)

    ORLDagent = ORLD(agent.DiscPredAgent(npreys), agIdx=0)
    ORLDagent.setParams(nReps=nreps, nSteps=nsteps,
                        fixedEpsilon=0.2,
                        Lambda=0.01, Gamma=0.95,
                        record=nsteps)

    # create WPL agent
    WPLagent = WPL(agent.DiscPredAgent(npreys), agIdx=0)
    WPLagent.setParams(nReps=nreps, nSteps=nsteps,
                        record=nsteps)
    WPLagent.gamma = 0.95
    WPLagent.eta = 0.001

    # create GIGA-WoLF agent
    GWFagent = GIGAWoLF(agent.DiscPredAgent(npreys), agIdx=0)
    GWFagent.setParams(nReps=nreps, nSteps=nsteps,
                        record=nsteps)
    GWFagent.gamma = 0.95
 
    # create Exp3 agent
    Exp3agent = Exp3(agent.DiscPredAgent(npreys), agIdx=0)
    Exp3agent.setParams(nReps=nreps, nSteps=nsteps,
                        record=nsteps)


    cur_algos = []

    sti = 0
    # create static preys 
    for i in xrange(sti, npreys):
        Preys.append(MAStatic(agent.DiscPreyAgent(npreys), agIdx=i))
        Preys[i].setParams(nReps=nreps, nSteps=nsteps, record=nsteps)
        Preys[i].agentIdx = i
        Preys[i].flgTerm = False
        agentList.append(Preys[i])

        cur_algos.append('static')

    # Predators
    sti = 0
    i = 0
    for algo in algos:

        cur_algos.append(algo)
        if algo == 'srl':
            Preds.append(SRLagent)
        elif algo == 'orl':
            Preds.append(ORLagent)
        elif algo == 'srld':
            Preds.append(SRLDagent)
        elif algo == 'orld':
            Preds.append(ORLDagent)
        elif algo == 'wpl':
            Preds.append(WPLagent) 
        elif algo == 'gwf':
            Preds.append(GWFagent) 
        elif algo == 'exp3':
            Preds.append(Exp3agent)
        elif algo == 'rand':
            Preds.append(MARandom(agent.DiscPredAgent(npreys), agIdx=npreys+i))
            Preds[i].setParams(nReps=nreps, nSteps=nsteps, record=nsteps)

        Preds[i].agentIdx = npreys+i
        Preds[i].flgTerm = False
        agentList.append(Preds[i])
        i += 1

    for ag in agentList:
        ag.model.FieldSize = fsize

    # create global tracker 
    Env = env.DiscPvPenv([0] * npreys + [1] * npreds, fsize)
    gtracker = gt.GlobalTracker(nagents, Env)
    gtracker.flgTerm = False

    #start global tracker thread
    gtracker.start()
    for thr in agentList: 
        thr.start()

    if True:
        time.sleep(running_time)
        for thr in agentList:
            thr.flgTerm = True
        gtracker.flgTerm = True

    for thr in agentList: 
        thr.join()
    gtracker.join()

    # collect rewards 
    to_store = []
    rlen = np.min([len(ag.rtrace) for ag in agentList])
    i = 0
    for ag, salgo in zip(agentList, cur_algos):
        tmp = {}
        tmp['type'] = ag.model.ID 
        tmp['algo'] = salgo

        tmp['rtrace'] = np.asarray(ag.rtrace)  # all rewards
        tmp['survive'] = np.asarray(ag.completeGames)  # 0 or 1 for each episode
        tmp['ngames'] = np.asarray(ag.nGames)     # num of games played in each episode
        # to create mask later, np.repeat(survive, ngames)
        tmp['rlen'] = rlen
        to_store.append(tmp)
 
    #rAlltr = None
    #rlen = np.min([len(ag.rtrace) for ag in agentList])
    #for ag in agentList:
    #    if rAlltr is None:
    #        rAlltr = np.asarray(ag.rtrace[:rlen])
    #    else:
    #        rAlltr = np.vstack((rAlltr, np.asarray(ag.rtrace[:rlen])))
    #rAlltr = rAlltr.T
            
    #print "mean: ", np.mean(Preys[0].rtrace), "last 10", np.mean(Preys[0].rtrace[-10:])
    #print "mean: ", np.mean(Preds[0].rtrace), "last 10", np.mean(Preds[0].rtrace[-10:])
    #print "mean: ", np.mean(Preds[1].rtrace), "last 10", np.mean(Preds[1].rtrace[-10:])
    #print "complete: ", np.sum(Preds[0].completeGames), "/", len(Preds[0].completeGames)

    if True:
        fname= storeArray(to_store, "coop_"+ algos[0] + "_" + algos[1] + '-'+ str(nsteps)+ 'step_' + str(running_time) + "sec",
        #fname= storeArray(rAlltr, "coop_"+ algos[0] + "_" + algos[1] + '-'+ str(nsteps)+ 'step_' + str(running_time) + "sec",
                            5, nagents, testn)
        shutil.move(fname, "coop/")


if __name__ == '__main__':

    running_time = 900
    nreps = 2000
    nsteps =  500
    fsize = 50

    #for i in xrange(1):
    run_coop([sys.argv[1], sys.argv[2]], sys.argv[3],
                nsteps, nreps, running_time, fsize)
 
