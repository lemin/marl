""" Analyze the performance of coordination in prey-vs-predator game
        store the computed metrics into an matrix (.mat) file
        and use R plot codes that is previously built

                                by Jake Lee (lemin)
"""
import os
import string
import re
import sys
import numpy as np 
from marl.utils.analMARL import readResultDir, getAnalMetricsPvP, read_nsec
from scipy.io import loadmat, savemat


if sys.argv[1] is None:
    print "usage: anal_travel result_dir"
    sys.exit(1)

results = {}
for outdir in sys.argv[1:]:
    res = readResultDir(outdir)
    for k in res.keys():
        if res[k] is not None:
            results[k] = res[k]

nsec = read_nsec(outdir)
anal = {}
print np.sort(results.keys())
for k in np.sort(results.keys()):
    anal[k] = {}
    for i in xrange(len(results[k])): # nreps
        ret = getAnalMetricsPvP(results[k][i], nsec=nsec, ignore_last=True)

        j = 0
        for key, agnt in ret.items():
            if agnt['type'] == 0: # skip preys
                continue
            if not anal[k].has_key('prod_'+str(j)):
                anal[k]['prod_'+str(j)] = []
            if not anal[k].has_key('adap_'+str(j)):
                anal[k]['adap_'+str(j)] = []
            if not anal[k].has_key('mean_'+str(j)):
                anal[k]['mean_'+str(j)] = []
            anal[k]['prod_'+str(j)].append(agnt['prod'])
            anal[k]['adap_'+str(j)].append(agnt['adap'])
            anal[k]['mean_'+str(j)].append(agnt['mean'])
            j += 1

all_res = {}
for k in np.sort(anal.keys()):
    typen, nagents, nactions = string.split(k, '-')
    key_tuple = (int(nagents), int(nactions))
    if not all_res.has_key(key_tuple):
        all_res[key_tuple] = {}
    for ak in anal[k].keys():
        if not all_res[key_tuple].has_key(ak):
            if typen == 'coop_gwf_exp3':
                all_res[key_tuple][ak] = np.asarray(anal[k][ak])
            else:
                all_res[key_tuple][ak] = np.vstack((np.zeros(len(anal[k][ak])), anal[k][ak]))
        else:
            all_res[key_tuple][ak] = np.vstack((all_res[key_tuple][ak], anal[k][ak]))

for k in all_res.keys():
    nagents, nactions = k
    fname = ''.join([str(nagents),'-',str(nactions),'-resDat.mat'])
    savemat(fname, all_res[k])
    print fname, "created"
    #for ak in all_res[k].keys():
    #    print ak, all_res[key_tuple][ak].shape
