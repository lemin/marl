#!/bin/bash
algos=(srl srld orl orld wpl gwf exp3)
skip=true

for ((a=0; a<7 ; a++))
do
    for ((b=$a+1; b<7 ; b++))
    do
        if [[ ${algos[$a]} == "srld" && ${algos[$b]} == "gwf" ]]; then
            skip=false
        fi
        if $skip; then 
            continue
        fi
        echo "test coop: ${algos[$a]} ${algos[$b]}  ________________ $1"
        nice -n 19 python coop.py ${algos[$a]} ${algos[$b]} $1
    done
done
