#!/bin/bash

echo $1

#sleep 5400
echo "======================================================="
date
echo "starting experiment..."
if [[ $1 == "SRL" ]]; then
    # run both SRL and ORL: est. time 900 min 
    nice -n 19 python travelers_dilemma.py SRL $2

elif [[ $1 == "SRLD" ]]; then
    nice -n 19 python travelers_dilemma.py SRLD $2

elif [[ $1 == "ORLD" ]]; then
    nice -n 19 python travelers_dilemma.py ORLD $2

elif [[ $1 == "ORL" ]]; then
    nice -n 19 python travelers_dilemma.py ORL $2

elif [[ $1 == "APQ" ]]; then
    # run APQ: est. time 450 min 
    nice -n 19 python travelers_dilemma.py APQ $2

    #sleep 30
    #echo "======================================================="
    #date
    #nice -n 19 python travelers_dilemma.py ORL $2

elif [[ $1 == "WPL" ]]; then
    nice -n 19 python travelers_dilemma.py WPL $2

elif [[ $1 == "GWF" ]]; then
    nice -n 19 python travelers_dilemma.py GWF $2

elif [[ $1 == "Exp3" ]]; then
    nice -n 19 python travelers_dilemma.py Exp3 $2
    #test=$(($2 + 5))
    #sleep 30
    #echo "======================================================="
    #date
    #nice -n 19 python travelers_dilemma.py Exp3 $test
fi
echo "======================================================="

