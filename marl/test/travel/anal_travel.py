""" Analyze the performance of travelers' dilemma games
        store the computed metrics into an matrix (.mat) file
        and use R plot codes that is previously built

                                by Jake Lee (lemin)
"""
import os
import string
import re
import sys
import numpy as np 
from marl.utils.analMARL import readResultDir, getAnalMetrics
from scipy.io import loadmat, savemat


if sys.argv[1] is None:
    print "usage: anal_travel result_dir"
    sys.exit(1)

results = {}
for outdir in sys.argv[1:]:
    res = readResultDir(outdir)
    for k in res.keys():
        if res[k] is not None:
            if len(res[k][0].shape) == 1:
                import pdb; pdb.set_trace()
                tmp = string.split(k, '-')
                nagent = int(tmp[1])
                results[k] = [r.reshape((nagent, -1)).T for r in res[k]]
            else:
                results[k] = res[k]

anal = {}
for k in np.sort(results.keys()):
    anal[k] = {'adap':[], 'adap1':[], 'prod':[], 'mean':[], 'radap':[] }

    for i in xrange(len(results[k])):
        adap, adap1, prod, adapMean, radap = getAnalMetrics(results[k][i], k)
        anal[k]['adap'].append(adap)
        anal[k]['adap1'].append(adap1)
        anal[k]['prod'].append(prod)
        anal[k]['mean'].append(adapMean)
        anal[k]['radap'].append(radap)

all_res = {}
for k in np.sort(anal.keys()):
    print k
    typen, nagents, nactions = string.split(k, '-')
    key_tuple = (int(nagents), int(nactions))
    if not all_res.has_key(key_tuple):
        all_res[key_tuple] = {}
    for ak in anal[k].keys():
        if not all_res[key_tuple].has_key(ak):
            if typen == 'APQ':
                all_res[key_tuple][ak] = np.asarray(anal[k][ak])
            else:
                all_res[key_tuple][ak] = np.vstack((np.zeros(len(anal[k][ak])), anal[k][ak]))
        else:
            all_res[key_tuple][ak] = np.vstack((all_res[key_tuple][ak], anal[k][ak]))

for k in all_res.keys():
    nagents, nactions = k
    fname = ''.join([str(nagents),'-',str(nactions),'-resDat.mat'])
    savemat(fname, all_res[k])
    print fname, "created"
    #for ak in all_res[k].keys():
    #    print ak, all_res[key_tuple][ak].shape
