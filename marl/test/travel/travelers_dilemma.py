import os
import marl.globalTracker as gt 
import marl.games.TDagent as agent
from marl.learn.marltrain import SRL, MARL
from marl.learn.marltrain import ORL
from marl.learn.discrete import SRLD, ORLD
from marl.learn.apq import APQ
from marl.learn.wpl import WPL
from marl.learn.gigawolf import GIGAWoLF
from marl.learn.exp3 import Exp3
from marl.learn.triv import MASequence
import marl.environ as env

#import matplotlib.pyplot as plt
import numpy as np
import random
import sys
from marl.utils.analMARL import storeArray, plotReward
import time
import shutil


def createSeqPool(nactions):
    seqPool = []
    seqPool.append(range(1,nactions+1))
    seqPool.append(range(nactions,0,-1))
    seqPool.append(range(2,nactions+1,2)+range(1,nactions+1,2))
    seqPool.append(range(1,nactions+1,2)+range(2,nactions+1,2))
    seqPool.append(range(nactions,0,-2)+range(nactions-1,0,-2))
    seqPool.append(range(nactions-1,0,-2)+range(nactions,0,-2))
    seqPool.append(range(1,nactions+1)+range(nactions,0,-1))
    seqPool.append(range(nactions,0,-1)+range(1,nactions+1))
    return seqPool


if __name__ == '__main__':

    running_time = 600
    if len(sys.argv) != 3:
        print "Usage: travelers_dilemma option repeat"
        sys.exit(1)

    nreps = 20000  # max games in 600 sec was around 15000
    nsteps = 1

    testn =  int(sys.argv[2])

    optstr = [ "SRL", "APQ", "ORL", "WPL", "GWF", "Exp3", "SRLD", "ORLD" ]

    option = optstr.index(sys.argv[1])
    # match option 
    optSRL, optAPQ, optORL,\
    optWPL, optGWF, optExp3,\
    optSRLD, optORLD = range(len(optstr))
    #option = WPLvsWPL #GWFvsRAND
    print "testing " + optstr[option] + ".............."

    totRtrace1 = None
    totRtrace2 = None

    if option == optSRL or option == optORL:
        nsteps = 1
    elif option == optSRLD or option == optORLD:
        nsteps = 2
    print "nsteps", nsteps
    if option == optAPQ:
        testNagents = [2,4,6] # test error from >=8
        nsteps = 2
    else:
        testNagents = [2,4,6,8,10,15,20,25,30]
    testNactions = [4,6,8,10]

    bstart = True #False
    start = (4, 4)
    for nagents in testNagents:
        for nactions in testNactions:
            if not bstart and (nagents < start[0] or nactions < start[1]):
                print "skip..", nagents, nactions
                continue
            else:
                bstart = True

            seqpool = createSeqPool(nactions)
            SEQagents = []
            print "_____________________________"+str((nagents,nactions))+"\n"

            if option == optSRL:
                # create SRL TD agent ###### Single RL ######
                TESTagent = MARL(agent.TDAgent(nactions), agIdx=0, expdec=0.9999)
                # creat NN network for SRL agent
                TESTagent.makeQNN([TESTagent.get_nnNI(), 5, 1])
                #TESTagent.setProgressOn() # print out progress from this thread
            elif option == optSRLD:
                TESTagent = SRLD(agent.TDAgent(nactions), agIdx=0, expdec=0.9999)
            elif option == optORLD:
                TESTagent = ORLD(agent.TDAgent(nactions), agIdx=0, expdec=0.9999)
            elif option == optAPQ:
                # A_j - create cartesian product of possible actions
                A_j=np.tile(np.array([range(1,nactions+1)]).T,(1,nagents-1))
                A_j=np.array([range(1,nactions+1)]).T
                for i in xrange(nagents-2):
                    nr = A_j.shape[0]
                    colm = np.repeat(range(1,nactions+1),nr)
                    colm.shape=(len(colm),1)
                    A_j = np.hstack((np.tile(A_j, (nactions,1)), colm))

                # create APQ agent
                TESTagent = APQ(agent.TDAgent(nactions, nagents-1), agIdx=0, 
                                A_j=A_j , expdec=0.9999)
            elif option == optORL:
                # create ORL TD agent ###### Omnisient RL ######
                TESTagent = ORL(agent.TDAgent(nactions, nagents-1), agIdx=0, 
                                nagents=nagents, expdec=0.9999)
                # creat NN network for SRL agent
                TESTagent.makeQNN([TESTagent.get_nnNI(), 5, 1])
     
            elif option == optWPL:
                # create WPL agent
                TESTagent = WPL(agent.TDAgent(nactions, nagents-1), agIdx=0)
                TESTagent.gamma = 0.9
                TESTagent.eta = 0.01
         
            elif option == optGWF:
                # create GIGA-WoLF agent
                TESTagent = GIGAWoLF(agent.TDAgent(nactions, nagents-1), agIdx=0)
                TESTagent.gamma = 0.1
         
            elif option == optExp3:
                # create Exp3 agent
                TESTagent = Exp3(agent.TDAgent(nactions, nagents-1), agIdx=0)
         
            TESTagent.setParams(nReps=nreps, nSteps=nsteps)
            TESTagent.agentIdx = 0
            TESTagent.flgTerm = False
            agentList = [TESTagent]

            # create Sequence Plyaers
            for i in xrange(nagents-1):
                SEQagents.append(MASequence(agent.TDAgent(nactions), agIdx=i+1))
                SEQagents[i].setParams(nReps=nreps, nSteps=nsteps)
                SEQagents[i].setPolicy(seqpool[random.sample(range(len(seqpool)),1)[0]])
                SEQagents[i].agentIdx = i+1
                SEQagents[i].flgTerm = False
                agentList.append(SEQagents[i])

            # create global tracker 
            Env = env.TDenv(nactions)
            gtracker = gt.GlobalTracker(nagents, Env)
            gtracker.flgTerm = False

            #start global tracker thread
            gtracker.start()

            TESTagent.start()
            for i in xrange(nagents-1):
                SEQagents[i].start()

            time.sleep(running_time) # running time in seconds

            for thr in agentList: 
                thr.flgTerm = True
            gtracker.flgTerm = True

            for thr in agentList: 
                thr.join()
            gtracker.join()

            # collect rewards 
            if TESTagent.rtrace is None:
                rAlltr = np.array([]).T
                print "NONE"
                rlen = 0
            else:
                print "TEST", len(TESTagent.rtrace)
                rlen = np.min([len(ag.rtrace) for ag in agentList])
                rAlltr = np.array([TESTagent.rtrace[:rlen]]).T
            print "MIN", rlen
            for seqag in SEQagents:
                print "rAlltr.shape = ", rAlltr.shape, "seq shape", np.asarray(seqag.rtrace).shape
                rAlltr = np.hstack((rAlltr, np.asarray([seqag.rtrace[:rlen]]).T))

            fname= storeArray(rAlltr, optstr[option] + '-'+ str(nsteps)+ 'step_' + str(running_time) + "sec",
                                nactions, nagents, testn)
            shutil.copy(fname, "DiscRL/")
            os.remove(fname)

            if rlen<=1: 
                print "Stop testing more number of actions: reached lowest number of games"
                break




