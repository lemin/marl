#!/bin/bash
preys=(none filt1 filt2 all)

for a in ${preys[*]}
do
    echo "test filt: $a  _________________ $1"
    nice -n 19 python filt.py $a rand $1
done
