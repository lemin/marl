""" test filtering heuristics on prey-vs-predator game
"""
import os
import marl.globalTracker as gt 
import marl.games.PvPagent as agent
from marl.learn.marltrain import SRL, MARL
from marl.learn.marltrain import ORL
from marl.learn.discrete import SRLD, ORLD
from marl.learn.apq import APQ
from marl.learn.wpl import WPL
from marl.learn.gigawolf import GIGAWoLF
from marl.learn.exp3 import Exp3
from marl.learn.triv import MARandom, MAStatic
import marl.environ as env

import matplotlib.pyplot as plt
import numpy as np
import random
import sys
from marl.utils.analMARL import storeArray, plotReward
import time
import shutil




def run_filt(algo, start_loc, testn, nsteps, nreps, running_time, fsize, eps):

    npreys = 1
    npreds = 3
    nagents = npreys+npreds
 
    Preys=[]
    Preds=[]
    agentList=[]
    nH = 20

    s_sloc = start_loc
    if start_loc != 'rand':
        start_loc = [50, 50]

#    SRLpred = SRL(agent.PredAgent(npreys), agIdx=0)
#    # creat NN network for SRL agent
#    SRLpred.makeQNN([SRLpred.get_nnNI(), nH, 1])
#    SRLpred.setParams(nReps=nreps, nSteps=nsteps,
#                        fixedEpsilon=eps,
#                        Lambda=0.01, Gamma=0.95, nIterations=10,
#                        wPrecision=1e-4, fPrecision=1e-4,
#                        record=nsteps)

    SRLprey = SRL(agent.PreyAgent(npreys, start=start_loc), agIdx=0)
    SRLprey.makeQNN([SRLprey.get_nnNI(), nH, 1])
    SRLprey.setParams(nReps=nreps, nSteps=nsteps,
                        fixedEpsilon=eps,
                        Lambda=0.01, Gamma=0.95, nIterations=10,
                        wPrecision=1e-4, fPrecision=1e-4,
                        record=nsteps)

    nincluded = 1
    FILT2prey = MARL(agent.PreyAgent(npreys, nincluded*2, start=start_loc), agIdx=0, 
                      filter='1', nagents=nagents)  # filter number means # of inclusion
    FILT2prey.makeQNN([FILT2prey.get_nnNI(), nH, 1])
    FILT2prey.setParams(nReps=nreps, nSteps=nsteps,
                        fixedEpsilon=eps,
                        Lambda=0.01, Gamma=0.95, nIterations=10,
                        wPrecision=1e-4, fPrecision=1e-4,
                        record=nsteps)

    nincluded = 2
    FILT1prey = MARL(agent.PreyAgent(npreys, nincluded*2, start=start_loc), agIdx=0, 
                      filter='2', nagents=nagents)
    FILT1prey.makeQNN([FILT1prey.get_nnNI(), nH, 1])
    FILT1prey.setParams(nReps=nreps, nSteps=nsteps,
                        fixedEpsilon=eps,
                        Lambda=0.01, Gamma=0.95, nIterations=10,
                        wPrecision=1e-4, fPrecision=1e-4,
                        record=nsteps)


    ORLprey = ORL(agent.PreyAgent(npreys,(nagents-1)*2, start=start_loc), agIdx=0)
    # creat NN network for SRL agent
    nH = 20
    ORLprey.makeQNN([ORLprey.get_nnNI(), nH, 1])
    ORLprey.setParams(nReps=nreps, nSteps=nsteps,
                        fixedEpsilon=eps,
                        Lambda=0.01, Gamma=0.95, nIterations=10,
                        wPrecision=1e-4, fPrecision=1e-4,
                        record=nsteps)


    cur_algos = []

    # create preys 
    i = 0 
    if algo == 'all':
        Preys.append(SRLprey)
    elif algo == 'none':
        Preys.append(ORLprey)
    elif algo == 'filt1':
        Preys.append(FILT1prey)
    elif algo == 'filt2':
        Preys.append(FILT2prey)
    elif algo == 'rand':
        Preys.append(MARandom(agent.PreyAgent(npreys, start=start_loc), agIdx=i))
        Preys[i].setParams(nReps=nreps, nSteps=nsteps, record=nsteps)
    Preys[i].agentIdx = i
    Preys[i].flgTerm = False
    agentList.append(Preys[i])
    cur_algos.append(algo)

    # Predators
    i = 0
    sti=0
    # first agent as static and the others as random
    for i in xrange(sti,npreds): 
        # create Static Predators
        if i==sti:
            Preds.append(MAStatic(agent.PredAgent(npreys), agIdx=npreys+i))
            cur_algos.append('static')

        else:
            # create random predators
            Preds.append(MARandom(agent.PredAgent(npreys), agIdx=npreys+i))
            cur_algos.append('random')

        Preds[i].setParams(nReps=nreps, nSteps=nsteps, record=nsteps)
        Preds[i].agentIdx = npreys+i
        Preds[i].flgTerm = False
        agentList.append(Preds[i])

    for ag in agentList:
        ag.model.FieldSize = fsize

    # create global tracker 
    Env = env.PvPenv([0] * npreys + [1] * npreds, fsize)
    gtracker = gt.GlobalTracker(nagents, Env)
    gtracker.flgTerm = False

    #start global tracker thread
    gtracker.start()
    for thr in agentList: 
        thr.start()

    if True:
        time.sleep(running_time)
        for thr in agentList:
            thr.flgTerm = True
        gtracker.flgTerm = True

    for thr in agentList: 
        thr.join()
    gtracker.join()

    # collect rewards 
    to_store = []
    rlen = np.min([len(ag.rtrace) for ag in agentList])
    i = 0
    for ag, salgo in zip(agentList, cur_algos):
        tmp = {}
        tmp['type'] = ag.model.ID 
        tmp['algo'] = salgo

        tmp['rtrace'] = np.asarray(ag.rtrace)  # all rewards
        tmp['survive'] = np.asarray(ag.completeGames)  # 0 or 1 for each episode
        tmp['ngames'] = np.asarray(ag.nGames)     # num of games played in each episode
        # to create mask later, np.repeat(survive, ngames)
        tmp['rlen'] = rlen
        to_store.append(tmp)
        
    if True:
        fname= storeArray(to_store, "filt_"+ str(eps) +"_" + algo + '-'+ str(nsteps)+ 'step_' + str(running_time) + "sec",
                            5, nagents, testn)
        shutil.move(fname, "filt/"+s_sloc)


if __name__ == '__main__':

    running_time = 900
    nreps = 2000
    nsteps = 500
    eps = 0.2
    fsize = 100
    start_loc = sys.argv[2]

    run_filt(sys.argv[1], start_loc, sys.argv[3],
             nsteps, nreps, running_time, fsize, eps)
 
