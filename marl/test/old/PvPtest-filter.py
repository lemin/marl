import os
import globalTracker as gt 
import PvPagent as agent
import marltrain as marl
import environ as env
import matplotlib.pyplot as plt
import numpy as np
import random
import pdb
from analMARL import storeArray, plotReward
import time
import sys
from scipy.io import loadmat, savemat


#if True:

#for trial in xrange(3):

filtopt = sys.argv[1]    
options = ['none', 'all','1','2' ]
if filtopt not in options:
    print "Usage: cmd ", options
    sys.exit(1)

for trial in xrange(20):

    print "trial=",trial

    ####### FIXME: 04/19/11 currently RL cannot find opt. policy
    #######         Try Indicator on NN and see if it can learn !!! 
    #######         If it works, try if there is a way to develop 
    #######             continous state/action handling with indicator by some kind of interpolation

    nreps = 2000
    nsteps=500
    lmbd = 0.01
    gamma = 0.95 # 0.9
    wpre = 1e-4
    fpre = 1e-4
    niter = 10
    nH=20

    npreys = 1
    npreds = 3
    nagents = npreys+npreds
    
    Preys=[]
    Preds=[]
    agentList=[]

    if True:
        # create SRL Prey agent ###### Single RL ######
        if filtopt=='none':
            Preys.append(marl.MARLtrain(agent.PreyAgent(npreys,(nagents-1)*2), agIdx=0,filter=filtopt,nagents=nagents))
        elif filtopt=='all':
            Preys.append(marl.MARLtrain(agent.PreyAgent(npreys), agIdx=0)) 
        else: # 1 2
            nfiltered = int(filtopt)
            Preys.append(marl.MARLtrain(agent.PreyAgent(npreys,nfiltered*2), agIdx=0,filter=filtopt,nagents=nagents))

        # creat NN network for SRL agent
        #### discrete action test to check learning
        if False:
            Preys[0].discAct =True
            Preys[0].model.nnNI -= 2
            Preys[0].makeQNN([Preys[0].get_nnNI(), nH, len(Preys[0].model.getActions(None))])
        else:
            Preys[0].makeQNN([Preys[0].get_nnNI(), nH, 1])
        Preys[0].setParams(nReps=nreps, nSteps=nsteps,
                            lmbd=lmbd, gamma=gamma, nIterations=niter, 
                            wPrecision=wpre, fPrecision=fpre)
        #Preys[0].setProgressOn() # print out progress from this thread
        Preys[0].agentIdx = 0
        Preys[0].flgTerm = False
        agentList.append(Preys[0])
        sti=1
    else:
        sti=0

    # create Static Preys
    for i in xrange(sti,npreys):
        Preys.append(marl.MAStatic(agent.PreyAgent(npreys), agIdx=i))
        Preys[i].setParams(nReps=nreps, nSteps=nsteps)
        Preys[i].agentIdx = i
        Preys[i].flgTerm = False
        agentList.append(Preys[i])


    # create SRL Predator agent ###### Single RL ######
    if False:
        Preds.append(marl.MARLtrain(agent.PredAgent(npreys), agIdx=npreys))
        # creat NN network for SRL agent
        Preds[0].makeQNN([Preds[0].get_nnNI(), nH, 1])
        Preds[0].setParams(nReps=nreps, nSteps=nsteps,
                            lmbd=lmbd, gamma=gamma, nIterations=niter, 
                            wPrecision=wpre, fPrecision=fpre)
        #Preds.setProgressOn() # print out progress from this thread
        Preds[0].agentIdx = npreys
        Preds[0].flgTerm = False
        agentList.append(Preds[0])
        sti=1
    else:
        sti=0

    for i in xrange(sti,npreds):
        # create Static Predators
        if i==sti:
            Preds.append(marl.MAStatic(agent.PredAgent(npreys), agIdx=npreys+i))
            Preds[i].setParams(nReps=nreps, nSteps=nsteps)
            Preds[i].agentIdx = npreys+i
            Preds[i].flgTerm = False
            agentList.append(Preds[i])
        else:
            # create random predators
            Preds.append(marl.MARandom(agent.PredAgent(npreys), agIdx=npreys+i))
            Preds[i].setParams(nReps=nreps, nSteps=nsteps)
            Preds[i].agentIdx = npreys+i
            Preds[i].flgTerm = False
            agentList.append(Preds[i])


    # create global tracker 
    pvpEnv = env.PvPenv([0]*npreys+[1]*npreds)
    gtracker = gt.GlobalTracker(nagents, pvpEnv)
    gtracker.flgTerm = False

    #start global tracker thread
    gtracker.start()

    for i in xrange(len(agentList)):
        print "starting", str(agentList[i].agentIdx)
        agentList[i].start()

    time.sleep(900) # 15 minutes

    for thr in agentList: 
        thr.flgTerm = True
    gtracker.flgTerm = True

    for thr in agentList: 
        thr.join()
    gtracker.join()

    print "Done"

    # collect rewards and survival vector for prey
    stomat = {} 
    stomat['rtrace'] = Preys[0].rtrace 
    stomat['survive'] = Preys[0].completeGames
    savemat('filter_'+filtopt+"-"+str(trial)+'.mat', stomat)


    #print Preys[0].xtrace
    #print "-----------------------------------------------"
    #print Preds[0].xtrace

    if False:
        nLen = [f.shape[0] for f in Preys[0].xtrace]
        plt.clf()
        plt.subplot(2,1,1)
        plt.plot(nLen)
        plt.title("# steps survived")
        plt.subplot(2,1,2)
        plt.plot(Preys[0].rtrace)
        plt.title("avg. Rewards")
        #plt.show()
        plt.savefig("PvPlearn-filter_"+str(filtopt)+"-"+str(trial)+".png")

    # animation to see the movement
    if False:
        plt.ion()
        lim=  min(len(Preys[0].xtrace[0]), len(Preds[0].xtrace[0]))
        for i in xrange(lim):
            plt.clf()
            preyx, preyy = Preys[0].xtrace[0][i,:2]
            predx, predy = Preds[0].xtrace[0][i,:2]
            plt.plot(preyx, preyy, 'ro')
            plt.axis([0,agent.FieldSize,0,agent.FieldSize])
            plt.plot(predx, predy, 'bx')

            #preyx1, preyy1 = Preys[1].xtrace[0][i,:2]
            #plt.plot(preyx1, preyy1, 'yo')
            #predx1, predy1 = Preds[1].xtrace[0][i,:2]
            #plt.plot(predx1, predy1, 'gx')

            plt.draw()
            time.sleep(.1)

        xtr = {}
        xtr['prey0'] = Preys[0].xtrace[0][:,:2]
        #xtr['prey1'] = Preys[1].xtrace[0][:,:2]
        xtr['pred0'] = Preds[0].xtrace[0][:,:2]
        #xtr['pred1'] = Preds[1].xtrace[0][i,:2]
        savemat('xtraceNofilter1.mat', xtr)


if False:
    mat = loadmat('xtraceNofilter1.mat')
    Prey0xtr = mat['prey0']
    Pred0xtr = mat['pred0']
    Pred1xtr = mat['pred1']
    plt.ion()
    for i in xrange(len(Prey0xtr)):
        plt.clf()
        preyx, preyy = Prey0xtr
        predx, predy = Pred0xtr
        plt.plot(preyx, preyy, 'ro')
        plt.axis([0,agent.FieldSize,0,agent.FieldSize])
        plt.plot(predx, predy, 'bx')

        #preyx1, preyy1 = Preys[1].xtrace[0][i,:2]
        #plt.plot(preyx1, preyy1, 'yo')
        predx1, predy1 = Pred1xtr
        plt.plot(predx1, predy1, 'gx')

        plt.draw()
        time.sleep(.1)


    #fname= storeArray(rAlltr, 'SRLbulk-'+str(nsteps)+'step', nactions, nagents)

    #os.system("scp "+fname+" lemin@conejos.cs.colostate.edu:~/conejos/exp/output/0408/SRLbulk")
    #os.system("rm "+fname)




