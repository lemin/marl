import numpy as np
import marl.globalTracker as gt 
import marl.games.RPSagent as agent
from marl.learn.marltrain import SRL
from marl.learn.apq import APQ
from marl.learn.wpl import WPL
from marl.learn.gigawolf import GIGAWoLF
from marl.learn.exp3 import Exp3
from marl.learn.triv import MARandom
import marl.environ as env
import matplotlib.pyplot as plt


if __name__ == '__main__':

    # match option 
    SRLvsRAND, APQvsRAND, SRLvsAPQ, \
    WPLvsRAND, SRLvsWPL, \
    GWFvsRAND, SRLvsGWF, \
    Exp3vsRAND, SRLvsEXP3 = range(9)
    option = WPLvsRAND

    totRtrace1 = None
    totRtrace2 = None
    for i  in xrange(1):
        nreps = 1000
        nsteps = 1

        # create SRL RPS agent
        SRLagent = SRL(agent.RPSAgent(), agIdx=0)
        # creat NN network for SRL agent
        SRLagent.makeQNN([SRLagent.get_nnNI(), 5, 1])
        SRLagent.setParams(nReps=nreps, nSteps=nsteps)
        SRLagent.setProgressOn() # print out progress from this thread

        # create SRL Rand agent
        RANDagent = MARandom(agent.RPSAgent(), agIdx=1)
        RANDagent.setParams(nReps=nreps, nSteps=nsteps)

        # create APQ agent
        APQagent = APQ(agent.RPSAgent(), agIdx=0)
        APQagent.setParams(nReps=nreps, nSteps=nsteps)

        # create WPL agent
        WPLagent = WPL(agent.RPSAgent(), agIdx=0)
        WPLagent.setParams(nReps=nreps, nSteps=nsteps)

        # create GIGA-WoLF agent
        GWFagent = GIGAWoLF(agent.RPSAgent(), agIdx=0)
        GWFagent.setParams(nReps=nreps, nSteps=nsteps)

        # create WPL agent
        Exp3agent = Exp3(agent.RPSAgent(), agIdx=0)
        Exp3agent.setParams(nReps=nreps, nSteps=nsteps)

        # create global tracker 
        rpsEnv = env.RPSenv()
        gtracker = gt.GlobalTracker(2, rpsEnv)

        #start global tracker thread
        gtracker.start()

        if option==SRLvsRAND:
            # start agents  (S.RL vs Random)
            SRLagent.agentIdx = 0
            RANDagent.agentIdx = 1
            SRLagent.start()
            RANDagent.start()
            agentList = [SRLagent, RANDagent, gtracker]
        elif option == APQvsRAND:
            # start agents  (APQ vs Random)
            APQagent.agentIdx = 0
            RANDagent.agentIdx = 1
            APQagent.start()
            RANDagent.start()
            agentList = [APQagent, RANDagent, gtracker]
        elif option == WPLvsRAND:
            WPLagent.agentIdx = 0
            RANDagent.agentIdx = 1
            WPLagent.start()
            RANDagent.start()
            agentList = [WPLagent, RANDagent, gtracker]
        elif option == GWFvsRAND:
            GWFagent.agentIdx = 0
            RANDagent.agentIdx = 1
            GWFagent.start()
            RANDagent.start()
            agentList = [GWFagent, RANDagent, gtracker]
        elif option == Exp3vsRAND:
            Exp3agent.agentIdx = 0
            RANDagent.agentIdx = 1
            Exp3agent.start()
            RANDagent.start()
            agentList = [Exp3agent, RANDagent, gtracker]
        elif option == SRLvsAPQ :
            SRLagent.agentIdx = 0
            APQagent.agentIdx = 1
            SRLagent.start()
            APQagent.start()
            agentList = [SRLagent, APQagent, gtracker]
        elif option == SRLvsWPL :
            SRLagent.agentIdx = 0
            WPLagent.agentIdx = 1
            SRLagent.start()
            WPLagent.start()
            agentList = [SRLagent, WPLagent, gtracker]

        for thr in agentList: 
            thr.join()
            #print "JOIN!!!"

        if option==SRLvsRAND:
            rtrace1 = SRLagent.rtrace
            label1 = 'SRL'
            rtrace2 = RANDagent.rtrace
            label2 = 'Rand'
        elif option == APQvsRAND:
            rtrace1 = APQagent.rtrace
            label1 = 'APQ'
            rtrace2 = RANDagent.rtrace
            label2 = 'Rand'
        elif option == WPLvsRAND:
            rtrace1 = WPLagent.rtrace
            label1 = 'WPL'
            rtrace2 = RANDagent.rtrace
            label2 = 'Rand'
        elif option == GWFvsRAND:
            rtrace1 = GWFagent.rtrace
            label1 = 'GIGA-WoLF'
            rtrace2 = RANDagent.rtrace
            label2 = 'Rand'
        elif option == Exp3vsRAND:
            rtrace1 = Exp3agent.rtrace
            label1 = 'Exp3'
            rtrace2 = RANDagent.rtrace
            label2 = 'Rand'
        elif option == SRLvsAPQ :
            rtrace1 = SRLagent.rtrace
            label1 = 'SRL'
            rtrace2 = APQagent.rtrace
            label2 = 'APQ'
        elif option == SRLvsWPL :
            rtrace1 = SRLagent.rtrace
            label1 = 'SRL'
            rtrace2 = WPLagent.rtrace
            label2 = 'WPL'
            
        # plot results from each agent
        #print SRLagent.rtrace
        #print RANDagent.rtrace
        print 'RPS game test DONE!'
        if totRtrace1 is None:
            totRtrace1 = np.array(rtrace1)
        else:
            #totRtrace1 = totRtrace1 + np.array(rtrace1)
            totRtrace1 = np.hstack((totRtrace1, rtrace1))
        if totRtrace2 is None:
            totRtrace2 = np.array(rtrace2)
        else:
            #totRtrace2 = totRtrace2 + np.array(rtrace2)
            totRtrace2 = np.hstack((totRtrace2, rtrace2))

    if False:
        np.savez("rtraces.npz", totRtrace1, totRtrace2)
    else:
        plt.clf()
        xs = range(1,len(totRtrace1)+1)
        #plt.plot(xs, totRtrace1/10., 'r.-', label=label1)
        #plt.plot(xs, totRtrace2/10., 'g.-', label=label2)
        plt.plot(np.convolve(totRtrace1,np.array([0.2]*100),mode='valid'), 'r', label=label1)
        plt.plot(np.convolve(totRtrace2,np.array([0.2]*100),mode='valid'), 'b', label=label2)
        plt.xlabel("Number of games (/"+str(nsteps)+")")
        plt.legend()
        plt.show()


    ##---------------------- Next Step
    # start agents  (S.RL vs APQ)

#def movingAvg(x,q=2):
    
