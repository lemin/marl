import numpy as np
import matplotlib.pyplot as plt
import sys


smooth_window = 1000
if len(sys.argv) != 2:
    print "Usage: plot_r_p npz_file"
    sys.exit(1)
    
#algo = sys.argv[1]
#ipart = algo.index('-')
#label1 = label2 = algo[:ipart]
npzread = np.load(sys.argv[1])
totResult = npzread['tot']
label1 = str(npzread['label1'])
label2 = str(npzread['label2'])

plt.figure(1)
plt.clf()
xs = range(1,len(totResult[0])+1)
#plt.plot(xs, totRtrace1/10., 'r.-', label=label1)
#plt.plot(xs, totRtrace2/10., 'g.-', label=label2)
plt.plot(np.convolve(totResult[0],np.array([0.2]*smooth_window),mode='valid'), 'r', label=label1)
plt.plot(np.convolve(totResult[1],np.array([0.2]*smooth_window),mode='valid'), 'b', label=label2)
plt.xlabel("Number of games")
plt.ylabel("Rewards")
plt.legend()
#plt.savefig("matching_pennies_rewards_" + optstr[option] +".pdf")

plt.figure(2)
plt.clf()
#plt.plot(np.convolve(totResult[2],np.array([0.2]*smooth_window),mode='valid'), 'r', label=label1)
#plt.plot(np.convolve(totResult[3],np.array([0.2]*smooth_window),mode='valid'), 'b', label=label2)
plt.plot(totResult[2], 'r', label=label1)
plt.plot(totResult[3], 'b', label=label2)
plt.xlabel("Number of games")
plt.ylabel("Policy")
plt.legend()
plt.show()
#plt.savefig("matching_pennies_policy_" + optstr[option] +".pdf")


