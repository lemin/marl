import os
import globalTracker as gt 
import TDagent as agent
import marltrain as marl
import environ as env
import matplotlib.pyplot as plt
import numpy as np
import random
import pdb
from analMARL import storeArray, plotReward
import time

def createSeqPool(nactions):
    seqPool = []
    seqPool.append(range(1,nactions+1))
    seqPool.append(range(nactions,0,-1))
    seqPool.append(range(2,nactions+1,2)+range(1,nactions+1,2))
    seqPool.append(range(1,nactions+1,2)+range(2,nactions+1,2))
    seqPool.append(range(nactions,0,-2)+range(nactions-1,0,-2))
    seqPool.append(range(nactions-1,0,-2)+range(nactions,0,-2))
    seqPool.append(range(1,nactions+1)+range(nactions,0,-1))
    seqPool.append(range(nactions,0,-1)+range(1,nactions+1))
    return seqPool




if __name__ == '__main__':

    # match option 
    SRLvsRAND, APQvsRAND, SRLvsAPQ = range(3)
    option = SRLvsAPQ 

    totRtrace1 = None
    totRtrace2 = None

    nreps = 1000000
    nsteps = 2 # minimum 2 steps are necessary for APQ

    for nagents in [2,4,6,8,10]: #,15,20,25,30]:
        for nactions in [4,6,8,10]:
            seqpool = createSeqPool(nactions)
            SEQagents = []
            print "_____________________________"+str((nagents,nactions))+"\n"

            # A_j - create cartesian product of possible actions
            A_j=np.tile(np.array([range(1,nactions+1)]).T,(1,nagents-1))
            A_j=np.array([range(1,nactions+1)]).T
            for i in xrange(nagents-2):
                nr = A_j.shape[0]
                colm = np.repeat(range(1,nactions+1),nr)
                colm.shape=(len(colm),1)
                A_j = np.hstack((np.tile(A_j, (nactions,1)), colm))

            # create APQ agent
            APQagent = marl.APQtrain(agent.TDAgent(nactions, nagents-1), agIdx=0, 
                                       A_j=A_j , expdec=0.9999)
            APQagent.setParams(nReps=nreps, nSteps=nsteps)
            APQagent.agentIdx = 0
            APQagent.flgTerm = False
            agentList = [APQagent]

            # create Sequence Plyaers
            for i in xrange(nagents-1):
                SEQagents.append(marl.MASequence(agent.TDAgent(nactions, nagents-1), agIdx=i+1))
                SEQagents[i].setParams(nReps=nreps, nSteps=nsteps)
                SEQagents[i].setPolicy(seqpool[random.sample(range(len(seqpool)),1)[0]])
                SEQagents[i].agentIdx = i+1
                SEQagents[i].flgTerm = False
                agentList.append(SEQagents[i])

            # create global tracker 
            rpsEnv = env.TDenv(nactions)
            gtracker = gt.GlobalTracker(nagents, rpsEnv)
            gtracker.flgTerm = False

            #start global tracker thread
            gtracker.start()

            APQagent.start()
            for i in xrange(nagents-1):
                SEQagents[i].start()

            time.sleep(900) # 15 minutes

            for thr in agentList: 
                thr.flgTerm = True
            gtracker.flgTerm = True

            for thr in agentList: 
                thr.join()
            gtracker.join()

            # collect rewards 
            rlen = len(APQagent.rtrace)
            print rlen
            rAlltr = np.array([APQagent.rtrace]).T
            for seqag in SEQagents:
                rAlltr = np.hstack((rAlltr, np.array([seqag.rtrace[:rlen]]).T))

            print "Done"
            fname= storeArray(rAlltr, 'APQ-2step', nactions, nagents)

            os.system("scp "+fname+" lemin@conejos.cs.colostate.edu:~/conejos/exp/output/0331/APQ")
            os.system("rm "+fname)

            if rlen<=1: 
                print "Stop testing more number of actions: reached lowest number of games"
                break






