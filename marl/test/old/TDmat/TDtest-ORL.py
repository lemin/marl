import os
import globalTracker as gt 
import TDagent as agent
import marltrain as marl
import environ as env
import matplotlib.pyplot as plt
import numpy as np
import random
import pdb
from analMARL import storeArray, plotReward
import time

def createSeqPool(nactions):
    seqPool = []
    seqPool.append(range(1,nactions+1))
    seqPool.append(range(nactions,0,-1))
    seqPool.append(range(2,nactions+1,2)+range(1,nactions+1,2))
    seqPool.append(range(1,nactions+1,2)+range(2,nactions+1,2))
    seqPool.append(range(nactions,0,-2)+range(nactions-1,0,-2))
    seqPool.append(range(nactions-1,0,-2)+range(nactions,0,-2))
    seqPool.append(range(1,nactions+1)+range(nactions,0,-1))
    seqPool.append(range(nactions,0,-1)+range(1,nactions+1))
    return seqPool


if __name__ == '__main__':

    # match option 
    SRLvsRAND, APQvsRAND, SRLvsAPQ = range(3)
    option = SRLvsAPQ 

    totRtrace1 = None
    totRtrace2 = None

    nreps = 10000000
    nsteps = 1

    for nagents in [2,4,6,8,10,15,20,25,30]:
        for nactions in [4,6,8,10]:
            seqpool = createSeqPool(nactions)
            SEQagents = []

            print "_____________________________"+str((nagents,nactions))+"\n"

            # create ORL TD agent ###### Omnisient RL ######
            ORLagent = marl.MARLtrain(agent.TDAgent(nactions, nagents-1), agIdx=0, 
                                        filter='none',nagents=nagents, expdec=0.9999)
            # creat NN network for SRL agent
            ORLagent.makeQNN([ORLagent.get_nnNI(), 5, 1])
            ORLagent.setParams(nReps=nreps, nSteps=nsteps)
            #ORLagent.setProgressOn() # print out progress from this thread
            ORLagent.agentIdx = 0
            ORLagent.flgTerm = False
            agentList = [ORLagent]

            # create Sequence Plyaers
            for i in xrange(nagents-1):
                SEQagents.append(marl.MASequence(agent.TDAgent(nactions, nagents-1), agIdx=i+1))
                SEQagents[i].setParams(nReps=nreps, nSteps=nsteps)
                SEQagents[i].setPolicy(seqpool[random.sample(range(len(seqpool)),1)[0]])
                SEQagents[i].agentIdx = i+1
                SEQagents[i].flgTerm = False
                agentList.append(SEQagents[i])

            # create global tracker 
            rpsEnv = env.TDenv(nactions)
            gtracker = gt.GlobalTracker(nagents, rpsEnv)
            gtracker.flgTerm = False

            #start global tracker thread
            gtracker.start()

            ORLagent.start()
            for i in xrange(nagents-1):
                SEQagents[i].start()

            time.sleep(900) # 15 minutes

            for thr in agentList: 
                thr.flgTerm = True
            gtracker.flgTerm = True

            for thr in agentList: 
                thr.join()
            gtracker.join()

            # collect rewards 
            rlen = len(ORLagent.rtrace)
            print rlen
            rAlltr = np.array([ORLagent.rtrace]).T
            for seqag in SEQagents:
                rAlltr = np.hstack((rAlltr, np.array([seqag.rtrace[:rlen]]).T))

            fname= storeArray(rAlltr, 'ORL-1step', nactions, nagents)

            os.system("scp "+fname+" lemin@conejos.cs.colostate.edu:~/conejos/exp/output/0331/ORL")
            os.system("rm "+fname)

            if rlen<=1: 
                print "Stop testing more number of actions: reached lowest number of games"
                break


