
import numpy as np
import matplotlib.pyplot as plt
import random
import pdb
import signal
from analMARL import toArray,storeArray
import os

flgTerm =False  # termination flag

def createSeqPool(nactions):
    seqPool = []
    seqPool.append(range(1,nactions+1))
    seqPool.append(range(nactions,0,-1))
    seqPool.append(range(2,nactions+1,2)+range(1,nactions+1,2))
    seqPool.append(range(1,nactions+1,2)+range(2,nactions+1,2))
    seqPool.append(range(nactions,0,-2)+range(nactions-1,0,-2))
    seqPool.append(range(nactions-1,0,-2)+range(nactions,0,-2))
    seqPool.append(range(1,nactions+1)+range(nactions,0,-1))
    seqPool.append(range(nactions,0,-1)+range(1,nactions+1))
    return seqPool

def assignSeqToAgents(seqpool, nagents):
    seqAgents= []
    for i in xrange(nagents):
        seqAgents.append(seqpool[random.sample(range(len(seqpool)),1)[0]])
    return seqAgents

# signal handler
def sighandler(signum, frame):
    print 'preset time is passed!!! finishing...'
    global flgTerm 
    flgTerm = True

def ncount(H, a):
    ncnt = 0
    for i in xrange(len(H)):
       if H[i] == a: ncnt+=1
    return ncnt 


def reinforcement(Aall,reward):
    #aj = Aall[0]
    #a_j = Aall[1]
    A = np.array(Aall)
    lownum = sum(A.min()==A)

    retlst = [[A.min()]] * len(A)
    if(lownum==1):
        lowidx = np.where(A==A.min())[0][0]
        retlst[lowidx] = [retlst[lowidx][0]+reward]        
     
    return retlst[0][0], retlst

def next(s,A):
    return A

def calcValidMoves(s,nactions):
    return range(1,nactions+1)

def values(s, validMoves, A_j, Q, Pi):
    global flgTerm 
    qs = []
    skey = tuple(s)
    if not Pi.has_key(skey): 
        Pi[skey] = 1. / float(A_j.shape[0])

    for a in validMoves:
        sumQ = 0
        for a_j in A_j:
            if flgTerm: return None 
            key = tuple(s+[a]+list(a_j))
            if not Q.has_key(key):
                Q[key] = 0
            sumQ += (Pi[skey]*Q[key]) 
        qs.append(sumQ) 
    return qs

def getA_j(t, nagents, policies):
    # generate nagents number of sequences 
    a_j = [] 
    for i in xrange(nagents):
        idx=t%len(policies[i])
        a_j.append(policies[i][idx])
    return a_j
    

def APQ(S,A_j,s0, tmax=1000, p=100, l=60, **params):

    global flgTerm 
    
    alpha = params.pop("alpha", 0.5) 
    gamma = params.pop("gamma", 0.9) 
    nActions = params.pop("nactions", 2)
    nAgents = params.pop("nagents", 2)
    reward = params.pop("reward", -1)
    bplot = params.pop("bplot", False)
    policies = params.pop("policies", None)
    if reward==-1: reward=nactions+1
    Q = {}
    t = 0
    H = {} #np.zeros((p, A_j.shape[1]))
    s = s0 
    epsilon = 1 
    epsilonExp = 0.9999

    rtrace = [] # for plotting
    rAlltr = None
    mrtr=[]
    Pi= {}
    V = {}
    skey = tuple(s)
    while True : #t <= tmax : 

        if flgTerm: break

        # Q: what is a_j to be set? 
        validMoves = calcValidMoves(s,nActions)
        if random.uniform(0,1) < epsilon: # random
            aj = validMoves[random.sample(range(len(validMoves)),1)[0]]
            #print "random ",aj
        else:                           # greedy 
            qs = values(s, validMoves, A_j, Q, Pi)
            if qs is None: break
            #print "epsilon: ", epsilon
            #print s
            #print qs
            #print "====================================================="
            aj = validMoves[np.argmax(qs)]
            #print "best ",qs, aj
            
        #a_j = A_j[random.sample(range(A_j.shape[0]),1)[0]] # random play opponent
        #a_j = A_j[0]  # static (0) play opponent
        #a_j = A_j[t%3] # seqential play opponent
        a_j = getA_j(t, nAgents-1, policies)
        Aall = [aj]+list(a_j)
        #print "Aall : ", Aall
        key = tuple(s+Aall)
        s1 = next(s,Aall) 
        r1,rAll = reinforcement(Aall,reward)

        #print "reward : ", r1

        rtrace.append(r1)
        rAlltr = toArray(rAlltr, rAll)

        # Value(St+1)
        s1Moves = calcValidMoves(s1,nActions)
        s1key = tuple(s1)
        qs = values(s1, s1Moves, A_j, Q, Pi)
        if qs is None: break
        #print qs   #_____________________________________> This is always 0's ==== check the reason why!!!
        V[s1key] = max(qs)

        # Q update
        if not Q.has_key(key):
            Q[key] = 0
        Q[key] = (1-alpha) * Q[key] + alpha * (r1 + gamma * V[s1key])
        #print key, r1,  Q[key]

        # add history 
        if not H.has_key(skey):
            H[skey] = []
        H[skey].append(a_j) # H[s] U a_j
        if len(H[skey]) > p: 
            H[skey].pop()

        Pi[skey] = float(ncount(H[skey][:-l], a_j)) / float(l)

        s = s1 
        t += 1
        epsilon *= epsilonExp
        skey = tuple(s)
        
        if bplot and t % 1000 ==0:
            mrtr.append(np.mean(rtrace))
            plt.plot(range(len(mrtr)),mrtr)
            #plt.plot(range(len(rtrace)),rtrace)
            #plt.plot(range(len(rtrace)),[0]*len(rtrace))
            plt.draw()
            rtrace = []

    #return rtrace
    return rAlltr


if __name__ == "__main__":



    for nagents in [2,4,6,8,10,15,20,25,30]:
        for nactions in [4,6,8,10]:
            seqpool = createSeqPool(nactions)
            print "_____________________________"+str((nagents,nactions))+"\n"
            seqAgents = assignSeqToAgents(seqpool, nagents-1)

            signal.signal(signal.SIGALRM, sighandler)
            flgTerm = False
            signal.alarm(900) # 15 minutes

            rAlltr = APQ([], np.array([[0,1,2]]).T, [0]*nagents,tmax=100000, 
                            nactions=nactions, nagents=nagents, reward=-1, bplot=False,
                            policies=seqAgents )
            print rAlltr.shape
            fname = storeArray(rAlltr, "centAPQ-1step", nactions, nagents)

            os.system("scp "+fname+" lemin@conejos.cs.colostate.edu:~/conejos/exp/output/0331/centAPQ")
            os.system("rm "+fname)


