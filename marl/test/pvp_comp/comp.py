""" run competition in prey-vs-predator game
"""
import os
import marl.globalTracker as gt 
import marl.games.PvPagent as agent
from marl.learn.marltrain import SRL, MARL
from marl.learn.marltrain import ORL
from marl.learn.discrete import SRLD, ORLD
from marl.learn.apq import APQ
from marl.learn.wpl import WPL
from marl.learn.gigawolf import GIGAWoLF
from marl.learn.exp3 import Exp3
from marl.learn.triv import MARandom, MAStatic
import marl.environ as env

import matplotlib.pyplot as plt
import numpy as np
import random
import sys
from marl.utils.analMARL import storeArray, plotReward
import time
import shutil


def run_comp(algos, testn, nsteps, nreps, running_time, fsize, eps):

    npreys = 1
    npreds = 2
    nagents = npreys+npreds
 
    Preys=[]
    Preds=[]
    agentList=[]

    SRLpred = SRL(agent.DiscPredAgent(npreys), agIdx=0)
    # creat NN network for SRL agent
    nH = 20
    SRLpred.makeQNN([SRLpred.get_nnNI(), nH, 1])
    SRLpred.setParams(nReps=nreps, nSteps=nsteps,
                        fixedEpsilon=eps,
                        Lambda=0.01, Gamma=0.95, nIterations=10,
                        wPrecision=1e-4, fPrecision=1e-4,
                        record=nsteps)

    SRLprey = SRL(agent.DiscPreyAgent(npreys), agIdx=0)
    SRLprey.makeQNN([SRLprey.get_nnNI(), nH, 1])
    SRLprey.setParams(nReps=nreps, nSteps=nsteps,
                        fixedEpsilon=eps,
                        Lambda=0.01, Gamma=0.95, nIterations=10,
                        wPrecision=1e-4, fPrecision=1e-4,
                        record=nsteps)

    CNTABSprey = MARL(agent.DiscPreyAgent(npreys, 1), agIdx=0, 
                      filter='none',abstract='cnt',nagents=nagents)
    CNTABSprey.makeQNN([CNTABSprey.get_nnNI(), nH, 1])
    CNTABSprey.setParams(nReps=nreps, nSteps=nsteps,
                        fixedEpsilon=eps,
                        Lambda=0.01, Gamma=0.95, nIterations=10,
                        wPrecision=1e-4, fPrecision=1e-4,
                        record=nsteps)

    FILT1prey = MARL(agent.DiscPreyAgent(npreys, 1*2), agIdx=0, 
                      filter='1', nagents=nagents)
    FILT1prey.makeQNN([FILT1prey.get_nnNI(), nH, 1])
    FILT1prey.setParams(nReps=nreps, nSteps=nsteps,
                        fixedEpsilon=eps,
                        Lambda=0.01, Gamma=0.95, nIterations=10,
                        wPrecision=1e-4, fPrecision=1e-4,
                        record=nsteps)

    # create ORL TD agent ###### Omnisient RL ######
    ORLpred = ORL(agent.DiscPredAgent(npreys,(nagents-1)*2), agIdx=0)
    # creat NN network for SRL agent
    nH = 20
    ORLpred.makeQNN([ORLpred.get_nnNI(), nH, 1])
    ORLpred.setParams(nReps=nreps, nSteps=nsteps,
                        fixedEpsilon=eps,
                        Lambda=0.01, Gamma=0.95, nIterations=10,
                        wPrecision=1e-4, fPrecision=1e-4,
                        record=nsteps)

    ORLprey = ORL(agent.DiscPreyAgent(npreys,(nagents-1)*2), agIdx=0)
    # creat NN network for SRL agent
    nH = 20
    ORLprey.makeQNN([ORLprey.get_nnNI(), nH, 1])
    ORLprey.setParams(nReps=nreps, nSteps=nsteps,
                        fixedEpsilon=eps,
                        Lambda=0.01, Gamma=0.95, nIterations=10,
                        wPrecision=1e-4, fPrecision=1e-4,
                        record=nsteps)

    SRLDpred = SRLD(agent.DiscPredAgent(npreys), agIdx=0)
    SRLDpred.setParams(nReps=nreps, nSteps=nsteps,
                        fixedEpsilon=eps,
                        Lambda=0.01, Gamma=0.95,
                        record=nsteps)

    SRLDprey = SRLD(agent.DiscPreyAgent(npreys), agIdx=0)
    SRLDprey.setParams(nReps=nreps, nSteps=nsteps,
                        fixedEpsilon=eps,
                        Lambda=0.01, Gamma=0.95,
                        record=nsteps)


    ORLDpred = ORLD(agent.DiscPredAgent(npreys), agIdx=0)
    ORLDpred.setParams(nReps=nreps, nSteps=nsteps,
                        fixedEpsilon=eps,
                        Lambda=0.01, Gamma=0.95,
                        record=nsteps)

    ORLDprey = ORLD(agent.DiscPreyAgent(npreys), agIdx=0)
    ORLDprey.setParams(nReps=nreps, nSteps=nsteps,
                        fixedEpsilon=eps,
                        Lambda=0.01, Gamma=0.95,
                        record=nsteps)

    # create WPL agent
    WPLpred = WPL(agent.DiscPredAgent(npreys), agIdx=0)
    WPLpred.setParams(nReps=nreps, nSteps=nsteps,
                        record=nsteps)
    WPLpred.gamma = 0.95
    WPLpred.eta = 0.001

    WPLprey = WPL(agent.DiscPreyAgent(npreys), agIdx=0)
    WPLprey.setParams(nReps=nreps, nSteps=nsteps,
                        record=nsteps)
    WPLprey.gamma = 0.95
    WPLprey.eta = 0.001


    # create GIGA-WoLF agent
    GWFpred= GIGAWoLF(agent.DiscPredAgent(npreys), agIdx=0)
    GWFpred.setParams(nReps=nreps, nSteps=nsteps,
                        record=nsteps)
    GWFpred.gamma = 0.95
 
    GWFprey= GIGAWoLF(agent.DiscPreyAgent(npreys), agIdx=0)
    GWFprey.setParams(nReps=nreps, nSteps=nsteps,
                        record=nsteps)
    GWFprey.gamma = 0.95
 
    # create Exp3 agent
    Exp3pred= Exp3(agent.DiscPredAgent(npreys), agIdx=0)
    Exp3pred.setParams(nReps=nreps, nSteps=nsteps,
                        record=nsteps)

    Exp3prey= Exp3(agent.DiscPreyAgent(npreys), agIdx=0)
    Exp3prey.setParams(nReps=nreps, nSteps=nsteps,
                        record=nsteps)


    cur_algos = []

    # create preys 
    i = 0 
    algo = algos[0]
    if algo == 'srl':
        Preys.append(SRLprey)
    elif algo == 'orl':
        Preys.append(ORLprey)
    elif algo == 'srld':
        Preys.append(SRLDprey)
    elif algo == 'orld':
        Preys.append(ORLDprey)
    elif algo == 'abscnt':
        Preys.append(CNTABSprey)
    elif algo == 'filt1':
        Preys.append(FILT1prey)
    elif algo == 'wpl':
        Preys.append(WPLprey) 
    elif algo == 'gwf':
        Preys.append(GWFprey) 
    elif algo == 'exp3':
        Preys.append(Exp3prey)
    elif algo == 'rand':
        Preys.append(MARandom(agent.DiscPreyAgent(npreys), agIdx=i))
        Preys[i].setParams(nReps=nreps, nSteps=nsteps, record=nsteps)
    Preys[i].agentIdx = i
    Preys[i].flgTerm = False
    agentList.append(Preys[i])
    cur_algos.append(algo)

    # Predators
    i = 0
    Preds.append(MAStatic(agent.DiscPredAgent(npreys), agIdx=npreys+i))
    Preds[i].setParams(nReps=nreps, nSteps=nsteps, record=nsteps)
    Preds[i].agentIdx = npreys+i
    Preds[i].flgTerm = False
    agentList.append(Preds[i])
    i += 1
    cur_algos.append('static')

    algo = algos[1]
    if algo == 'srl':
        Preds.append(SRLpred)
    elif algo == 'orl':
        Preds.append(ORLpred)
    elif algo == 'srld':
        Preds.append(SRLDpred)
    elif algo == 'orld':
        Preds.append(ORLDpred)
    elif algo == 'wpl':
        Preds.append(WPLpred) 
    elif algo == 'gwf':
        Preds.append(GWFpred) 
    elif algo == 'exp3':
        Preds.append(Exp3pred)
    elif algo == 'rand':
        Preds.append(MARandom(agent.DiscPredAgent(npreys), agIdx=npreys+i))
        Preds[i].setParams(nReps=nreps, nSteps=nsteps, record=nsteps)
    cur_algos.append(algo)

    Preds[i].agentIdx = npreys+i
    Preds[i].flgTerm = False
    agentList.append(Preds[i])
    i += 1

    for ag in agentList:
        ag.model.FieldSize = fsize

    # create global tracker 
    Env = env.DiscPvPenv([0] * npreys + [1] * npreds, fsize)
    gtracker = gt.GlobalTracker(nagents, Env)
    gtracker.flgTerm = False

    #start global tracker thread
    gtracker.start()
    for thr in agentList: 
        thr.start()

    if True:
        time.sleep(running_time)
        for thr in agentList:
            thr.flgTerm = True
        gtracker.flgTerm = True

    for thr in agentList: 
        thr.join()
    gtracker.join()

    # collect rewards 
    to_store = []
    rlen = np.min([len(ag.rtrace) for ag in agentList])
    i = 0
    for ag, salgo in zip(agentList, cur_algos):
        tmp = {}
        tmp['type'] = ag.model.ID 
        tmp['algo'] = salgo

        tmp['rtrace'] = np.asarray(ag.rtrace)  # all rewards
        tmp['survive'] = np.asarray(ag.completeGames)  # 0 or 1 for each episode
        tmp['ngames'] = np.asarray(ag.nGames)     # num of games played in each episode
        # to create mask later, np.repeat(survive, ngames)
        tmp['rlen'] = rlen
        to_store.append(tmp)
        
    #print "mean: ", np.mean(Preys[0].rtrace), "last 10", np.mean(Preys[0].rtrace[-10:])
    #print "mean: ", np.mean(Preds[0].rtrace), "last 10", np.mean(Preds[0].rtrace[-10:])
    #print "mean: ", np.mean(Preds[1].rtrace), "last 10", np.mean(Preds[1].rtrace[-10:])
    #print "complete: ", np.sum(Preds[0].completeGames), "/", len(Preds[0].completeGames)

    if True:
        fname= storeArray(to_store, "comp_eps"+ str(eps) +"_" + algos[0] + "_" + algos[1] + '-'+ str(nsteps)+ 'step_' + str(running_time) + "sec",
                            5, nagents, testn)
        shutil.move(fname, "comp/fsize"+str(fsize))


if __name__ == '__main__':

    running_time = 900
    nreps = 2000
    nsteps = 500
    eps = float(sys.argv[3])
    fsize = int(sys.argv[4])
    print "Fsize: ", fsize

    #for i in xrange(1):
    run_comp([sys.argv[1], sys.argv[2]], sys.argv[5],
                nsteps, nreps, running_time, fsize, eps)
 
