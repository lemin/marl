#!/bin/bash
preys=(srl srld orl orld wpl) # gwf exp3 abscnt filt1)
preds=(srl srld orl orld wpl gwf exp3)
fixed_epsilon=0.2
#skip=false

for a in ${preys[*]}
do
    for b in ${preds[*]}
    do
        #if $skip; then 
        #    continue
        #fi
 
        echo "test comp: $a $b  _________________ $2"
        echo "fixed epsilon = $fixed_epsilon"
        nice -n 19 python comp.py $a $b $fixed_epsilon $1 $2
    done
done
