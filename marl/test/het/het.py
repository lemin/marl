import os
import marl.globalTracker as gt 
import marl.games.PvPagent as agent
from marl.learn.marltrain import SRL, MARL
from marl.learn.marltrain import ORL
from marl.learn.discrete import SRLD, ORLD
from marl.learn.apq import APQ
from marl.learn.wpl import WPL
from marl.learn.gigawolf import GIGAWoLF
from marl.learn.exp3 import Exp3
from marl.learn.triv import MARandom, MAStatic
import marl.environ as env

import matplotlib.pyplot as plt
import numpy as np
import random
import sys
from marl.utils.analMARL import storeArray, plotReward
import time
import shutil


def run_het(testn, nsteps, nreps, running_time, fsize, eps):

    npreys = 9#9
    npreds = 7#7
    nagents = npreys+npreds
 
    Preys=[]
    Preds=[]
    agentList=[]

    SRLpred = SRL(agent.DiscPredAgent(npreys), agIdx=0)
    # creat NN network for SRL agent
    nH = 20
    SRLpred.makeQNN([SRLpred.get_nnNI(), nH, 1])
    SRLpred.setParams(nReps=nreps, nSteps=nsteps,
                        fixedEpsilon=eps,
                        Lambda=0.01, Gamma=0.95, nIterations=10,
                        wPrecision=1e-4, fPrecision=1e-4,
                        record=nsteps)

    SRLprey = SRL(agent.DiscPreyAgent(npreys), agIdx=0)
    SRLprey.makeQNN([SRLprey.get_nnNI(), nH, 1])
    SRLprey.setParams(nReps=nreps, nSteps=nsteps,
                        fixedEpsilon=eps,
                        Lambda=0.01, Gamma=0.95, nIterations=10,
                        wPrecision=1e-4, fPrecision=1e-4,
                        record=nsteps)

    CNTABSprey = MARL(agent.DiscPreyAgent(npreys, 1), agIdx=0, 
                      filter='none',abstract='cnt',nagents=nagents)
    CNTABSprey.makeQNN([CNTABSprey.get_nnNI(), nH, 1])
    CNTABSprey.setParams(nReps=nreps, nSteps=nsteps,
                        fixedEpsilon=eps,
                        Lambda=0.01, Gamma=0.95, nIterations=10,
                        wPrecision=1e-4, fPrecision=1e-4,
                        record=nsteps)

    CNTABSpred = MARL(agent.DiscPredAgent(npreys, 1), agIdx=0, 
                      filter='none',abstract='cnt',nagents=nagents)
    CNTABSpred.makeQNN([CNTABSpred.get_nnNI(), nH, 1])
    CNTABSpred.setParams(nReps=nreps, nSteps=nsteps,
                        fixedEpsilon=eps,
                        Lambda=0.01, Gamma=0.95, nIterations=10,
                        wPrecision=1e-4, fPrecision=1e-4,
                        record=nsteps)

    FILT1prey = MARL(agent.DiscPreyAgent(npreys, 1*2), agIdx=0, 
                      filter='1', nagents=nagents)
    FILT1prey.makeQNN([FILT1prey.get_nnNI(), nH, 1])
    FILT1prey.setParams(nReps=nreps, nSteps=nsteps,
                        fixedEpsilon=eps,
                        Lambda=0.01, Gamma=0.95, nIterations=10,
                        wPrecision=1e-4, fPrecision=1e-4,
                        record=nsteps)

    FILT1pred = MARL(agent.DiscPredAgent(npreys, 1*2), agIdx=0, 
                      filter='1', nagents=nagents)
    FILT1pred.makeQNN([FILT1pred.get_nnNI(), nH, 1])
    FILT1pred.setParams(nReps=nreps, nSteps=nsteps,
                        fixedEpsilon=eps,
                        Lambda=0.01, Gamma=0.95, nIterations=10,
                        wPrecision=1e-4, fPrecision=1e-4,
                        record=nsteps)

    # create ORL TD agent ###### Omnisient RL ######
    ORLpred = ORL(agent.DiscPredAgent(npreys,(nagents-1)*2), agIdx=0)
    # creat NN network for SRL agent
    nH = 20
    ORLpred.makeQNN([ORLpred.get_nnNI(), nH, 1])
    ORLpred.setParams(nReps=nreps, nSteps=nsteps,
                        fixedEpsilon=eps,
                        Lambda=0.01, Gamma=0.95, nIterations=10,
                        wPrecision=1e-4, fPrecision=1e-4,
                        record=nsteps)

    ORLprey = ORL(agent.DiscPreyAgent(npreys,(nagents-1)*2), agIdx=0)
    # creat NN network for SRL agent
    nH = 20
    ORLprey.makeQNN([ORLprey.get_nnNI(), nH, 1])
    ORLprey.setParams(nReps=nreps, nSteps=nsteps,
                        fixedEpsilon=eps,
                        Lambda=0.01, Gamma=0.95, nIterations=10,
                        wPrecision=1e-4, fPrecision=1e-4,
                        record=nsteps)

    SRLDpred = SRLD(agent.DiscPredAgent(npreys), agIdx=0)
    SRLDpred.setParams(nReps=nreps, nSteps=nsteps,
                        fixedEpsilon=eps,
                        Lambda=0.01, Gamma=0.95,
                        record=nsteps)

    SRLDprey = SRLD(agent.DiscPreyAgent(npreys), agIdx=0)
    SRLDprey.setParams(nReps=nreps, nSteps=nsteps,
                        fixedEpsilon=eps,
                        Lambda=0.01, Gamma=0.95,
                        record=nsteps)


    ORLDpred = ORLD(agent.DiscPredAgent(npreys), agIdx=0)
    ORLDpred.setParams(nReps=nreps, nSteps=nsteps,
                        fixedEpsilon=eps,
                        Lambda=0.01, Gamma=0.95,
                        record=nsteps)

    ORLDprey = ORLD(agent.DiscPreyAgent(npreys), agIdx=0)
    ORLDprey.setParams(nReps=nreps, nSteps=nsteps,
                        fixedEpsilon=eps,
                        Lambda=0.01, Gamma=0.95,
                        record=nsteps)

    # create WPL agent
    WPLpred = WPL(agent.DiscPredAgent(npreys), agIdx=0)
    WPLpred.setParams(nReps=nreps, nSteps=nsteps,
                        record=nsteps)
    WPLpred.gamma = 0.95
    WPLpred.eta = 0.001

    WPLprey = WPL(agent.DiscPreyAgent(npreys), agIdx=0)
    WPLprey.setParams(nReps=nreps, nSteps=nsteps,
                        record=nsteps)
    WPLprey.gamma = 0.95
    WPLprey.eta = 0.001


    # create GIGA-WoLF agent
    GWFpred= GIGAWoLF(agent.DiscPredAgent(npreys), agIdx=0)
    GWFpred.setParams(nReps=nreps, nSteps=nsteps,
                        record=nsteps)
    GWFpred.gamma = 0.95
 
    GWFprey= GIGAWoLF(agent.DiscPreyAgent(npreys), agIdx=0)
    GWFprey.setParams(nReps=nreps, nSteps=nsteps,
                        record=nsteps)
    GWFprey.gamma = 0.95
 
    # create Exp3 agent
    Exp3pred= Exp3(agent.DiscPredAgent(npreys), agIdx=0)
    Exp3pred.setParams(nReps=nreps, nSteps=nsteps,
                        record=nsteps)

    Exp3prey= Exp3(agent.DiscPreyAgent(npreys), agIdx=0)
    Exp3prey.setParams(nReps=nreps, nSteps=nsteps,
                        record=nsteps)

    RANDprey = MARandom(agent.DiscPreyAgent(npreys), agIdx=0)
    RANDprey.setParams(nReps=nreps, nSteps=nsteps, record=nsteps)

    RANDpred = MARandom(agent.DiscPredAgent(npreys), agIdx=0)
    RANDpred.setParams(nReps=nreps, nSteps=nsteps, record=nsteps)

    STATprey = MAStatic(agent.DiscPreyAgent(npreys), agIdx=0)
    STATprey.setParams(nReps=nreps, nSteps=nsteps, record=nsteps)

    STATpred = MAStatic(agent.DiscPredAgent(npreys), agIdx=0)
    STATpred.setParams(nReps=nreps, nSteps=nsteps, record=nsteps)

    cur_algos = []

    # create preys 
    Preys.append(SRLprey)
    cur_algos.append('srl')
    Preys.append(ORLprey)
    cur_algos.append('orl')
    Preys.append(SRLDprey)
    cur_algos.append('srld')
    Preys.append(ORLDprey)
    cur_algos.append('orld')
    Preys.append(CNTABSprey)
    cur_algos.append('abscnt')
    Preys.append(FILT1prey)
    cur_algos.append('filt1')
    Preys.append(WPLprey) 
    cur_algos.append('wpl')
    Preys.append(GWFprey) 
    cur_algos.append('gwf')
    Preys.append(Exp3prey)
    cur_algos.append('exp3')
    for i in xrange(len(Preys)):
        Preys[i].agentIdx = i
        Preys[i].flgTerm = False
        if i == npreys: break
        agentList.append(Preys[i])

    # Predators
    Preds.append(SRLpred)
    cur_algos.append('srl')
    Preds.append(ORLpred)
    cur_algos.append('orl')
    Preds.append(SRLDpred)
    cur_algos.append('srld')
    Preds.append(ORLDpred)
    cur_algos.append('orld')
    Preds.append(CNTABSpred)
    cur_algos.append('abscnt')
    Preds.append(FILT1pred)
    cur_algos.append('filt1')
    Preds.append(WPLpred) 
    cur_algos.append('wpl')
    Preds.append(GWFpred) 
    cur_algos.append('gwf')
    Preds.append(Exp3pred)
    cur_algos.append('exp3')

    for i in xrange(len(Preds)):
        Preds[i].agentIdx = npreys+i
        Preds[i].flgTerm = False
        if i == npreds: break
        agentList.append(Preds[i])

    for ag in agentList:
        ag.model.FieldSize = fsize

    # create global tracker 
    Env = env.DiscPvPenv([0] * npreys + [1] * npreds, fsize)
    Env.nHerd = 2 # allow herding between preys
    gtracker = gt.GlobalTracker(nagents, Env)
    gtracker.flgTerm = False

    #start global tracker thread
    gtracker.start()
    for thr in agentList: 
        thr.start()

    if False:
        time.sleep(running_time)
        for thr in agentList:
            thr.flgTerm = True
        gtracker.flgTerm = True

    for thr in agentList: 
        thr.join()
    gtracker.join()

    # collect rewards 
    to_store = []
    rlen = np.min([len(ag.rtrace) for ag in agentList])
    i = 0
    for ag, salgo in zip(agentList, cur_algos):
        tmp = {}
        tmp['type'] = ag.model.ID 
        tmp['algo'] = salgo

        tmp['rtrace'] = np.asarray(ag.rtrace)  # all rewards
        tmp['survive'] = np.asarray(ag.completeGames)  # 0 or 1 for each episode
        tmp['ngames'] = np.asarray(ag.nGames)     # num of games played in each episode
        # to create mask later, np.repeat(survive, ngames)
        tmp['rlen'] = rlen
        to_store.append(tmp)
        
    #print "mean: ", np.mean(Preys[0].rtrace), "last 10", np.mean(Preys[0].rtrace[-10:])
    #print "mean: ", np.mean(Preds[0].rtrace), "last 10", np.mean(Preds[0].rtrace[-10:])
    #print "mean: ", np.mean(Preds[1].rtrace), "last 10", np.mean(Preds[1].rtrace[-10:])
    #print "complete: ", np.sum(Preds[0].completeGames), "/", len(Preds[0].completeGames)

    if True:
        fname= storeArray(to_store, "het(eps"+str(eps)+")-"+ str(nsteps)+ 'step_' + str(running_time) + "sec",
                            5, nagents, testn)
        shutil.move(fname, "het/")


if __name__ == '__main__':

    running_time = 900
    nreps = 2000
    nsteps = 500
    fsize = 100
    eps = 0.2

    #for i in xrange(1):
    run_het(sys.argv[1], nsteps, nreps, running_time, fsize, eps)
 
