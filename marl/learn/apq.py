import numpy as np
from .. import marlComm as comm
import random
from base import MASframe
import sys


class APQ(MASframe):

    # nopp : # of opponents
    def __init__(self, model, agIdx=-1, A_j=None, server=('localhost',3030),**param):
        MASframe.__init__(self, model, agIdx, server)
        # create set of joint action pairs of counterparts (A-j) if not given
        # if None, assume 1 opponent
        if A_j is None: 
            self.A_j = np.array([self.model.get_actions()]).T
        else:
            self.A_j = A_j 
        self.expEpsDec = param.pop('expdec',False)
        self.epsilon = 1

        self.set_discrete_on()

    def values(self,s,actions):
        qs = []
        skey = self.get_key(s)
        if not self.Pi.has_key(skey): 
            self.Pi[skey] = 1. / float(self.A_j.shape[0])

        for a in actions:
            sumQ = 0
            for a_j in self.A_j:
                if self.flgTerm: return None
                key = self.get_key(s, a, a_j)
                if not self.Q.has_key(key):
                    self.Q[key] = 0
                sumQ += (self.Pi[skey] * self.Q[key]) 
            qs.append(sumQ) 
        return qs

    def epsilon_greedy(self, s, s_j, epsilon):
        actions = self.model.get_actions(s)
        if random.uniform(0,1) < epsilon: # random
            aj = actions[random.sample(range(len(actions)),1)[0]]
        else:                           # greedy 
            qs = self.values(list(s)+s_j, actions)
            if qs is None: return None
            aj = actions[np.argmax(qs)]

        return aj  # when randomly 
        
    def get_samples(self, numSamples, epsilon) :
        
        nagents = self.A_j.shape[1] + 1 # # of opponents + self
        X = np.zeros((numSamples,self.model.get_state_dim()*nagents))
        R = np.zeros((numSamples,1))
        A = np.zeros((numSamples,self.model.get_action_dim()*nagents))

        # APQ uses joint states - # of column of A_j = # of opponents
        s = self.model.init()
        sall = [s for i in xrange(nagents)]
        s_j = s * (nagents-1)
        # join to global env 
        t = 0
        tj, s0all = comm.joinMarl(self.sock, s, self.agentIdx)
        if tj is None: 
            return (X,R,A,-1)
        s = self.model.get_S(s0all, j) #s = s0all[j]

        if self.agentIdx == -1: j= tj
        else: j = self.agentIdx
        #sys.stdout.write("MARL: "+ str(j)+ "----Joined\n")

        if self.expEpsDec: eps = self.epsilon
        else: eps= epsilon
        a = self.epsilon_greedy(s, s_j, eps)

        sall = s0all

        for step in range(numSamples):
            if self.flgTerm: break

            if self.expEpsDec:
                self.epsilon *= self.expEpsDec
                eps = self.epsilon
            # send action and get next states from Global tracker 
            t = t+1
            tmp, termflg, Rg, s1all,aall = comm.actionMarl(self.sock, j, t, s, a)
            if tmp==-1: break
            if not isinstance(s1all, np.ndarray):
                s1all = np.asarray(s1all)
            s1 = self.model.get_S(s1all, j) #s = s1all[j, :]
            r1 = self.model.get_reward(s,s1,a, Rg,j, step, numSamples)

            sall_j = np.vstack((sall[:j], sall[j+1:]))
            s1_j = list(sall_j.flatten())

            a1 = self.epsilon_greedy(s, s1_j, eps)

            # Collect
            X[step,:] = sall.flatten()[:]
            R[step,0] = r1
            A[step,:] = np.asarray(aall).flatten()[:] #aall
            
            if step > 0:
                self.update_policy(X[step-1], A[step-1], R[step-1], X[step-1])
            if self.model.check_early_stop(Rg) or termflg:
                break

            # next state
            s,a = s1,a1
            sall = s1all

        # end of episode. disconnect from GlobalTracker
        comm.exitMarl(self.sock, j)

        return (X,R,A,step)

    def ncount(self, H, a):
        ncnt = 0
        for i in xrange(len(H)):
            if H[i] == a: ncnt+=1
        return ncnt
   
    def start_agent(self, **params):
        if not comm.connectSock(self.sock,self.server) : return 
        self.rtrace = None
        self.rtrace, __= self.train(**params)


    def train(self, **params):

        epsilonType=params.pop("epsilonType", 'expDecrease') 
        epsilonSlope=params.pop("epsilonSlope", 6)
        epsilonStart=params.pop("epsilonStart", 0)
        epsilonEnd=params.pop("epsilonEnd", -1)
        fixedEpsilon=params.pop("fixedEpsilon", 0.5)
        finalEpsilon=params.pop("finalEpsilon", 0.001)

        self.alpha = params.pop("alpha", 0.5) 
        self.gamma = params.pop("gamma", 0.9) 
        self.n_history = params.pop("nhistory", 100) 
        self.n_samples = params.pop("nsampling", 60) 
        nReps = params.pop("nReps", 100)
        nSteps = params.pop("nSteps", 1000)
        N = params.pop("N", [nSteps]*nReps)

        #nmax_games = params.pop('record', 0)

        ### Initial epsilon is 1, for fully random action selection
        epsilon = 1
        if epsilonType=='fixed': 
            epsilon = fixedEpsilon
        elif epsilonType=='-tanh':
            if epsilonEnd<0 or epsilonEnd > nReps: 
                epsilonEnd = nReps
            sigmoidRange=epsilonEnd - epsilonStart

            step= (2.*epsilonSlope+1.)/sigmoidRange
            epsx = list(np.arange(-epsilonSlope,epsilonSlope+step, step))
            epsilons = [1]*epsilonStart + list((-np.tanh(epsx)+1)/2) + [0]*(nReps-epsilonEnd)
        else: # 'expDecrease'
            epsilonRate = np.exp(np.log(finalEpsilon)/nReps)
            #print "epsilon decay rate = %d" % epsilonRate
 
        self.rtrace = []
        epsilontrace = []
        self.completeGames = []
        self.nGames = []
        nCompGames = 0

        self.Q = {}
        self.V = {}
        self.H = {}
        self.Pi = {}
 
        for reps  in xrange(nReps):

            if self.flgTerm: break

            self.print_progress(reps, nReps)

            ## Collect N[reps] samples, each being s, a, r, s1, a1
            smplX,smplR, smplA,steps = self.get_samples(N[reps], epsilon)

            if epsilonType=='expDecrease': 
                epsilon *= epsilonRate
            elif epsilonType=='-tanh':
                epsilon = epsilons[reps]

            #rtrace.append(np.mean(smplR))
            map(self.rtrace.append, list(smplR[:steps+1,0]))
            epsilontrace.append(epsilon)

            if self.nmax_games > 0:
                self.nGames.append(steps+1)
                if steps >= self.nmax_games: 
                    nCompGames += 1
                    self.completeGames.append(1)
                else:
                    self.completeGames.append(0)

        return (self.rtrace, epsilontrace)       

    def update_policy(self, smplX, smplA, smplR, smplX1):
        ## Update the Q and V table and H (history)
        key = self.get_key(smplX, smplA)
        skey = self.get_key(smplX)
        r1 = smplR[0]
        a_j = list(smplA[:self.agentIdx]) + list(smplA[self.agentIdx+1:])

        stidx = self.agentIdx 
        endidx= stidx+self.model.get_state_dim()
        s1j = smplX1[stidx:endidx]
        s1Moves = self.model.get_actions(s1j)  # to get possible moves, extract state of agent j
        s1key = self.get_key(smplX1)
        # Value(St+1)
        qs = self.values(smplX1, s1Moves)
        if qs is None: return
        self.V[s1key] = max(qs)

        # Q update
        if not self.Q.has_key(key):
            self.Q[key] = 0
        self.Q[key] = (1 - self.alpha) * self.Q[key] +\
                        self.alpha * (r1 + self.gamma * self.V[s1key])
        #print key, r1,  Q[key]

        # add history 
        if not self.H.has_key(skey):
            self.H[skey] = []
        self.H[skey].append(a_j) # H[s] U a_j
        if len(self.H[skey]) > self.n_history: 
            self.H[skey].pop()

        self.Pi[skey] = float(self.ncount(self.H[skey][:-self.n_samples], a_j))\
                            / float(self.n_samples)


