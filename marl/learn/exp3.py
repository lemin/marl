""" Exponential-weight algorithm for Exploration and Exploitation (Exp3)

                                                by Jake Lee (lemin)

    For reference,
    ..  [1] Peter Auer
            The non-stochastic multi-armed bandit problem
            SIAM Journal on Computing 32, 48-77 (2002)

    example usage:
        agent = Exp3(agent.MatchAgent(), agIdx=0)
        agent.start()
"""
import numpy as np
from grad import Gradient
import math


class Exp3(Gradient):
    """ Exponential-weight algorithm for Exploration and Exploitation (Exp3)
            referenced codes by Crandall's

        Methods
        -------
        train(self, **params)
            train Exp3

        References
        ----------
        Peter Auer. The non-stochastic multi-armed bandit problem
            http://cseweb.ucsd.edu/~yfreund/papers/bandits.pdf
    """

    def train(self, **params):

        self._actions = self.model.get_actions()
        self._nactions = len(self._actions)

        nReps = params.pop("nReps", 100)
        nSteps = params.pop("nSteps", 1)
        N = params.pop("N", [nSteps] * nReps)

        #nmax_games = params.pop('record', 0)

        ### Initial epsilon is 1, for fully random action selection
        epsilon = 1
        epsilonType, epsilonRate, epsilons, epsilon =\
                    self.get_eps_policy(nReps, **params)

        self.rtrace = []
        ptrace = []
        self.completeGames = []
        self.nGames = []
        nCompGames = 0

        g = nSteps * nReps * self._nactions
        e = math.exp(1)
        self.gamma = min(1., math.sqrt((self._nactions * math.log(self._nactions)) / ((e - 1) * g)))

        # slight modification for pi and z
        # incrementally add states using dictionary
        # in get_samples, Pi for each state is initialized
        self.wt = np.ones(self._nactions)
        self.Pi = (1 - self.gamma) * np.ones(self._nactions) / self._nactions + self.gamma / self._nactions
        #print "gamma :", gamma
        #print "pi: ", self.Pi

        for reps  in xrange(nReps):

            if self.flgTerm: break

            self.print_progress(reps, nReps)

            ## Collect N[reps] samples, each being s, a, r, s1, a1
            smplX, smplR, smplA, steps = self.get_samples(N[reps], None)

            epsilon = self.get_cur_epsilon(epsilon, epsilonType,
                                            epsilonRate, epsilons, reps)
            map(self.rtrace.append, list(smplR[:steps+1,0]))
            ptrace.append(self.Pi[0])

            if self.nmax_games > 0:
                self.nGames.append(steps+1)
                if steps >= self.nmax_games: 
                    nCompGames += 1
                    self.completeGames.append(1)
                else:
                    self.completeGames.append(0)

        return (self.rtrace, None, ptrace)

    def update_policy(self, smplX, smplA, smplR):

        a = smplA
        if isinstance(self._actions, list):
            ai = self._actions.index(a)
        else:
            ai = int(np.where(np.all(self._actions == a, 1))[0])

        # update weights
        xj = np.zeros(self._nactions)
        xj[ai] = smplR / self.Pi[ai]
        self.wt = self.wt * np.exp(self.gamma * xj / self._nactions)
        #print "xj: ", xj
        #print "wt: ", wt

        # update policy
        self.Pi = (1 - self.gamma) * self.wt / sum(self.wt) + self.gamma / self._nactions
        #print "pi: ", self.Pi
