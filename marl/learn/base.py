""" Multiagent Learning Agent

                        by Jake Lee (lemin) 
"""
from .. import marlComm as comm
from threading import Thread
import numpy as np
import sys
from collections import Iterable


#
#  MAS framework
#
class MASframe(Thread):
    """ Multiagnet (Superclass)

        Parameters
        ----------
        model
        server
        agentIdx

        Attributes
        ----------
        showProgress    boolean
                        option for display
        flgTerm         boolean
                        finish process when the flag is set
        bulkopt         boolean
                        bulk messaging option
        sock            socket
                        communication socket

        Methods
        -------
        set_params(self, **params)
            manuall parameter setting

        start_agent(self, **params)
        run(self)
            start/run agent thread

        debug_file(self, string)
            debug and store the message into a file

        get_eps_policy(self, nReps, **rlparams)
        get_cur_epsilon(self, eps, epsType, epsRate, epsilons, reps)
            epsilon policy configuration reader and controller
    """

    def __init__(self, model, agIdx=-1, server=('localhost',3030)):
        Thread.__init__(self)
        self.model = model 
        self.sock,_ = comm.openSock()
        self.server = server # global tracker addr/port
        self.agentIdx = agIdx
        self.showProgress = False
        self.flgTerm = False
        self.bulkopt = False
        self.discrete = False
        

    def __del__(self):
        if self.sock is not None: comm.closeSock(self.sock)

    def set_params(self, **params):
        # option for recording games played
        self.nmax_games = params.pop('record', 0)
        if self.nmax_games > 0:
            self.nmax_games -= 1
        self.params=params

    def set_progress_on(self):
        self.showProgress = True

    def set_discrete_on(self):
        self.discrete = True

    def print_progress(self, reps, nReps):
        if self.showProgress and nReps > 5 and reps % 5 == 0: # show alive
            sys.stdout.write(".")
            sys.stdout.flush()

    def get_key(self, *args):
        state = []
        for arg in args:
            if isinstance(arg, Iterable):
                state += list(arg)
            else:
                state.append(arg)

        if self.discrete:
            state = list(np.ceil(state))
        return tuple(state)

    def start_agent(self, **params):
        pass

    def run(self):
        self.start_agent(**self.params)
        comm.closeSock(self.sock)

    def debug_file(self, string):
        fname = "debug-"+str(self.agentIdx)
        with open(fname, 'a+') as f:
            f.write(string+"\n")

    def get_eps_policy(self, nReps, **rlparams):
        """ return epsilon policy based on parameters. """

        epsilonType = rlparams.pop("epsilonType", 'expDecrease') 
        epsilonSlope = rlparams.pop("epsilonSlope", 6)
        epsilonStart = rlparams.pop("epsilonStart", 0)
        epsilonEnd = rlparams.pop("epsilonEnd", -1)
        fixedEpsilon = rlparams.pop("fixedEpsilon", 0.5)
        finalEpsilon = rlparams.pop("finalEpsilon", 0.001)
        epsilons = None
        epsilon = 1
        epsilonRate = 1

        if epsilonType == 'fixed': 
            epsilon = fixedEpsilon
        elif epsilonType == '-tanh':
            if epsilonEnd < 0 or epsilonEnd > nReps: 
                epsilonEnd = nReps
            sigmoidRange = epsilonEnd - epsilonStart

            step= (2. * epsilonSlope + 1.) / sigmoidRange
            epsx = list(np.arange(-epsilonSlope, epsilonSlope+step, step))
            epsilons = [1] * epsilonStart + list((-np.tanh(epsx) + 1) / 2)\
                        + [0] * (nReps - epsilonEnd)
        else: # 'expDecrease'
            epsilonRate = np.exp(np.log(finalEpsilon) / nReps)
            #print "epsilon decay rate = %d" % epsilonRate

        return (epsilonType, epsilonRate, epsilons, epsilon)

    def get_cur_epsilon(self, eps, epsType, epsRate, epsilons, reps):
        """ return computed current epsilon """

        if epsType=='expDecrease': 
            return eps * epsRate
        elif epsType=='-tanh':
            return  epsilons[reps]
        elif epsType =='fixed':
            return eps


