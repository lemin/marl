"""learning agents"""
from .base import MASframe

__all__ = ["apq", "exp3", "gigawolf", "grad", "marlnn", "discrete", "triv", "wpl"]
