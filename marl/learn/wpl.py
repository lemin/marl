""" Weighted Policy Learner (WPL)

                                    by Jake Lee (lemin)

    For reference,
    ..  [1] Sherief Abdallah and Victor Lesser
            A Multiagent Reinforcement Learning Algorithm with
            Non-linear Dynamics
            Journal of Artificial Intelligence Research 33:521-549 

    example usage:
        agent = WPL(agent.MatchAgent(), agIdx=0)
        agent.gamma = 0.95
        agent.eta = 0.001
        agent.start()
"""
import numpy as np
from grad import Gradient
from copy import deepcopy as copy


class WPL(Gradient):
    """ Weighted Policy Learner (WPL)
            referenced codes by Crandall's

        Parameters
        ----------
        gamma   float
                discount rate
        eta     float
                learning rate

        Methods
        -------
        train(self, **params)
            train WPL

        References
        ----------
        Sherief Abdallah and Victor Lesser.
        A Multiagent Reinforcement Learning Algorithm with Non-linear Dynamics
            http://www.aaai.org/Papers/JAIR/Vol33/JAIR-3314.pdf
    """

    def __init__(self, model, agIdx=-1, server=('localhost', 3030), **param):
        Gradient.__init__(self, model, agIdx, server)
        self.gamma = param.pop("gamma", 0.9)
        self.eta = param.pop('eta', 0.01)

    def train(self, **params):

        nReps = params.pop("nReps", 100)
        nSteps = params.pop("nSteps", 1)
        N = params.pop("N", [nSteps] * nReps)

        #nmax_games = params.pop('record', 0)

        ### Initial epsilon is 1, for fully random action selection
        epsilonType, epsilonRate, epsilons, epsilon =\
                    self.get_eps_policy(nReps, **params)
        self.rtrace = []
        ptrace = []

        # slight modification for pi and z
        # incrementally add states using dictionary
        # in get_samples, Pi for each state is initialized
        self._actions = self.model.get_actions()
        self._nactions = len(self.model.get_actions())
        self.Pi = self.gen_random_dist(self._nactions)

        self.completeGames = []
        self.nGames = []
        nCompGames = 0
        """ For test convergenece...
        if self.agentIdx == 0:
            self.Pi = np.array([0.9, 0.1])
        else:
            self.Pi = np.array([0.1, 0.9])
        """

        #self.Pi = np.random.rand(nactions)
        #self.Pi = self.Pi / np.sum(self.Pi)
        self._R = np.zeros(self._nactions)

        for reps  in xrange(nReps):

            if self.flgTerm: break
            
            self.print_progress(reps, nReps)

            ## Collect N[reps] samples, each being s, a, r, s1, a1
            smplX, smplR, smplA, steps = self.get_samples(N[reps], None)

            epsilon = self.get_cur_epsilon(epsilon, epsilonType,
                                            epsilonRate, epsilons, reps)
            #rtrace.append(np.mean(smplR))
            map(self.rtrace.append, list(smplR[:steps+1, 0]))
            #map(ptrace.append, list(smplA[:step+1,0]))
            #map(ptrace.append, list(self.Pi[0])
            ptrace.append(self.Pi[0])

            if self.nmax_games > 0:
                self.nGames.append(steps+1)
                if steps >= self.nmax_games: 
                    nCompGames += 1
                    self.completeGames.append(1)
                else:
                    self.completeGames.append(0)

        return (self.rtrace, None, ptrace)

    def update_policy(self, smplX, smplA, smplR):

        DPI = 0.1
        pi_t = self.Pi

        a = smplA
        if isinstance(self._actions, list):
            ai = self._actions.index(a)
        else:
            ai = int(np.where(np.all(self._actions == a, 1))[0])
        self._R[ai] = self._R[ai] + self.gamma * (smplR - self._R[ai])

        dV = np.zeros(self._nactions)
        Vt = np.sum(self._R * pi_t)
        for i in xrange(self._nactions):
            z_t = copy(pi_t)
            z_t[i] = z_t[i] + DPI
            z_t = self.proj(z_t)
            Vt1 = np.sum(self._R * z_t)
            dV[i] = Vt1 - Vt

        # update policy
        posi = np.where(dV >= 0)
        tmp_pi = copy(pi_t)
        tmp_pi[posi] = 1 - pi_t[posi]
        dpi_t = dV * self.eta * tmp_pi
        self.Pi = self.proj(pi_t + dpi_t)
        #print "pi:", self.Pi, "pi_t:", pi_t, "dpi_t:", dpi_t


