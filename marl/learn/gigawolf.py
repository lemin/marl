""" Generalized Infinitesimal Gradient Ascent with
    Win or Learn Fast (WoLF) heuristic

                                by Jake Lee (lemin)

    For reference,
    ..  [1] M. Bowling
            Convergence and No-Regret in Multiagent Learning
            Advances in Neural Information Processing Systems, 
            vol. 17MIT Press, Cambridge, MA (2005)

    example usage:
        agent = GIGAWoLF(agent.MatchAgent(), agIdx=0)
        agent.gamma = 0.95
        agent.start()
"""
import numpy as np
from grad import Gradient
from copy import deepcopy as copy


class GIGAWoLF(Gradient):
    """ GIGA-WoLF
           ref. codes by Crandall and Leyton-Brown (MALT)

        Parameters
        ----------
        gamma   float
                discount rate

        Methods
        -------
        get_dist(self, skey=None, nactions=0):
            get initial random distribution for Pi and Z

        train(self, **params)
            train GIGA-WoLF

        References
        ----------
        M. Bowling. Convergence and No-Regret in Multiagent Learning
            http://webdocs.cs.ualberta.ca/~bowling/papers/04nips.pdf
    """

    def __init__(self, model, agIdx=-1, server=('localhost',3030), **param):
        Gradient.__init__(self, model, agIdx, server)
        self.gamma = param.pop('gamma', 0.1)

    def get_dist(self, skey=None, nactions=0):
        if not self.Pi.has_key(skey): 
            #self.Pi[skey] = np.array([1. / float(nactions)] * nactions)
            self.Pi[skey] = self.gen_random_dist(nactions)
        if self.Z is not None and not self.Z.has_key(skey): 
            #self.Z[skey] = np.array([1. / float(nactions)] * nactions)
            self.Z[skey] = self.gen_random_dist(nactions)
        return self.Pi[skey]

    def train(self, **params):

        nReps = params.pop("nReps", 100)
        nSteps = params.pop("nSteps", 1)
        N = params.pop("N", [nSteps] * nReps)

        #nmax_games = params.pop('record', 0)

        ### Initial epsilon is 1, for fully random action selection
        epsilon = 1
        epsilonType, epsilonRate, epsilons, epsilon =\
                    self.get_eps_policy(nReps, **params)

        self.rtrace = []
        ptrace = []
        self.completeGames = []
        self.nGames = []
        nCompGames = 0

        # slight modification for pi and z
        # incrementally add states using dictionary
        # in get_samples, Pi for each state is initialized
        self.Pi = {}
        self.Z = {}
        self._actions = self.model.get_actions()
        self._nactions = len(self.model.get_actions())
        self._R = np.ones(self._nactions) * 0.5  # init to 0.5 to make it update

        ### GIGA-WoLF does not maintain strategy of other agents
 
        for reps  in xrange(nReps):

            if self.flgTerm: break

            self.print_progress(reps, nReps)

            ## Collect N[reps] samples, each being s, a, r, s1, a1
            smplX,smplR, smplA,steps = self.get_samples(N[reps], None)

            epsilon = self.get_cur_epsilon(epsilon, epsilonType,
                                            epsilonRate, epsilons, reps)
            #rtrace.append(np.mean(smplR))
            map(self.rtrace.append, list(smplR[:steps+1,0]))
            #map(ptrace.append, list(smplA[:step+1,0]))
            #ptrace.append(self.Pi[first_key][0])

            if self.nmax_games > 0:
                self.nGames.append(steps+1)
                if steps >= self.nmax_games: 
                    nCompGames += 1
                    self.completeGames.append(1)
                else:
                    self.completeGames.append(0)

        return (self.rtrace, None, ptrace)

    def update_policy(self, smplX, smplA, smplR):

        skey = self.get_key(smplX)
        z_t = copy(self.Z[skey])
        pi_t = copy(self.Pi[skey])

        a = smplA
        if isinstance(self._actions, list):
            ai = self._actions.index(a)
        else:
            ai = int(np.where(np.all(self._actions == a, 1))[0])
        self._R[ai] = self._R[ai] + self.gamma * (smplR - self._R[ai])
        
        eta = 0.002 #1 / np.sqrt(t)
        pi_hat = pi_t + eta * self._R
        pi_hat = self.proj(pi_hat)

        z_t1 = z_t + eta * self._R / 3.
        z_t1 = self.proj(z_t1)
        self.Z[skey] = z_t1

        denom = np.linalg.norm(z_t1 - pi_hat)
        num = np.linalg.norm(z_t1 - z_t)
        delta = min(1., num / denom) if denom != 0 else 1.
        self.Pi[skey] = pi_hat + delta * (z_t1 - pi_hat)
        #print "t = ", t, "skey:", skey, "pi:", self.Pi[skey], "delta", eta * (z_t1 - pi_hat)
        #print "eta", eta, "(",num,"/", denom,  ")", "z_t1", z_t1, "pi_hat", pi_hat
        #t += 1


