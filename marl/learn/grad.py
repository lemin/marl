""" Gradient ascent/descent multiagent learner
    Base class for gradient-based learner such as
    WPL, GIGA-WolF, and Exp3

                                by Jake Lee (lemin)
"""
import numpy as np
from .. import marlComm as comm
from base import MASframe
from copy import deepcopy as copy
import random


class Gradient(MASframe):
    """ Gardient Multiagnet (Superclass)

        Methods
        -------
        gen_random_dist(self, nactions):
            generate random distribution
        get_dist(self, skey=None, nactions=0):
            return current policy distribution
        epsilon_greedy(self, s, s_j, epsilon):
            select an action based on current policy
        get_samples(self, numSamples, epsilon) :
            run a simulation and collect state changes and rewards
        start_agent(self, **params):
            run entire simulation thread
        proj(self, policy):
            policy projection to valid space
        train(self, **params):
            learning policy
    """

    def __init__(self, model, agIdx=-1, server=('localhost', 3030), **param):
        MASframe.__init__(self, model, agIdx=-1, server=('localhost', 3030), **param)
        self.set_discrete_on()

    def gen_random_dist(self, nactions):
        dist = np.random.rand(nactions)
        return dist / np.sum(dist)

    def get_dist(self, skey=None, nactions=0):
        return self.Pi
        
    ### Pi strategy distribution of an agent (not for all)
    ### Z is approx. NE strategy
    ### do not depend on epsilon policy but the strategy
    def epsilon_greedy(self, s, s_j, epsilon, t):
        """ named epsilon_greedy for consistency, but
            do not use epsilon policy
        """
        actions = self.model.get_actions(s)
        if random.uniform(0,1) < epsilon: # random
            i = actions[random.sample(range(len(actions)),1)[0]]
        else:                           # greedy 
            skey = self.get_key(s)
            nactions = len(actions)
            Pi = self.get_dist(skey, nactions)

            ## sample an action based on the distribution Pi
            ## Kitagawa sampling by Nando de Freitas 
            i = np.sum(np.cumsum(Pi) < random.random())
        return actions[i]
        
    def get_samples(self, numSamples, epsilon) :

        X = np.zeros((numSamples,self.model.get_state_dim()))
        R = np.zeros((numSamples,1))
        A = np.zeros((numSamples,self.model.get_action_dim()))

        s = self.model.init()
        # join to global env 
        t = 0
        tj, s0all = comm.joinMarl(self.sock, s,self.agentIdx)
        if tj is None: 
            return (X,R,A,-1)

        if self.agentIdx == -1: j= tj
        else: j = self.agentIdx
        #sys.stdout.write("MARL: "+ str(j)+ "----Joined\n")

        s = self.model.get_S(s0all, j)
        a = self.epsilon_greedy(s, None, None, t) # d

        for step in range(numSamples):
            if self.flgTerm:
                step -= 1
                break

            # send action and get next states from Global tracker 
            t = t+1
            tmp, termflg, Rg, s1all,aall = comm.actionMarl(self.sock, j, t, s, a)
            if tmp==-1:
                step -= 1
                break
            if not isinstance(s1all, np.ndarray):
                s1all = np.asarray(s1all)
            s1 = self.model.get_S(s1all, j)

            r1 = self.model.get_reward(s,s1,a, Rg,j, step, numSamples)
            a1 = self.epsilon_greedy(s, None, None, t)

            # update policy
            self.update_policy(s, a, r1)

            # Collect
            X[step,:] = s
            R[step,0] = r1
            A[step,:] = a
            
            if self.model.check_early_stop(Rg) or termflg:
                break

            # next state
            s,a = s1,a1

        # end of episode. disconnect from GlobalTracker
        comm.exitMarl(self.sock, j)

        return (X, R, A, step)

    def start_agent(self, **params):
        if not comm.connectSock(self.sock,self.server) : return 
        self.rtrace = None
        self.rtrace, _, self.ptrace = self.train(**params)

    # project policy onto valid dist space
    def proj(self, policy):
        """ Implements a version of the retraction operator from:
		    Srihari Govindan and Robert Wilson -
  		    "A Global Newton Method to compute Nash Equilibria"

            referenced codes from MALT
        """
        dist = copy(policy)
        length = len(dist)
        if not isinstance(dist, np.ndarray):
            dist = np.asarray(dist)
        i = 0
        while (np.sum(dist) != 1 and i < length) or\
            np.any(dist < 0) or np.any(dist > 1):

            if i != 0:
                nonz = (dist != 0)
            else:
                nonz = np.array([True] * length)

            if sum(nonz) != 0:
                #findnz = np.where(nonz)[0]
                #print "findnz", findnz
                v_n = (sum(dist) - 1) / sum(nonz)
                dist[nonz] = dist[nonz] - v_n
                if np.any(dist < 0):
                    dist[dist < 0] = 0
                #for di in findnz:
                #    dist[di] = np.max(dist[di], 0)

            i += 1
                
        return dist

    def train(self, **params):
        pass

    def update_policy(self, smplX, smplA, smplR):
        pass
