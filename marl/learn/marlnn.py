""" multiagent reinforcement learning for neural network

                            by Jake Lee (lemin)
"""
import numpy as np
from ..approx.nnetq import NeuralNetQ
import random 
from .. import marlComm as comm
import sys
from scipy.spatial.distance import cdist
from mpl_toolkits.mplot3d import Axes3D
import matplotlib
matplotlib.use('Agg')  # backend
from matplotlib import cm
import matplotlib.pyplot as plt
from base import MASframe


class MARL(MASframe):
    """ reinforcement learning in MAS 
    """

    def __init__(self, model, agIdx=-1, server=('localhost',3030), **param):
        MASframe.__init__(self, model, agIdx, server)
        # search action
        self.set_search_action(False)
        self.filter = param.pop('filter', 'all')
        self.abstract = param.pop('abstract', 'none')
        self.expEpsDec = param.pop('expdec',False)
        self.epsilon = 1
        self.collectAllRewards=True

        self.discAct = False ## support simplification for discrete action

    def get_nnNI(self):
        return self.model.nnNI

    def set_search_action(self, enable=True, **params):
        """ set search action parameters. """
        self.search_act = enable
        self._updact_wpre = params.pop('wpre', 1e-8)
        self._updact_fpre = params.pop('wpre', 1e-8)
        self._updact_niter= params.pop('niter', 10000)

    def epsilon_greedy(self, s, s_j, epsilon):
        actions = self.model.get_actions(s)
        s = self.model.transform_state(s)


        if self.filter=='all':
            sm = convertToNPmatrix(s)
        else:
            sm = np.hstack((convertToNPmatrix(s), convertToNPmatrix(s_j)))

        if self.discAct: 
            qs = self.__nn.use(sm)[0]
            if random.uniform(0,1) < epsilon :
                bestActIndex = random.sample(range(len(actions)), 1)[0]
            else:
                bestActIndex = np.argmax(qs)
            a = actions[bestActIndex]
            Q = qs[bestActIndex]
            return (Q, a, bestActIndex)

        else :
            a = self.model.transform_action(self.model.get_random_action())
            am = convertToNPmatrix(a)

            if random.uniform(0,1) < epsilon :
                Q = self.__nn.use(np.hstack((sm,am)))
            else:
                if self.search_act:
                    # search the best action 
                    a = self.__nn.search_action(sm, am, 
                                                self._updact_wpre,
                                                self._updact_fpre,
                                                self._updact_niter)
                    a = self.model.get_bound_action(a)
                    am = convertToNPmatrix(a)
                    Q = self.__nn.use(np.hstack((sm,am)))
                else: # remove loop 20110107
                    #actions = self.model.get_actions(s)
                    mactions = actions
                    if not isinstance(mactions, np.ndarray): #type(actions)!=type(np.array([])):
                        mactions = np.array([mactions]).T
                    inputs = np.hstack((np.tile(sm,(len(mactions),1)),mactions))
                    qs = self.__nn.use(inputs)
                    i = np.argmax(qs)
                    a = actions[i]
                    Q = qs[i,0]
            return (Q, a, None)

    def get_s_j(self,sall, j):
        return self.model.get_S(sall, -j-1)
        """
        print type(sall)
        print "index j: ", j
        if np.min(sall[:j,:].shape)==0:
            s_j = sall[j+1:,:]
        elif np.min(sall[j+1:,:].shape)==0:
            s_j = sall[:j,:]
        else:
            s_j = np.vstack((sall[:j,:],  sall[j+1:,:]))
        return s_j
        """

    def getS_j(self,sall, j,s0all):
        if isinstance(sall, list):
            sall = np.asarray(sall)

        s_j = self.get_s_j(sall,j)

        if self.filter=='all':
            return None
        elif self.filter=='none': 
            if self.abstract == 'none':
                return s_j.flat
                
            dist = cdist([sall[j,:]], s_j)    
            if self.abstract =='norm':
                return dist.flat
            elif self.abstract =='cnt':
                return [np.sum(dist<20)]
            elif self.abstract =='delta':
                if s0all==None:    
                    return [1]*len(dist.flat)
                else:
                    s_j_old = self.get_s_j(s0all,j)
                    dist_old = cdist([s0all[j,:]], s_j_old)    
                    approach = ((dist - dist_old) < 0).astype(int)
                    return approach.flat
        else:
            iflt = int(self.filter)
            return s_j[:iflt,:].flat

        
    def get_samples(self, numSamples, epsilon) :
        X = np.zeros((numSamples,self.model.nnNI))
        R = np.zeros((numSamples,1))
        Q = np.zeros((numSamples,1))
        Y = np.zeros((numSamples,1))
        if self.discAct:
            AI = np.zeros((numSamples,1))
        else:
            AI = None

        s = self.model.init()
        # join to global env 
        t = 0
        tj,s0all = comm.joinMarl(self.sock, s,self.agentIdx)
        ##TODO: put conversion  to two dimension for Octopus
        if tj is None: 
            return (X,R,Q,Y,AI, -1)

        if self.agentIdx == -1: j= tj
        else: j = self.agentIdx
        #sys.stdout.write("MARL: "+ str(j)+ "----Joined\n")

        if self.expEpsDec: eps = self.epsilon
        else: eps= epsilon

        s = self.model.get_S(s0all, j)  # update s after join (can be different model.init())
        s_j = self.getS_j(s0all,j, None) #s * (self.nagents-1)
        q,a,ai = self.epsilon_greedy(s, s_j, eps)

        sall = s0all 

        if self.bulkopt: # Bulk messaging is possible if state is previous actions!!!
            for step in range(numSamples):
                if self.flgTerm : break

                s1 = [a] # next()
                #q1,a1,a1i = self.epsilon_greedy(s1, s_j if self.filter=='none' else None, eps)
                q1,a1,a1i = self.epsilon_greedy(s1, s_j, eps)

                X[step,0:len(s)] = s
                if self.filter=='all':
                    if self.discAct == False:
                        X[step,len(s):] = a
                else:
                    X[step,len(s):len(s)+len(s_j)] = s_j
                    if self.discAct == False:
                        X[step,len(s)+len(s_j):] = a

                Q[step,0],Y[step,0] = q1,q
                if self.discAct: AI[step,:] = ai

                s,a,q,ai = s1,a1,q1,a1i
            
            if self.flgTerm: step=-1
            else:
                _, termflg, Rgs, s1all = comm.actionMarlBulk(self.sock, j, step+1, s, AI)
                if s1all is None: 
                    print "SRL:: None", step 
                    step=-1
                else:
                    #R[:(step+1),0] = s1all[:(step+1),j]
                    R[:(step+1),0] = s1all[:(step+1)]

        else:
            for step in range(numSamples):
                if self.flgTerm:
                    step -= 1
                    break

                if self.expEpsDec:
                    self.epsilon *= self.expEpsDec
                    eps = self.epsilon
                
                #s1 = self.__model.next(s,a)  ---- for single model only
                # send action and get next states from Global tracker 
                t = t+1
                #sys.stdout.write("MARL: "+ str(j)+ "----Action\n")
                tmp, termflg, Rg, s1all,_ = comm.actionMarl(self.sock, j, t, s, a)
                if tmp==-1: break
                if not isinstance(s1all, np.ndarray):
                    s1all = np.asarray(s1all)
                #s1 = s1all[j,:]  # pick state only for it self
                s1 = self.model.get_S(s1all, j)  # update s after join (can be different model.init())

                # FIXME: put filtering and discretization here !!!
                s1_j = self.getS_j(s1all,j, sall)

                # now calculate reward !
                # For now, ignore global reward Rg
                r1 = self.model.get_reward(s,s1,a, Rg,j, step, numSamples)

                q1,a1,a1i = self.epsilon_greedy(s1, s1_j, eps)

                # Collect
                ts = self.model.transform_state(s)
                ta = self.model.transform_action(a)
                X[step,0:len(ts)] = ts
                if self.filter=='all':
                    if self.discAct == False:
                        X[step,len(ts):] = ta
                else:
                    ts_j = self.model.transform_state(s_j)
                    X[step,len(s):len(s)+len(s_j)] = ts_j
                    if self.discAct == False:
                        X[step,len(ts)+len(ts_j):] = ta

                R[step,0],Q[step,0],Y[step,0] = r1,q1,q
                if self.discAct: AI[step,:] = ai

                # In case of PvP::::
                #if abs(r1)==500: break # for PvP
                #if np.max(np.abs(Rg))==500: break  # for PvP
                if self.model.check_early_stop(Rg) or termflg:
                    break
                
                # next state
                s,a,q = s1,a1,q1
                sall, s_j = s1all, s1_j

        #print str(j)+"  ExitMarl"
        # end of episode. disconnect from GlobalTracker
        comm.exitMarl(self.sock, j)

        #print str(j)+" RETURN..."
        return (X,R,Q,Y,AI, step)

    def make(self, nunits, **rlparams):
        stRange=self.model.get_state_range()
        self.__nn = NeuralNetQ(nunits, stRange);

    def pretrain(self, pretrainW):
        self.__nn.set_hunit(pretrainW)

    def get_NN(self):
        return self.__nn

    def start_agent(self, **params):
        """ run SRL agent by calling train
            this function assumes that neural network is already created explicitly
        """
        if not comm.connectSock(self.sock,self.server) : return 
        self.rtrace = None
        self.rtrace, self.xtrace, _, _, _= self.train(**params)
 
    def train(self, nunits=None, **rlparams):
        
        epsilonType=rlparams.pop("epsilonType", 'expDecrease') 
        epsilonSlope=rlparams.pop("epsilonSlope", 6)
        epsilonStart=rlparams.pop("epsilonStart", 0)
        epsilonEnd=rlparams.pop("epsilonEnd", -1)
        fixedEpsilon=rlparams.pop("fixedEpsilon", 0.5)
        finalEpsilon=rlparams.pop("finalEpsilon", 0.001)

        # RL params
        lmbd = rlparams.pop("Lambda", 0.0)
        gamma = rlparams.pop("Gamma", 0.9)
        nReps = rlparams.pop("nReps", 100)
        nSteps = rlparams.pop("nSteps", 1000)
        N = rlparams.pop("N", [nSteps]*nReps)

        # Pretraining
        pretrainW = rlparams.pop("pretrain",None)

        # NeuralNet
        bnnetarg = rlparams.pop("nnet",True)

        if nunits is not None and bnnetarg == True:
            self.make(nunits)
        if pretrainW!=None:
            self.pretrain(pretrainW)
            #print "Pretrain: ", pretrainW

        # SCG params
        niter= rlparams.pop("nIterations",1000)
        wprecision = rlparams.pop("wPrecision",1e-10)
        fprecision = rlparams.pop("fPrecision",1e-10)

        #nmax_games = rlparams.pop('record', 0)

        ### Initial epsilon is 1, for fully random action selection
        epsilon = 1
        if epsilonType=='fixed': 
            epsilon = fixedEpsilon
        elif epsilonType=='-tanh':
            if epsilonEnd<0 or epsilonEnd > nReps: 
                epsilonEnd = nReps
            sigmoidRange=epsilonEnd - epsilonStart

            step= (2.*epsilonSlope+1.)/sigmoidRange
            epsx = list(np.arange(-epsilonSlope,epsilonSlope+step, step))
            epsilons = [1]*epsilonStart + list((-np.tanh(epsx)+1)/2) + [0]*(nReps-epsilonEnd)
        else: # 'expDecrease'
            epsilonRate = np.exp(np.log(finalEpsilon)/nReps)
            #print "epsilon decay rate = %d" % epsilonRate
        
        ### Variables for plotting later
        xtrace = []
        ftrace = []
        self.rtrace = []
        epsilontrace = []
        wtrace = []
        self.completeGames = []
        self.nGames = []
 
        #nCompGames = 0
        #f = open('test.txt','a+') 
        for reps  in xrange(nReps):

            #sys.stdout.write(".") ## Show progress
            #sys.stdout.flush()
            if self.flgTerm: break
            
            if self.showProgress and nReps > 5 and reps % 5 == 0:
                sys.stdout.write("\rProgress: %3.1f%%" % (reps*100./nReps))
                sys.stdout.flush()
            ## Collect N[reps] samples, each being s, a, r, s1, a1
            smplX,smplR, smplQ, smplY, smplA,steps = self.get_samples(N[reps],epsilon)

            if steps>=0:
                ## Update the Q neural network.
                self.__nn.ftracep=True

                if self.discAct:
                    self.__nn.train(smplX[:steps+1,:], smplY[:steps+1,:], 
                                    smplR[:steps+1,:], smplQ[:steps+1,:], smplA[:steps+1,:],
                                    niter=niter, Lambda=lmbd, Gamma=gamma, 
                                    wprecision=wprecision, fprecision=fprecision)
                else:
                    self.__nn.train(smplX[:steps+1,:], smplY[:steps+1,:],
                                    smplR[:steps+1,:], smplQ[:steps+1,:],
                                    niter=niter, Lambda=lmbd, Gamma=gamma, 
                                    wprecision=wprecision, fprecision=fprecision)
            if epsilonType=='expDecrease': 
                epsilon *= epsilonRate
            elif epsilonType=='-tanh':
                epsilon = epsilons[reps]

            xtrace.append(smplX[:steps,:])
            #rtrace.append() # Return raw rewards 
            if self.collectAllRewards: # for TD
                map(self.rtrace.append, list(smplR[:steps+1,0]))
            else:
                self.rtrace.append(np.sum(smplR[:steps+1,:])/(steps+1))

            if self.nmax_games > 0:
                self.nGames.append(steps+1)
                if steps >= self.nmax_games: 
                    #nCompGames += 1
                    self.completeGames.append(1)
                else:
                    self.completeGames.append(0)

        #print "over 499::: ",nCompGames, "   ", np.sum(self.completeGames)
        xpos = np.linspace(0,100,51)
        ypos = np.linspace(0,100,51)
        xs, ys = np.meshgrid(xpos,ypos)
        actions = self.model.get_actions(None)
        states = np.vstack((xs.flat,ys.flat)).T

        ### plot Q values for debugging
        if False:
            qs = None
            for i in xrange(len(actions)):

                inputs = np.hstack((states,np.tile(actions[i,:],(len(states),1))))
                if qs is None:
                    qs = self.__nn.use(inputs)
                else:
                    qs = np.hstack((qs, self.__nn.use(inputs)))

            qsmax = np.max(qs,axis=1).reshape(xs.shape)

            fig = plt.figure(1)
            plt.clf()
            rect = fig.add_subplot(1,2,1).get_position()
            ax = Axes3D(fig, rect)
            ax.plot_surface(xs,ys,qsmax,cstride=1,rstride=1,cmap=cm.jet)
            ax.set_xlabel("$x$")
            ax.set_ylabel("$y$")
            ax.set_zlabel("Max Q")

            rect = fig.add_subplot(1,2,2).get_position()
            cs = plt.contourf(xs,ys,qsmax)
            plt.colorbar(cs)
            plt.xlabel("$x$")
            plt.ylabel("$\dot{x}$")
            plt.title("Max Q")
            #plt.draw()
            plt.show()

            #plt.savefig("Qplot-"+str(i)+".png")

        #f.close()
        return (self.rtrace, xtrace, epsilontrace, ftrace, wtrace)
        

def convertToNPmatrix(a):
    am = np.array(a)
    if am.ndim==1: am.shape = (1,len(am))
    elif am.ndim==0: am.shape = (1,1)
    return am


class SRL(MARL):
    def __init__(self, model, agIdx=-1, server=('localhost',3030), **param):
        tfilter = param.pop('filter', 'all')
        MARL.__init__(self, model, agIdx, server, filter='all', **param)


class ORL(MARL):
    def __init__(self, model, agIdx=-1, server=('localhost',3030), **param):
        tfilter = param.pop('filter', 'all')
        MARL.__init__(self, model, agIdx, server, filter='none', **param)
