import numpy as np
#from ..approx import neuralnet as nnet
from ..approx.nnetq import NeuralNetQ
import random 
from .. import marlComm as comm
import sys
from scipy.spatial.distance import cdist
from mpl_toolkits.mplot3d import Axes3D
import matplotlib
matplotlib.use('Agg')  # backend
from matplotlib import cm
import matplotlib.pyplot as plt
from base import MASframe
from collections import Iterable


#
# Reinforcement learning framework
#
class MARL_D(MASframe):

    def __init__(self, model, agIdx=-1, server=('localhost',3030), **param):
        MASframe.__init__(self, model, agIdx, server)
        self.filter = param.pop('filter', 'all')
        self.abstract = param.pop('abstract', 'none')
        self.nagents = param.pop('nagents', 1)
        self.epsilon = 1

    def epsilon_greedy(self, s, s_j, epsilon):
        actions = self.model.get_actions(s)

        if self.filter=='all':
            S = list(s)
        else:
            S = list(s) + list(s_j)

        if random.uniform(0,1) < epsilon :
            bestActIndex = random.sample(range(len(actions)), 1)[0]
        else:
            qs = []
            for a in actions:
                if isinstance(a, Iterable):
                    key = tuple(list(S) + list(a))
                else:
                    key = tuple(list(s) + [a])
                if not self.Q.has_key(key):
                    self.Q[key] = 0
                qs.append(self.Q[key])
            bestActIndex = np.argmax(qs)

        return actions[bestActIndex]

    def get_s_j(self,sall, j):
        return self.model.get_S(sall, -j-1)
        """
        if np.min(sall[:j,:].shape)==0:
            s_j = sall[j+1:,:]
        elif np.min(sall[j+1:,:].shape)==0:
            s_j = sall[:j,:]
        else:
            s_j = np.hstack((sall[:j,:],  sall[j+1:,:]))
        return s_j
        """

    def getS_j(self,sall, j,s0all):
        s_j = self.get_s_j(sall,j)

        if self.filter=='all':
            return None
        elif self.filter=='none': 
            if self.abstract == 'none':
                return s_j.flat
                
            dist = cdist([sall[j,:]], s_j)    
            if self.abstract =='norm':
                return dist.flat
            elif self.abstract =='cnt':
                return [np.sum(dist<20)]
            elif self.abstract =='delta':
                if s0all==None:    
                    return [1]*len(dist.flat)
                else:
                    s_j_old = self.get_s_j(s0all,j)
                    dist_old = cdist([s0all[j,:]], s_j_old)    
                    approach = ((dist - dist_old) < 0).astype(int)
                    return approach.flat
        else:
            iflt = int(self.filter)
            return s_j[:iflt,:].flat

        
    def get_samples(self, numSamples, epsilon) :
        R = np.zeros((numSamples,1))

        s = self.model.init()
        # join to global env 
        t = 0
        tj,s0all = comm.joinMarl(self.sock, s,self.agentIdx)

        if self.agentIdx == -1: j= tj
        else: j = self.agentIdx

        s = self.model.get_S(s0all, j)
        s_j = self.getS_j(s0all,j, None) #s * (self.nagents-1)
        a = self.epsilon_greedy(s, s_j, epsilon)

        sall = s0all 

        for step in range(numSamples):
            if self.flgTerm:
                step -= 1
                break

            # send action and get next states from Global tracker 
            t = t+1
            tmp, termflg, Rg, s1all,_ = comm.actionMarl(self.sock, j, t, s, a)
            if tmp==-1: break
            if not isinstance(s1all, np.ndarray):
                s1all = np.asarray(s1all)
            s1 = self.model.get_S(s1all, j)
            s1_j = self.getS_j(s1all,j, sall)
            r1 = self.model.get_reward(s,s1,a, Rg,j, step, numSamples)

            a1 = self.epsilon_greedy(s1, s1_j, epsilon)

            if self.filter == 'all':
                S = list(s)
                S1 = list(s1)
            else:
                S = list(s) + list(s_j)
                S1 = list(s1) + list(s_j)

            if step == (numSamples-1):
                self.update_Q(s, s_j, a, r1)
            else:
                self.update_Q(s, s_j, a, r1, s1, s1_j, a1)

            R[step, 0] = r1

            if self.model.check_early_stop(Rg) or termflg:
                break
            
            # next state
            s,a = s1,a1
            sall, s_j = s1all, s1_j

        comm.exitMarl(self.sock, j)

        return (R, step)


    def start_agent(self, **params):
        """ run SRL agent by calling train
        """
        if not comm.connectSock(self.sock,self.server) : return 
        self.rtrace = None
        self.rtrace = self.train(**params)
 
    def train(self, nunits=None, **rlparams):
        
        # RL params
        self.Lambda = rlparams.pop("Lambda", 0.0)
        self.Gamma = rlparams.pop("Gamma", 0.9)
        nReps = rlparams.pop("nReps", 100)
        nSteps = rlparams.pop("nSteps", 1000)
        N = rlparams.pop("N", [nSteps]*nReps)

        #nmax_games = rlparams.pop('record', 0)

        print "DISCRETE::::", self.nmax_games

        ### Initial epsilon is 1, for fully random action selection
        epsilonType, epsilonRate, epsilons, epsilon =\
                    self.get_eps_policy(nReps, **rlparams)

        ### Q TABLE ###
        self.Q = {}
        self.Q[None] = 0
        
        ### Variables for plotting later
        ftrace = []
        self.rtrace = []
        wtrace = []
        self.completeGames = []
        self.nGames = []
 
        nCompGames = 0
        for reps  in xrange(nReps):

            if self.flgTerm: break
            
            self.print_progress(reps, nReps)

            ## Collect N[reps] samples, each being s, a, r, s1, a1
            smplR,steps = self.get_samples(N[reps],epsilon)

            epsilon = self.get_cur_epsilon(epsilon, epsilonType,
                                            epsilonRate, epsilons, reps)

            map(self.rtrace.append, list(smplR[:steps+1,0]))

            if self.nmax_games > 0:
                self.nGames.append(steps+1)
                if steps >= self.nmax_games: 
                    nCompGames += 1
                    self.completeGames.append(1)
                else:
                    self.completeGames.append(0)

        return (self.rtrace)

    def update_Q(self, s, s_j, a, r, s1=None, s1_j=None, a1=None):
        """ update Q table """
        if self.filter == 'all':
            S, S1 = s, s1
        else:
            S = list(s) + list(s_j)
            if s1 is not None:
                S1 = list(s1) + list(s1_j)

        if not isinstance(a, Iterable):
            a, a1 = [a], [a1]

        key = tuple(list(S) + list(a))
        keyn = None

        if not self.Q.has_key(key):
            self.Q[key] = 0
        if s1 is not None:
            keyn = tuple(list(S1) + list(a1))
            if not self.Q.has_key(keyn):
                self.Q[keyn] = 0

        self.Q[key] += self.Lambda * (r + self.Gamma * self.Q[keyn] - self.Q[key])

        ### for matrix game, each step is iid. 
        ### can it be the same for this???? no mdp relation btn each game?
        

class SRLD(MARL_D):
    def __init__(self, model, agIdx=-1, server=('localhost',3030), **param):
        tfilter = param.pop('filter', 'all')
        MARL_D.__init__(self, model, agIdx, server, filter='all', **param)


class ORLD(MARL_D):
    def __init__(self, model, agIdx=-1, server=('localhost',3030), **param):
        tfilter = param.pop('filter', 'all')
        MARL_D.__init__(self, model, agIdx, server, filter='none', **param)
