"""
    trivial policy (no learning considered)
        random / sequential / static policy
    
                                    lemin (Jake Lee)
"""
import numpy as np
from .. import marlComm as comm
from base import MASframe


class MARandom(MASframe):

    def get_samples(self, numSamples) :
        X = np.zeros((numSamples,self.model.nnNI))
        R = np.zeros((numSamples,1))
        A = np.zeros((numSamples,self.model.get_action_dim()))

        s = self.model.init()
        # join to global env 
        t = 0
        tj,s0all = comm.joinMarl(self.sock, s,self.agentIdx)
        if tj is None: 
            return (X,R,A,-1)

        if self.agentIdx == -1: j= tj
        else: j = self.agentIdx
        #sys.stdout.write("RAND: "+ str(j)+ "----Joined\n")
        s = self.model.get_S(s0all, j)
        a = self.model.get_random_action()

        if self.bulkopt: 
            for step in range(numSamples):
                if self.flgTerm : break
                s1 = [a] # next()
                a1 = self.model.get_random_action()
                A[step,:] = a
                s,a = s1,a1

            if self.flgTerm: step=-1
            else:
                _, _, s1all = comm.actionMarlBulk(self.sock, j, step+1, s, A)
                if s1all is None: step=-1
                else:
                    #R[:(step+1),0] = s1all[:(step+1),j]
                    R[:(step+1),0] = s1all[:(step+1)]
            
        else:
            for step in range(numSamples):
                if self.flgTerm:
                    step -= 1
                    break
                # send action and get next states from Global tracker 
                t = t+1
                #sys.stdout.write("RAND: "+ str(j)+ "----Action\n")
                tmp, termflg, Rg, s1all,_ = comm.actionMarl(self.sock, j, t, s, a)
                if tmp==-1: break
                if not isinstance(s1all, np.ndarray):
                    s1all = np.asarray(s1all)
                s1 = self.model.get_S(s1all, j)

                # For now, ignore global reward Rg
                r1 = self.model.get_reward(s,s1,a, Rg,j, step, numSamples)

                a1 = self.model.get_random_action()

                # Collect
                X[step,0:len(s)] = s
                X[step,len(s):] = a
                R[step,0] = r1
                A[step,:] = a

                #if np.max(np.abs(Rg))==500:break  # for PvP
                if self.model.check_early_stop(Rg) or termflg:
                    break
                
                # next state
                s,a = s1,a1

        # end of episode. disconnect from GlobalTracker
        comm.exitMarl(self.sock, j)

        return (X,R,A,step)


    def start_agent(self, **params):
        if not comm.connectSock(self.sock,self.server) : return 
            
        self.rtrace = None
        nReps = params.pop("nReps", 100)
        nSteps = params.pop("nSteps", 1000)
        N = params.pop("N", [nSteps]*nReps)
        #nmax_games = params.pop('record', 0)
   
        self.rtrace = []
        self.ptrace = []
        self.dbgXtr = []
        self.completeGames = []
        self.nGames = []

        for reps  in xrange(nReps):
            if self.flgTerm : break 

            #_,smplR,_,steps = self.get_samples(N[reps])
            smplX, smplR, smplA, steps = self.get_samples(N[reps])
            #rtrace.append(np.mean(smplR))
            map(self.rtrace.append, list(smplR[:steps+1,0]))
            map(self.ptrace.append, list(smplA[:steps+1,0]))
            self.dbgXtr.append(smplX)

            if self.nmax_games > 0:
                self.nGames.append(steps+1)
                if steps >= self.nmax_games: 
                    self.completeGames.append(1)
                else:
                    self.completeGames.append(0)

        #self.rtrace = rtrace
        #self.ptrace = ptrace
        #self.xtrace = dbgXtr


class MASequence(MARandom):

    def set_policy(self, policy):
        self.policy = policy

    def get_samples(self, numSamples) :
        X = np.zeros((numSamples,self.model.nnNI))
        R = np.zeros((numSamples,1))
        A = np.zeros((numSamples,self.model.get_actionDim()))

        s = self.model.init()
        # join to global env 
        t = 0
        tj,s0all = comm.joinMarl(self.sock, s,self.agentIdx)
        if tj is None: 
            return (X,R,A,-1)
            
        if self.agentIdx == -1: j= tj
        else: j = self.agentIdx
        #sys.stdout.write("RAND: "+ str(j)+ "----Joined\n")
        s = self.model.get_S(s0all, j)
        a = self.policy[0]

        if self.bulkopt: 
            for step in range(numSamples):
                if self.flgTerm : break
                s1 = [a] # next()
                a1 = self.policy[(t+1)%len(self.policy)]
                A[step,:] = a
                s,a = s1,a1

            if self.flgTerm: step=-1
            else:
                _, _, s1all = comm.actionMarlBulk(self.sock, j, step+1, s, A)
                if s1all is None: 
                    step=-1
                else:
                    #R[:(step+1),0] = s1all[:(step+1),j]
                    R[:(step+1),0] = s1all[:(step+1)]
            
        else:
            for step in range(numSamples):
                if self.flgTerm:
                    step -= 1
                    break

                # send action and get next states from Global tracker 
                t = t+1
                #sys.stdout.write("RAND: "+ str(j)+ "----Action\n")
                tmp, termflg, Rg, s1all,_ = comm.actionMarl(self.sock, j, t, s, a)
                if tmp==-1: break
                if not isinstance(s1all, np.ndarray):
                    s1all = np.asarray(s1all)
                s1 = self.model.get_S(s1all, j)

                # For now, ignore global reward Rg
                r1 = self.model.get_reward(s,s1,a, Rg,j, step, numSamples)

                a1 = self.policy[(t+1)%len(self.policy)]

                # Collect
                X[step,0:len(s)] = s
                X[step,len(s):] = a
                R[step,0] = r1
                A[step,:] = a
                
                if self.model.check_early_stop(Rg) or termflg:
                    break
                # next state
                s,a = s1,a1

        # end of episode. disconnect from GlobalTracker
        #print str(j)+"  ExitMarl"
        comm.exitMarl(self.sock, j)
        #print str(j)+" RETURN..."

        return (X,R,A,step)


"""
    Static Player for Prey Vs Predator
        move to direction there is no predator
        or move to closest prey

        => static action selection is defined in agent model
            getNextAct()
"""
class MAStatic(MARandom):

    def get_samples(self, numSamples) :
        X = np.zeros((numSamples,self.model.nnNI))
        R = np.zeros((numSamples,1))
        A = np.zeros((numSamples,self.model.get_action_dim()))

        s = self.model.init()
        # join to global env 
        t = 0
        tj,s0all = comm.joinMarl(self.sock, s,self.agentIdx)
        if tj is None: 
            return (X,R,A,-1)

        if self.agentIdx == -1: j= tj
        else: j = self.agentIdx
        #sys.stdout.write("RAND: "+ str(j)+ "----Joined\n")
        s = self.model.get_S(s0all, j)
        a = self.model.get_random_action()

        if self.bulkopt: 
            for step in range(numSamples):
                if self.flgTerm : break
                s1 = [a] # next()
                a1 = self.model.get_random_action()
                A[step,:] = a
                s,a = s1,a1

            if self.flgTerm: step=-1
            else:
                _, _, s1all = comm.actionMarlBulk(self.sock, j, step+1, s, A)
                if s1all is None: step=-1
                else:
                    #R[:(step+1),0] = s1all[:(step+1),j]
                    R[:(step+1),0] = s1all[:(step+1)]
            
        else:
            for step in range(numSamples):
                if self.flgTerm:
                    step -= 1
                    break
                # send action and get next states from Global tracker 
                t = t+1
                tmp, termflg, Rg, s1all,_ = comm.actionMarl(self.sock, j, t, s, a)
                if tmp==-1: break
                if not isinstance(s1all, np.ndarray):
                    s1all = np.asarray(s1all)
                s1 = self.model.get_S(s1all, j)

                # For now, ignore global reward Rg
                r1 = self.model.get_reward(s,s1,a, Rg,j, step, numSamples)

                a1 = self.model.get_next_act(s1all, j)

                # Collect
                X[step,0:len(s)] = s
                X[step,len(s):] = a
                R[step,0] = r1
                A[step,:] = a
                
                # In case of PvP::::
                #if np.max(np.abs(Rg))==500: break  # for PvP
                if self.model.check_early_stop(Rg) or termflg:
                    break

                # next state
                s,a = s1,a1

        # end of episode. disconnect from GlobalTracker
        comm.exitMarl(self.sock, j)

        return (X,R,A,step)


