#
# virtual class for reinforce learning agent model 
#
class RLModel:
    nnNI=0

    def __init__(self):
        pass
    def init(self):
        pass
    def get_random_action(self):
        pass
    #def next(self,s,a) : deprecated when extending to MARL
    #    pass
    def get_reward(self,s,s1,a):
        pass
    def get_state_range(self):
        pass
    def get_state_dim(self):
        pass
    def get_actions(self,s=None):
        pass
    def get_action_dim(self):
        pass
    def get_bound_action(self, a):
        return a
    def check_early_stop(self, R):
        return False
    def get_S(self, state, j):
        return state[j]
    def transform_action(self, a):
        return a
    def transform_state(self, s):
        return s


"""
    Multiagent environment base class
"""
class Environment():

    def __init__(self):
        pass

    def init(self):
        return None

    def next(self, Sall, Aall):
        return Sall, 0
