""" Prey vs Predator (continous version)
    agent model and environment

                by Jake Lee (lemin)
"""
import numpy as np
import math 
import random
from .base import RLModel, Environment
from scipy.spatial.distance import cdist
from copy import deepcopy as copy


"""
    class OrgAgent

        represent single organism: it can be either prey or predator

"""
class OrgAgent(RLModel):

    FieldSize=100
    def __init__(self, x, y, nFilteredStates=0):
        self.state = [x, y]
        self.nFilteredStates=nFilteredStates
        self.dActions = 3#2 - decompose to sin and cos for angle
        self.dState = 2
        self.nnNI = self.dState + self.dActions + self.nFilteredStates
        self.finalR = 0
    
    def init(self):
        return self.state

    def get_random_action(self):
        #return [random.uniform(0,math.pi*2), random.uniform(0,1)]
        #return [random.uniform(0,math.pi*2), random.uniform(0,1)]
        if self.dActions==2:
            return [random.sample(np.linspace(0,math.pi*2,13)[:12],1)[0], random.sample(np.linspace(0.25,1,4),1)[0] ]
        else:
            #angle=random.sample(np.linspace(0,math.pi*2,13)[:12],1)[0]
            angle=random.sample(np.linspace(0,math.pi*2,5)[:4],1)[0] ## lemin - REDUCE the RANDOMNESS
            #return [math.sin(angle),math.cos(angle), random.sample(np.linspace(0.25,1,4),1)[0] ]
            return [math.sin(angle),math.cos(angle), random.sample(np.linspace(0.5,1,2),1)[0] ]

    def get_reward(self, s, s1, a, Rg,j, t, last):
        # linear combination, but since RPG does not global reward, 
        #    {-1,0,1} reward stored in s1 is returned
        finalR=0
        if t==last-1: finalR = self.finalR
        return Rg[j] + finalR
        
    def get_state_range(self):
        return None

    def get_state_dim(self):
        return self.dState

    # discretize the actions
    def get_actions(self,s=None):
        dirs=np.linspace(0,math.pi*2,13)[:12]
        #step=np.linspace(0.25,1,4)
        step=np.linspace(0.5,1,2)
        dirs.shape=len(dirs),1
        step.shape=len(step),1

        if self.dActions==2:
            return np.hstack((np.tile(dirs,step.shape), np.repeat(step,dirs.shape[0],axis=0)))
        else:
            angles = np.tile(dirs,step.shape)
            return np.hstack((np.cos(angles), np.sin(angles), np.repeat(step,dirs.shape[0],axis=0)))

    def get_action_dim(self):
        return self.dActions

    def check_early_stop(self, R):
        if np.max(np.abs(R)) == 500:
            return True
        return False

class PreyAgent(OrgAgent):

    def __init__(self, startPred, nFilteredStates=0, **params):
        start_loc = params.pop('start', 'random')
        FieldSize = params.pop('fsize', OrgAgent.FieldSize)
        if isinstance(start_loc, list):
            mid = FieldSize * .5
            OrgAgent.__init__(self, mid, mid, nFilteredStates)
        else:
            fs10 = FieldSize * .1
            fs90 = FieldSize * .9
            OrgAgent.__init__(self, random.uniform(fs10, fs90),
                              random.uniform(fs10, fs90), nFilteredStates)
            
        self.ID = 0
        self.startPred = startPred
        self.finalR = 500

    def get_next_act(self, sall, j):

        # find closest predator
        dist = []
        dist.append( cdist([sall[j,:]], sall[self.startPred:, :]) )
        dist.append( cdist([(sall[j,:]+[self.FieldSize,0])], sall[self.startPred:, :]) )
        dist.append( cdist([(sall[j,:]+[0,self.FieldSize])], sall[self.startPred:, :]) )
        dist.append( cdist([(sall[j,:]-[self.FieldSize,0])], sall[self.startPred:, :]) )
        dist.append( cdist([(sall[j,:]-[0,self.FieldSize])], sall[self.startPred:, :]) )

        imindist = np.argmin(map(np.min, dist))
        closest = np.argmin(dist[imindist]) + self.startPred

        distadd = ([0,0],[self.FieldSize,0],[0,self.FieldSize],[-self.FieldSize,0],[0,-self.FieldSize])
        dx, dy = (sall[j,:]+distadd[imindist]) - sall[closest,:]
        if dx==0:
            if dy>0:
                angle = math.pi/2.
            else:
                angle = 3.*math.pi/2.
        else:
            angle = math.atan(abs(float(dy)/float(dx)))
            if dx<0 and dy>0:
                angle = math.pi - angle
            elif dx<0 and dy<0:
                angle = math.pi + angle
            elif dx>0 and dy<0:
                angle = 2*math.pi - angle
        
        if self.dActions == 2:
            return [angle, 1]
        else:
            return [math.cos(angle),math.sin(angle), 1]
        

class PredAgent(OrgAgent):

    def __init__(self, startPred, nFilteredStates=0):
        OrgAgent.__init__(self,random.uniform(0,1),random.uniform(0,1), nFilteredStates)
        #OrgAgent.__init__(self,random.uniform(0,0.3),random.uniform(0,0.3), nFilteredStates)
        #OrgAgent.__init__(self,0,0, nFilteredStates)
        self.ID = 1
        self.startPred = startPred
        self.finalR = -500

    def get_next_act(self, sall, j):

        # find closest prey
        dist = []
        dist.append( cdist([sall[j,:]], sall[:self.startPred, :]) )
        dist.append( cdist([(sall[j,:]+[self.FieldSize,0])], sall[:self.startPred, :]) )
        dist.append( cdist([(sall[j,:]+[0,self.FieldSize])], sall[:self.startPred, :]) )
        dist.append( cdist([(sall[j,:]-[self.FieldSize,0])], sall[:self.startPred, :]) )
        dist.append( cdist([(sall[j,:]-[0,self.FieldSize])], sall[:self.startPred, :]) )

        imindist = np.argmin(map(np.min, dist))
        closest = np.argmin(dist[imindist]) 

        distadd = ([0,0],[self.FieldSize,0],[0,self.FieldSize],[-self.FieldSize,0],[0,-self.FieldSize])
        dx, dy = sall[closest,:] - (sall[j,:]+distadd[imindist])
        if dx==0:
            if dy>0:
                angle = math.pi/2.
            else: 
                angle = 3.*math.pi/2.
        else:
            angle = math.atan(abs(float(dy)/float(dx)))
            if dx<0 and dy>0:
                angle = math.pi - angle
            elif dx<0 and dy<0:
                angle = math.pi + angle
            elif dx>0 and dy<0:
                angle = 2*math.pi - angle
       
        if self.dActions==2:
            return [angle, 1]
        else:
            return [math.cos(angle),math.sin(angle), 1]


"""
   class PvsPenv

        class for Prey vs Predator model
        update position and compute rewards
"""
PREY, PREDATOR = range(2)
class PvPenv(Environment):
    

    # initiate the identity of each agents
    def __init__(self, ids, fsize=100):
        self.ids = ids
        self.radius = 0.5
        self.killed = []
        self.FieldSize = fsize 

    # Sall, Aall - dictionary 
    def next(self, Sall, Aall):

        if type(Sall)==type({}): S = np.array(Sall.values())
        else: S = Sall
        if type(Aall)==type({}): A = np.array(Aall.values())
        else: A = Aall

        # update next step
        if False:
            S1 = S + (np.vstack((np.cos(A[:,0]), np.sin(A[:,0]))) * A[:,1]).T
        else:
            scale = A[:,2]
            scale.shape = len(scale),1
            S1 = S + (A[:,:2] * scale)
            
        distMat = cdist(S1, S1)
        bDist= distMat < self.radius
        np.fill_diagonal(bDist, False)
        iMeet = np.array(np.where(bDist)).T
        maxi = len(iMeet)/2
        rewards = [0] * len(self.ids)


        # reinforcement - delta distance 
        if True:
            dist0= cdist(S, S) 
            dist1= cdist(S1, S1) 
            delta = dist0 - dist1
            rwds = np.sign(delta)
            for i in xrange(delta.shape[0]):
                if self.ids[i] == PREY:
                    rewards[i] = np.sum(rwds[i,np.array(self.ids)==PREDATOR])
                else:
                    rewards[i] = np.sum(-rwds[i,np.array(self.ids)==PREY])
                


        if False: # reinforcement - with sum of distance
            maxdist = (self.FieldSize/2.)*math.sqrt(2)
            for i in xrange(distMat.shape[0]):
                if self.ids[i] == PREY:
                    rewards[i] = np.sum(distMat[i,np.array(self.ids)==PREDATOR])/maxdist
                else:
                    rewards[i] = -np.sum(distMat[i,np.array(self.ids)==PREY])/maxdist
        
        if False: 
            bDist= cdist(S1, S1) < self.FieldSize/4
            np.fill_diagonal(bDist, False)
            iClose = np.array(np.where(bDist)).T
            maxic = len(iClose)/2
            
            for k in xrange(maxic):
                i,j = iClose[k,:]
                if self.ids[i] != self.ids[j]: 
                    if self.ids[i] == PREY:
                        rewards[i] = -1
                        rewards[j] = 1
                    else:
                        rewards[i] = 1
                        rewards[j] = -1

        for k in xrange(maxi):
            i,j = iMeet[k,:]
            if self.ids[i] != self.ids[j]: 
                if self.ids[i] == PREY:
                    rewards[i] = -500#-1
                    rewards[j] = 500#1
                else:
                    rewards[i] = 500#1
                    rewards[j] = -500#-1

        if False:
            for k in xrange(len(rewards)):
                if rewards[k]==0:
                    if self.ids[k] == PREY:
                        rewards[k] = 1 
                    else:
                        rewards[k] = -1 

        # toroid precessing
        S1 += self.FieldSize
        S1 %= self.FieldSize
                
        return S1, rewards


