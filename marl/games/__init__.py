""" Game agents and environments
"""
from .base import RLModel, Environment

__all__ = ["match", "rps", "td", "pvp", "discpvp"]
