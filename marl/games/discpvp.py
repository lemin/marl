""" Prey vs Predator (discrete version)
    agent model and environment

                by Jake Lee (lemin)
"""
import numpy as np
import math 
import random
from .base import RLModel, Environment
from scipy.spatial.distance import cdist
from copy import deepcopy as copy


"""
    class DiscOrgAgent

        represent single organism: it can be either prey or predator

"""
class DiscOrgAgent(RLModel):

    FieldSize=10

    def __init__(self, x, y, nFilteredStates=0):
        self.state = [x, y]
        self.dActions = 1
        self.dState = 2
        self.finalR = 0
        self.nFilteredStates=nFilteredStates
        self.nnNI = self.dState + self.dActions + self.nFilteredStates
    
    def init(self):
        return self.state

    def get_random_action(self):
        return random.randint(-2, 2) # (-2,-1,0,1,2) = (down,left,stay,right,up)

    def get_reward(self, s, s1, a, Rg,j, t, last):
        # linear combination, but since RPG does not global reward, 
        #    {-1,0,1} reward stored in s1 is returned
        finalR=0
        if t==last-1: finalR = self.finalR
        return Rg[j] + finalR
        
    def get_state_range(self):
        return None

    def get_state_dim(self):
        return self.dState

    # discretize the actions
    def get_actions(self,s=None):
        return [-2, -1, 0, 1, 2]

    def get_action_dim(self):
        return self.dActions

    def check_early_stop(self, R):
        if np.max(np.abs(R)) == 500:
            return True
        return False


class DiscPreyAgent(DiscOrgAgent):

    def __init__(self, startPred, nFilteredStates=0):
        DiscOrgAgent.__init__(self, self.FieldSize/2, self.FieldSize/2, nFilteredStates)
        self.ID = 0
        self.startPred = startPred
        self.finalR = 500

    def get_next_act(self, sall, j):

        # find closest predator
        dist = []
        dist.append( cdist([sall[j,:]], sall[self.startPred:, :]) )
        dist.append( cdist([(sall[j,:]+[self.FieldSize,0])], sall[self.startPred:, :]) )
        dist.append( cdist([(sall[j,:]+[0,self.FieldSize])], sall[self.startPred:, :]) )
        dist.append( cdist([(sall[j,:]-[self.FieldSize,0])], sall[self.startPred:, :]) )
        dist.append( cdist([(sall[j,:]-[0,self.FieldSize])], sall[self.startPred:, :]) )

        imindist = np.argmin(map(np.min, dist))
        closest = np.argmin(dist[imindist]) + self.startPred

        distadd = ([0,0],[self.FieldSize,0],[0,self.FieldSize],[-self.FieldSize,0],[0,-self.FieldSize])
        dx, dy = (sall[j,:]+distadd[imindist]) - sall[closest,:]

        a = 0
        if abs(dx) > abs(dy):
            if sall[j,1] > sall[closest, 1]:
                a = -2
            else:
                a = 2
        else:
            if sall[j,0] > sall[closest, 0]:
                a = -1
            else:
                a = 1
        return a
        

class DiscPredAgent(DiscOrgAgent):

    def __init__(self, startPred, nFilteredStates=0):
        DiscOrgAgent.__init__(self, 0, 0, nFilteredStates)
        self.ID = 1
        self.startPred = startPred
        self.finalR = -500

    def get_next_act(self, sall, j):

        # find closest prey
        dist = []
        dist.append( cdist([sall[j,:]], sall[:self.startPred, :]) )
        dist.append( cdist([(sall[j,:]+[self.FieldSize,0])], sall[:self.startPred, :]) )
        dist.append( cdist([(sall[j,:]+[0,self.FieldSize])], sall[:self.startPred, :]) )
        dist.append( cdist([(sall[j,:]-[self.FieldSize,0])], sall[:self.startPred, :]) )
        dist.append( cdist([(sall[j,:]-[0,self.FieldSize])], sall[:self.startPred, :]) )

        imindist = np.argmin(map(np.min, dist))
        closest = np.argmin(dist[imindist]) 

        distadd = ([0,0],[self.FieldSize,0],[0,self.FieldSize],[-self.FieldSize,0],[0,-self.FieldSize])
        dx, dy = sall[closest,:] - (sall[j,:]+distadd[imindist])

        a = 0
        if abs(dx) > abs(dy):
            if sall[j,1] > sall[closest, 1]:
                a = 2
            else:
                a = -2
        else:
            if sall[j,0] > sall[closest, 0]:
                a = 1
            else:
                a = -1
        return a


"""
   class DiscPvsPenv

        class for discrete Prey vs Predator model
        update position and compute rewards
"""
PREY, PREDATOR = range(2)
class DiscPvPenv(Environment):

    # initiate the identity of each agents
    def __init__(self, ids, fsize=10):
        self.ids = ids
        self.radius = 0.5  # in discrete, radius 0.5 = meet on one point
        self.killed = []
        self.FieldSize = fsize 
        self.nHerd = 0

    # Sall, Aall - dictionary 
    def next(self, Sall, Aall):

        if type(Sall)==type({}): S = np.array(Sall.values())
        else: S = Sall
        if type(Aall)==type({}): A = np.array(Aall.values())
        else: A = Aall

        # check herding
        if self.nHerd >= 2: # min herd size is 2
            iprey = np.where(np.asarray(self.ids) == PREY)[0]
            Spreys = S[iprey]
            dist_preys = cdist(Spreys, Spreys)
            bdist_preys = dist_preys < self.radius
            np.fill_diagonal(bdist_preys, False)

            iherd = np.where(np.sum(bdist_preys, 1) > self.nHerd)[0]
            safe_zone = set([tuple(a) for a in Spreys[iherd]])
        else:
            safe_zone = set()

        # update next step
        yupd = np.sign(A) * (abs(A) == 2)
        xupd = np.sign(A) * (abs(A) == 1)
        S1 = copy(S)
        S1[:, 0] += xupd
        S1[:, 1] += yupd

        # check if invading a herd
        if len(safe_zone) > 0: 
            ipred = np.where(np.asarray(self.ids) == PREDATOR)[0]
            sz = np.asarray([list(a) for a in safe_zone]) #set -> array
            dist_preds = cdist(S1, sz)
            bdist_preds = dist_preds < self.radius

            binvade = np.any(bdist_preds == True, 1)
            binvade[iprey] = False

            # void invaded action
            S1[binvade] = S[binvade]

        distMat = cdist(S1, S1)
        bDist= distMat < self.radius
        np.fill_diagonal(bDist, False)
        iMeet = np.array(np.where(bDist)).T
        maxi = len(iMeet)/2
        rewards = [0] * len(self.ids)

        # reinforcement - delta distance 
        if True:
            dist0= cdist(S, S) 
            dist1= cdist(S1, S1) 
            delta = dist0 - dist1
            rwds = np.sign(delta)
            for i in xrange(delta.shape[0]):
                if self.ids[i] == PREY:
                    rewards[i] = np.sum(rwds[i,np.array(self.ids)==PREDATOR])
                else:
                    rewards[i] = np.sum(-rwds[i,np.array(self.ids)==PREY])
                
        for k in xrange(maxi):
            i,j = iMeet[k,:]
            if self.ids[i] != self.ids[j]: 
                if self.ids[i] == PREY:
                    rewards[i] = -500#-1
                    rewards[j] = 500#1
                else:
                    rewards[i] = 500#1
                    rewards[j] = -500#-1
                # for all predators
                for p in xrange(len(self.ids)):
                    if self.ids[p] == PREDATOR:
                        rewards[p] = 500

        # toroid precessing
        S1 += self.FieldSize
        S1 %= self.FieldSize

        #print S1, rewards
        return S1, rewards
