""" Matching pennies """
import numpy as np
import random 
from .base import RLModel


nFilteredStates=0

class MatchAgent(RLModel):

    def __init__(self):
        self.state = [0]
        self.nnNI = 2 + nFilteredStates
        self.nActions = 2
    
    def init(self):
        self.state = [0]
        return self.state

    def get_random_action(self):
        return random.randint(0,1)

    def get_reward(self, s, s1, a, Rg,j, t, last):
        # linear combination, but since RPG does not global reward, 
        #    {-1,0,1} reward stored in s1 is returned
        return s1[0]+Rg  
        
    def get_state_range(self):
        # ignoring other agents state 
        # 1st column: range of state, 2nd column: range of actions
        if nFilteredStates>0:
            return np.hstack((np.tile(np.array([[0,1]]).T,(1,1+nFilteredStates)), np.array([[0,1]]).T))
        else:
            return np.hstack((np.array([[0,1]]).T, np.array([[0,1]]).T))

    def get_state_dim(self):
        return 1

    def get_actions(self,s=None):
        return range(2)

    def get_action_dim(self):
        return 1


"""
    Matching Pennies Game environment 

        win or lose status on the last game is current status
        0 for tie, 1 for win, -1 for lose
"""
class Matchenv(Environment):
    
    def __init__(self):
        # create reward table for 2player RockPaperScissors
        self.rewardTable = {}
        for p1 in xrange(2):
            for p2 in xrange(2):
                if p1==p2:
                    self.rewardTable[(p1,p2)] = (1,0)   
                else:
                    self.rewardTable[(p1,p2)] = (0,1)

    def next(self, Sall, Aall):
        # for state representation compatibility
        return [[i] for i in self.rewardTable[tuple(Aall.values())]], 0

