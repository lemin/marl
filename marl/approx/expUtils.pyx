""" This is a collection of utility function for experiment

    for partitioning and standardization, please take a look at
    lecture notes from CS545 (http://www.cs.colostate.edu/~cs545)
    some implementations are taken from the lecture notes

                                    by lemin (Minwoo Jake Lee)

    Currently expUtils have
        partition(X, T, frac) for cross validation
        make_standardize(X) for data normalization
        bootstrap(X, func, nboot) for bootstraping

    late modified: Sep 20, 2011
"""
import random
import numpy as np


# partitioning of training and testing data sets
#
# @X    source data set
# @T    target data set (label)
# @frac fraction information for cross validation
#       this should be summed 1
def partition(X, T, frac=(0.8, 0.2)):
    """ A function for random partitioning of train and test sets
        Usage:
            (Xtrain, Ttrain), (Xval, Tval), (Xtest, Ttest) =
                partition(X, T, (0.4, 0.3, 0.3))
    """
    if not isinstance(X, np.ndarray):
        X = np.asanyarray(X)
    if not isinstance(T, np.ndarray):
        T = np.asanyarray(T)
    assert X.shape[0] == T.shape[0]
    n_samples, _ = X.shape

    ns = [int(round(n_samples * f)) for f in frac[:-1]]
    # make sure sum to n_samples
    last = n_samples - np.sum(ns)
    if last > 0:
        ns.append(last)

    Idx = range(n_samples)
    random.shuffle(Idx)
    return [(X[Idx[:n], :], T[Idx[:n], :]) for n in ns]


# construct standardize and unstandardize function with means and stds
#
# @X    data to standardize
def make_standardize(X):
    means = np.mean(X, 0)
    stds = np.std(X, 0)

    # standardize function to return
    def standardize(Xnew):
        return (Xnew - means) / stds

    # standardize function to return
    def unstandardize(Xs):
        return Xs * stds + means

    return standardize, unstandardize


class Standardizer: 
    """ class version of standardization """
    def __init__(self, X):
        self._mu = np.mean(X,0)  
        self._sigma = np.std(X,0)

    def standardize(self,X):
        return (X - self._mu) / self._sigma 

    def unstandardize(self,X):
        return (X * self._sigma) + self._mu 


# bootstraping resampling
#
# @X        data to resample
# @func     function to apply to the resampled data
# @nboot    the number of resampling
def bootstrap(X, func, nboot):
    """ a function for bootstraping
        usage:
            bootstrapMeans = bootstrap(data, np.mean, nboot)

            # confidence interval with normal assumption
            meanboot = np.mean(bootstrapMeans)
            stdboot = np.std(bootstrapMeans)

            lowBoot1 = meanboot - 1.96 * stdboot
            highBoot1 = meanboot + 1.96 * stdboot)
            print "Bootstrap CI assuming means are Normal:",lowBoot1,highBoot1

            # CI with quantile (w/o normal assumption)
            bootstrapMeans.sort()
            loweri = int(round(nboot*0.025))
            (lowBoot2, highBoot2) = bootstrapMeans[[loweri,nboot-loweri]]
            print "Bootstrap CI without assumptions:",lowBoot2,highBoot2
    """
    n = len(X)
    resamples = np.array([[random.choice(X) for i in xrange(n)]
                            for j in range(nboot)])
    return np.apply_along_axis(func, 1, resamples)


# normal distribution density
#
# @X        data
# @mu       mean of data
# @Sigma    covariance matrix
# For multiple samples, for any dimension, including 1
def dnorm(X, mu=None, sigma=None):
    """ norm distribution density for n_samples and n_dim data X
    """
    n_samples, n_dim = X.shape
    if np.any(mu == None):
        mu = np.zeros((n_dim,1))
    if np.any(sigma == None):
        sigma = np.eye(n_dim)
    if n_dim == 1:
        det_sigma = sigma
        sigma_inv = 1.0 / sigma
    else:
        det_sigma = np.linalg.det(sigma)
        sigma_inv = np.linalg.inv(sigma)
    norm_const = 1.0 / np.sqrt((2 * np.pi) ** n_dim * det_sigma)
    diffv = X - mu.T # change column vector mu to be row vector
    exp_term = np.exp(-0.5 * np.sum(np.dot(diffv, sigma_inv) * diffv, axis=1))
    return norm_const * exp_term[:,np.newaxis]


# create indicator matrix from target labels
#
# @T    target label data
def makeIndicatorVars(T):
    """ create indicator matrix
    Parameters
    ----------
    T: array. shape=[n_samples]
        target class labels
    """
    # Make sure T is two-dimensiona. Should be nSamples x 1.
    if T.ndim == 1:
        T = T.reshape((-1,1))    
    return (T == np.unique(T)).astype(int)
