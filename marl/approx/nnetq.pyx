""" Neural Network for SARSA

                                by Jake Lee (lemin)

    last modified: 3/7/2012
        a seperate inherited class for SARSA func approx
"""
import numpy as np
from neuralnet import NeuralNet
from expUtils import Standardizer
from grad import scg, steepest


class NeuralNetQ(NeuralNet):
    """ Neural Network Function Approximation (SARSA)
    """

    def __init__(self, nunits, stateRange):
        NeuralNet.__init__(self, nunits)
        if stateRange is not None:
            self.stdX = Standardizer(stateRange)
        self.stdTarget = False

    def _errorf(self, T, Y):
        return self._R + self._gamma * self._Q - Y

    def _objectf(self, T, Y, wpenalty):
        #error = self._errorf(T, Y)
        error = self._errorf(self._R + self._gamma * self._Q, Y) # 20120514 found error
        return .5 * np.mean(np.square(error)) + wpenalty

    def train(self, X, T, R, Q, **params):
        self._R = R
        self._Q = Q
        self._gamma = params.pop('Gamma', 1.)
        NeuralNet.train(self, X, T, **params)

    def search_action(self, S, A, wprec=1e-12, fprec=1e-12, nIter=10000):

        DA = A.shape[1]
        #DS = S.shape[1]

        def gradf(act):
            SA = np.hstack((S,np.array([act])))
            SA = self.stdX.standardize(SA)
            [Y,Z] = self.forward(SA)

            delta = np.identity(self._W[self._nLayers-1].shape[1])
            for i in xrange(self._nLayers-1,0,-1):
                delta = np.dot(delta, self._W[i][1:,:].T) * (1 - Z[i]*Z[i])
            last = self._W[0].shape[0]
            grad = np.dot(-delta, self._W[0][(last-DA):,:].T)
            grad.shape=grad.size,
            return grad #, False

        def qfunc(act):
            SA = np.hstack((S,np.array([act])))
            SA = self.stdX.standardize(SA)
            [Y,Z] = self.forward(SA)
            return -Y  # maximize Q

        scgResult = scg(A[0], gradf, qfunc,
                            wPrecision=wprec, fPrecision=fprec, nIterations=nIter)
        #print scgResult['f']
        #print scgResult['reason']
        return scgResult['w']
