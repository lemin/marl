# simple makefile to build and install 

PYTHON=python
ifdef home
	homearg=--home=$(home$)
endif
record=installed
recordarg=--record=$(record)


all: inplace

clean-pyc:
	find . -name "*.pyc" | xargs rm -f

clean-so:
	find . -name "*.so" | xargs rm -f

clean: clean-pyc clean-so
	rm -rf build

inplace:
	$(PYTHON) setup.py build_ext --inplace

install:
	$(PYTHON) setup.py install $(homearg) $(recordarg)

uninstall:
	@if [ -e $(record) ]; then \
		echo "uninstalling...";\
		cat $(record) | xargs rm -rf ; \
	else \
		echo "installation records are not found!"; \
	fi

dist:
	$(PYTHON) setup.py sdist
	
